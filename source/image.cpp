#include "main.h"


//コンストラクタ
image::image(
	game& c_Game):
	cGame(c_Game)
{
	this->initialize();
}

//デストラクタ
image::~image()
{
}

//初期化
int image::initialize()
{
	//画像の読み込み
	this->handle[static_cast<int>(IMAGE::IMAGES::TITLE_BACK)] = DxLib::LoadGraph("dat\\image\\titleBack.png");
	this->handle[static_cast<int>(IMAGE::IMAGES::MAGIC_CIRCLE1)] = DxLib::LoadGraph("dat\\image\\magicCircle1.png");
	this->handle[static_cast<int>(IMAGE::IMAGES::MAGIC_CIRCLE1_FLASH)] = DxLib::LoadGraph("dat\\image\\magicCircle1Flash.png");
	this->handle[static_cast<int>(IMAGE::IMAGES::MAGIC_CIRCLE2)] = DxLib::LoadGraph("dat\\image\\magicCircle2.png");
	this->handle[static_cast<int>(IMAGE::IMAGES::MAGIC_CIRCLE2_FLASH)] = DxLib::LoadGraph("dat\\image\\magicCircle2Flash.png");
	this->handle[static_cast<int>(IMAGE::IMAGES::FIELD_POLE)] = DxLib::LoadGraph("dat\\image\\fieldPole.png");
	this->handle[static_cast<int>(IMAGE::IMAGES::BLOCK_SAMPLE1)] = DxLib::LoadGraph("dat\\image\\block\\00.png");
	this->handle[static_cast<int>(IMAGE::IMAGES::BLOCK_SAMPLE2)] = DxLib::LoadGraph("dat\\image\\block\\01.png");
	this->handle[static_cast<int>(IMAGE::IMAGES::BLOCK_SAMPLE3)] = DxLib::LoadGraph("dat\\image\\block\\02.png");
	this->handle[static_cast<int>(IMAGE::IMAGES::BLOCK_SAMPLE4)] = DxLib::LoadGraph("dat\\image\\block\\03.png");
	this->handle[static_cast<int>(IMAGE::IMAGES::BLOCK_SAMPLE5)] = DxLib::LoadGraph("dat\\image\\block\\04.png");
	this->handle[static_cast<int>(IMAGE::IMAGES::BLOCK_SAMPLE6)] = DxLib::LoadGraph("dat\\image\\block\\05.png");
	this->handle[static_cast<int>(IMAGE::IMAGES::BLOCK_SAMPLE7)] = DxLib::LoadGraph("dat\\image\\block\\06.png");

	//読み込みに失敗した画像がないか確認する
	for (int i = 0; i < IMAGE::IMAGE_MAX; i++) {
		if (this->handle[i] == -1)
		{
			//読み込み失敗
			MessageBox(NULL, "画像データ読み込み失敗", "TETRIS4D", 0);
			exit(-1);
		}
	}
	return 0;
}

//画像の取得
int image::getHandle(IMAGE::IMAGES handle)
{
	return this->handle[static_cast<int>(handle)];
}
