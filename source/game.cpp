#include "main.h"

//コンストラクタ
game::game()
{
	//オプションデータの読み込み
	this->cOption = new option(*this);
	this->cOption->load();

	if (this->initialize() == -1)
	{
		MessageBox(NULL, "初期化エラー", "TETRIS4D", 0);
		exit(-1);
	}
}

//デストラクタ
game::~game()
{
	//セーブデータの保存
	this->cOption->save();

	//各クラスの破棄
	delete this->cFont;
	delete this->cSound;
	delete this->cImage;
	delete this->cController;
	delete this->cFps;
	delete this->cMaingame;
	delete this->cOption;
	delete this->cTitle;
	delete this->cAutoPlay;
	delete this->cReplay;
	delete this->cRanking;
	this->effectList.clear();

	//Dxライブラリの終了
	DxLib_End();
}

//初期化処理
int game::initialize()
{
	//ログ出力を行わない(最優先)
	//ウィンドウテキストの設定
	//FPSを固定しない
	//ウィンドウモード/フルスクリーンの設定
	//画面解像度の設定
	//ウィンドウ初期位置の設定
	//ウィンドウサイズを変更できるようにする
	//DxLibの初期化
	//描画対象を裏画面に
	//ウィンドウが非アクティブでも実行
	std::string titleStr = "TETRIS Dim.4 Ver";
	titleStr += VERSION_STRING;

	if (DxLib::SetOutApplicationLogValidFlag(FALSE) == -1)goto INITIALIZE_ERROR;
	if (DxLib::SetMainWindowText(titleStr.c_str()) == -1)goto INITIALIZE_ERROR;
	if (DxLib::SetWaitVSyncFlag(FALSE) == -1)goto INITIALIZE_ERROR;
	//if (DxLib::ChangeWindowMode(TRUE));
	if (DxLib::SetGraphMode(
		OPTION::WINDOW_LIST_X[this->cOption->getSaveData().windowType],
		OPTION::WINDOW_LIST_Y[this->cOption->getSaveData().windowType], 32) == -1)goto INITIALIZE_ERROR;
	if (DxLib::ChangeWindowMode(this->cOption->getSaveData().fullScreen) == -1)goto INITIALIZE_ERROR;
	if (DxLib::SetWindowPosition(0, 0) == -1)goto INITIALIZE_ERROR;
	if (DxLib::SetWindowSizeChangeEnableFlag(TRUE) == -1)goto INITIALIZE_ERROR;
	if (DxLib::DxLib_Init() == -1)goto INITIALIZE_ERROR;
	if (DxLib::SetDrawScreen(DX_SCREEN_BACK) == -1)goto INITIALIZE_ERROR;
	if (DxLib::SetAlwaysRunFlag(TRUE) == -1)goto INITIALIZE_ERROR;

	//if (SetUseZBuffer3D(TRUE) == -1)goto INITIALIZE_ERROR;
	if (SetUseZBufferFlag(TRUE) == -1)goto INITIALIZE_ERROR;
	//if (SetWriteZBuffer3D(TRUE) == -1)goto INITIALIZE_ERROR;
	if (SetWriteZBufferFlag(TRUE) == -1)goto INITIALIZE_ERROR;

	//所有クラスの初期化
	this->cFont = new font(*this);
	this->cSound = new sound(*this);
	this->cImage = new image(*this);
	this->cFps = new fps(*this);
	this->cController = new controller(*this);
	this->cConfig = new config(*this, *this->cController);
	this->cTitle = new title(*this, *this->cController);
	this->cMaingame = new maingame(*this, *this->cController);
	this->cReplay = new replay(*this, *this->cController);
	this->cAutoPlay = new autoPlay(*this, *this->cController);
	this->cRanking = new ranking(*this, *this->cController);
	this->effectList.clear();

	//ゲーム状態の初期化
	this->eScene = SCENE::TITLE;
	this->cTitle->initialize();

	//常駐エフェクトの発生
	//this->effectList.push_back(std::shared_ptr<effectBase>(new effectMagicCircle(*this)));
	//this->effectList.push_back(std::shared_ptr<effectBase>(new effectFieldPole(*this)));
	//this->effectList.push_back(std::shared_ptr<effectBase>(new effectMagicCircleTop(*this)));

	return 0;

INITIALIZE_ERROR:
	return -1;
}

//ゲーム状態を変更する関数
void game::setScene(SCENE scene)
{
	this->eScene = scene;
}

//スクリーン上の座標を割合で取得する
//この関数を使い座標を取得すると,
//解像度の変更に対応できる
//float:0~1(左端→右端)
int game::getScreenX(
	float rate)
{
	return OPTION::WINDOW_LIST_X[this->cOption->getSaveData().windowType] / 1.0f*rate;
}

//スクリーン上の座標を割合で取得する
//この関数を使い座標を取得すると,
//解像度の変更に対応できる
//float:0~1(上端→下端)
int game::getScreenY(
	float rate)
{
	return OPTION::WINDOW_LIST_Y[this->cOption->getSaveData().windowType] / 1.0f*rate;
}

//エフェクトの更新
void game::effectUpdate()
{
	for (std::vector<std::shared_ptr<effectBase>>::iterator itEffect = this->effectList.begin();
	itEffect != this->effectList.end();)
	{
		if (itEffect->get()->update() == -1)
		{
			itEffect = this->effectList.erase(itEffect);
			continue;
		}
		++itEffect;
	}
}

//アップデート関数
int game::update()
{

	//メインループ
	while (DxLib::ProcessMessage() == 0)
	{
		//画面を初期化
		DxLib::ClearDrawScreen();
		//コントローラの更新
		this->cController->update();

		//ゲーム状態によって分岐
		switch (this->eScene)
		{
		case SCENE::TITLE:
			this->cTitle->update();
			break;
		case SCENE::MAINGAME:
			this->cMaingame->update();
			break;
		case SCENE::CONFIG:
			this->cConfig->update();
			break;
		case SCENE::REPLAY:
			this->cReplay->update();
			break;
		case SCENE::AUTOPLAY:
			this->cAutoPlay->update();
			break;
		case SCENE::RANKING:
			this->cRanking->update();
			break;
		default:
			break;
		}

		//FPS管理
		this->cFps->update();
		//サウンド再生
		this->cSound->update();

		//エフェクトの更新
		this->effectUpdate();

		//裏画面を表画面に複写
		DxLib::ScreenFlip();

		//SS撮影
		if (this->cController->getButton(BUTTON::BACK) == 1)
		{
			SaveDrawScreenToPNG(
				0,
				0,
				OPTION::WINDOW_LIST_X[this->cOption->getSaveData().windowType],
				OPTION::WINDOW_LIST_Y[this->cOption->getSaveData().windowType],
				"SS.png");
		}
	}

	return 0;
}