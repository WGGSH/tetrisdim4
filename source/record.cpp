#include "main.h"

//リプレイに不備あり
//現在停止中

//コンストラクタ
record::record(
	game& c_Game, controller& c_Controller):
	cGame(c_Game),cController(c_Controller)
{
}

//デストラクタ
record::~record()
{
}

//初期化処理
//入力情報を保存するメモリを確保する
int record::initialize()
{
	this->sReplayData = new REPLAY::REPLAYDATA;
	this->sReplayData->frames = 0;
	this->sReplayData->srand = 0;
	this->sReplayData->inputList.clear();
	//memset(this->sReplayData->input, 0, sizeof(int) * 600 * BUTTON_MAX);
	
	return 0;
}

//更新処理
//全てのボタンの入力を記録
int record::update()
{
	this->sReplayData->frames++;
	//int button[BUTTON_MAX];
	int *button = new int[BUTTON_MAX];
	for (int i = 0; i < BUTTON_MAX; i++) {
		button[i] = this->cController.getButton(static_cast<BUTTON>(i));
	}
	this->sReplayData->inputList.push_back(button);


	return 0;
}

//保存処理
//リプレイデータをファイルに書き込む
void record::save()
{
	FILE *file;
	errno_t error;

	//ファイルの書き込み開始
	if (error = fopen_s(&file, "replay.dat", "wb") != 0) {
		//エラー処理
	}
	else {
		//データの書き込み
		//初期乱数
		fwrite(&this->sReplayData->srand, sizeof(int), 1, file);
		//総フレーム数
		fwrite(&this->sReplayData->frames, sizeof(int), 1, file);
		//キー入力
		for (int i = 0; i < this->sReplayData->frames; i++) {
			for (int j = 0; j < BUTTON_MAX; j++) {
				fwrite(&this->sReplayData->inputList.at(i)[j], sizeof(int), 1, file);
			}
		}
		//fwrite(&this->sReplayData->input, sizeof(int), 600*BUTTON_MAX, file);
	}

	fclose(file);
}