#pragma once
#include "effectBase.h"

namespace EFFECT
{
};

class effectMagicCircle :
	public effectBase
{
protected:
	MATRIX rotMat;
	MATRIX scaleMat;
	MATRIX convertMat;

	VECTOR pos1[4];
	VERTEX3D vertex1[6];
public:
	effectMagicCircle(game&);
	~effectMagicCircle();

	virtual int update();
};

#include "effectMagicCircleTop.h"