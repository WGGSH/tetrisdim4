#include "main.h"


//コンストラクタ
replay::replay(
	game& c_Game,controller& c_Controller):
	maingame(c_Game, c_Controller)
{
}


//デストラクタ
replay::~replay()
{
}

//読み込み
void replay::load()
{
	//リプレイデータの読み込み

	FILE *file;
	errno_t error;
	if (error = fopen_s(&file, "replay.dat", "rb") != 0)
	{
		//リプレイデータが見つからない
	}
	else
	{
		this->sReplayData = new REPLAY::REPLAYDATA;
		//初期乱数
		fread(&this->sReplayData->srand, sizeof(int), 1, file);
		//フレーム数
		fread(&this->sReplayData->frames, sizeof(int), 1, file);
		//ボタン入力
		*this->sReplayData->input = new int[this->sReplayData->frames];
		for (int i = 0; i < this->sReplayData->frames; i++) {
			this->sReplayData->input[i] = new int[BUTTON_MAX];
			for (int j = 0; j < BUTTON_MAX; j++) {
				fread(&this->sReplayData->input[i][j], sizeof(int), 1, file);
			}
		}
		//fread(&this->sReplayData->input, sizeof(int), 600 * BUTTON_MAX, file);
		fclose(file);
	}

}

//初期化
int replay::initialize()
{
	this->load();

	//カウンタの初期化
	this->count = 0;
	this->gameOverCount = 0;
	//ゲーム状態の初期化
	this->eState = STATE::WIPE_S;
	this->eGameState = GAME_STATE::DROP;
	this->gameOverSelect = 0;
	//得点の初期化
	this->score = 0;

	//背景画像ハンドルの初期化
	this->backHandle = DxLib::MakeGraph(
		OPTION::WINDOW_LIST_X[this->cGame.getOption().getSaveData().windowType],
		OPTION::WINDOW_LIST_Y[this->cGame.getOption().getSaveData().windowType]);


	//フィールドの初期化
	for (int z = 0; z < FIELD_SIZE_Z; z++) {
		for (int x = 0; x < FIELD_SIZE_X; x++) {
			for (int y = 0; y < FIELD_SIZE_Y; y++) {
				for (int w = 0; w < FIELD_SIZE_W; w++) {

					if (
						((x == 0 || x == FIELD_SIZE_X - 1) +
							(y == 0 || y == FIELD_SIZE_Y - 1) +
							(z == 0 || z == FIELD_SIZE_Z - 1) +
							(w == 0 || w == FIELD_SIZE_W - 1)) == 0)
					{
						this->field[x][y][z][w] = static_cast<int>(BLOCK_TYPE::NON);
					}
					else
					{
						this->field[x][y][z][w] = static_cast<int>(BLOCK_TYPE::GROUND);
					}

				}
			}
		}
	}

	this->create();

	//3Dの初期化処理
	this->initialize3d();

	return 0;
}

//ゲームオーバー判定
int replay::over()
{
	if (this->check(this->field, this->nowBlock, this->location) == FALSE)
	{

		//ゲームオーバー
		//this->initialize();
		this->eState = MAINGAME::STATE::GAMEOVER_ANIM;
		//this->cRecord->save();
	}

	return 0;
}

//更新
int replay::update()
{
	this->count++;
	//コントローラをハックする
	if (this->sReplayData->frames > this->count-1)
	{
		for (int i = 0; i < BUTTON_MAX; i++) 
		{
			this->cController.getButtonAll()[i] = this->sReplayData->input[this->count - 1][i];
		}
	}

	switch (this->eState)
	{
	case STATE::WIPE_S:
		this->draw3D();
		this->drawUI();
		this->drawWipe();
		break;
	case STATE::WIPE_F:
		this->draw3D();
		this->drawUI();
		this->drawWipe();
		break;
	case STATE::PLAYING:
		switch (this->eGameState)
		{
		case GAME_STATE::DROP:
			this->input();
			//this->draw();
			this->cameraInput();
			this->cameraMove();
			this->draw3D();
			this->drawUI();
			break;
		case GAME_STATE::DELETING:
			break;
		}
		break;
	case STATE::PAUSE:
		this->pauseUpdate();
		break;
	case STATE::GAMEOVER_ANIM:
		this->gameOverAnimUpdate();
		break;
	case STATE::GAMEOVER_SELECT:
		this->gameOverSelectUpdate();
		break;
	default:
		break;
	}

	return 0;
}