#pragma once

namespace CONFIG
{
	//シーン
	enum STATE
	{
		WIPE_S,
		WIPE_F,
		MAIN,//コンフィグ画面メイン
		WAIT_INPUT,//パッド入力受付
	};

}

class config
{
private:
	//参照
	game& cGame;
	controller& cController;
	
	//変数
	CONFIG::STATE state;//現在のシーン状態
	int select;//選択中の項目

	int wipeCount;//ワイプカウンタ
	int backHandle;//背景の画像ハンドル
	
	
	int padConfig[BUTTON_MAX];//コンフィグデータ

	//キーボードによる入力のみを受け付けるので,
	//連打判定カウンタが必要
	int upCount;
	int downCount;


	//関数
	void inputMain();//コンフィグするボタンを選択する状態の入力処理
	void inputWait();//コンフィグ後のボタンを決定する状態の入力処理

	void draw();//描画
	void drawWipe();//ワイプ描画
public:
	config(game&,controller&);
	~config();

	int update();//更新
	int initialize();//初期化

	int save();//保存
	int load();//読み込み

	//背景画像ハンドルの取得
	int getBackHandle() { return this->backHandle; }
};

