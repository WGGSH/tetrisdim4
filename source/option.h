#pragma once

namespace OPTION
{
	//セーブデータ構造体
	struct saveData {
		//int version;//現在のバージョン
		int windowType;//ウィンドウ解像度の配列番号
		BOOL fullScreen;//フルスクリーン
		int fpsMax;//フレームレートの上限値
	};

	//定数
		const int WINDOW_TYPE_MAX = 6;//ウィンドウサイズのパターン合計数
	//ウィンドウサイズリスト(X)
	const int WINDOW_LIST_X[WINDOW_TYPE_MAX] =
	{
		1920,1600,1280,1024,800,640
	};
	//ウィンドウサイズリスト(Y)
	const int WINDOW_LIST_Y[WINDOW_TYPE_MAX] =
	{
		1080,900,720,576,450,360
	};
	//解像度スケール比
	//getScreen関数の実装によりほぼ不要に
	const float SCREEN_SCALE_RATE[WINDOW_TYPE_MAX] =
	{
		1.0f,
		0.83333333333333333333333333333333f,
		0.66666666666666666666666666666667f,
		0.53333333333333333333333333333333f,
		0.41666666666666666666666666666667f,
		0.33333333333333333333333333333333f,
	};

}

class option
{
private:
	//参照
	game& cGame;

	//変数
	OPTION::saveData sSaveData;//セーブデータ
public:
	option(game&);
	~option();

	void save();//オプションデータの保存
	void load();//オプションデータの読み込み

	//セーブデータの取得
	OPTION::saveData& getSaveData() { return this->sSaveData; }
};

