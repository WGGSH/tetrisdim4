#pragma once
//警告の非表示
#pragma warning(disable:4091)

//クラスextern宣言
extern class option;
extern class font;
extern class sound;
extern class controller;
extern class replay;
extern class record;
extern class config;
extern class ranking;
extern class title;
extern class autoPlay;
extern class evaluateFunction;
extern class titleBack;
extern class maingame;
extern class fps;
extern class effectBase;
extern class image;

//定数宣言
const float PI = 3.14159265f;
const float PI2 = PI * 2;


//ライブラリのインポート
#include "DxLib.h"
//標準ライブラリ
#include <math.h>
#include <string>
#include <vector>
#include <functional>
#include <memory>
//自作ヘルパーライブラリ,構造体
#include "position.h"
#include "axis4d.h"
#include "helper.h"


const int NOW_VERSION = 50;//現在のバージョン
const std::string VERSION_STRING = "0.50";

//ヘッダファイルのインポート
#include "game.h"
#include "sound.h"
#include "font.h"
#include "option.h"
#include "controller.h"
#include "replay.h"
#include "record.h"
#include "fps.h"
#include "title.h"
#include "autoPlay.h"
#include "evaluateFunction.h"
#include "titleBack.h"
#include "config.h"
#include "ranking.h"
#include "maingame.h"
#include "effectBase.h"
#include "image.h"
