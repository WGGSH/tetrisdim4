#pragma once

namespace FONT {
	const int FONT_MAX = 3;//ゲーム内で使用するフォントの最大数
	//ゲーム内で使用するフォントの識別子
	enum class FONT_TYPE
	{
		SMALL,
		NORMAL,
		LARGE,
	};
}

class font
{
	//参照
	game& cGame;

	//変数
	int fonts[FONT::FONT_MAX];//フォントハンドル

public:
	font(game&);
	~font();

	int initialize();//初期化
	//フォントハンドルの取得
	int *getFonts() { return this->fonts; }
};

