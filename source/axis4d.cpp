#include "main.h"

//+オペレーター
//全要素を加算する
axis4d axis4d::operator+(
	axis4d& val)
{
	axis4d r = { 0 };
	r.xy = this->xy + val.xy;
	r.xz = this->xz + val.xz;
	r.xw = this->xw + val.xw;
	r.yz = this->yz + val.yz;
	r.yw = this->yw + val.yw;
	r.zw = this->zw + val.zw;
	return r;
}

//-オペレーター
//全要素を減算する
axis4d axis4d::operator-(
	axis4d& val)
{
	axis4d r = { 0 };
	r.xy = this->xy - val.xy;
	r.xz = this->xz - val.xz;
	r.xw = this->xw - val.xw;
	r.yz = this->yz - val.yz;
	r.yw = this->yw - val.yw;
	r.zw = this->zw - val.zw;
	return r;
}

//+=オペレーター
axis4d axis4d::operator+=(
	axis4d& val)
{
	this->xy += val.xy;
	this->xz += val.xz;
	this->xw += val.xw;
	this->yz += val.yz;
	this->yw += val.yw;
	this->zw += val.zw;
	return *this;
}

//-=オペレーター
axis4d axis4d::operator-=(
	axis4d& val)
{
	this->xy -= val.xy;
	this->xz -= val.xz;
	this->xw -= val.xw;
	this->yz -= val.yz;
	this->yw -= val.yw;
	this->zw -= val.zw;
	return *this;
}

//×オペレーター
//使うのか?
axis4d axis4d::operator*(
	int val)
{
	axis4d r = { 0 };
	r.xy = this->xy * val;
	r.xz = this->xz * val;
	r.xw = this->xw * val;
	r.yz = this->yz * val;
	r.yw = this->yw * val;
	r.zw = this->zw * val;
	return r;
}

//==オペレーター
bool axis4d::operator==(
	axis4d& val)
{
	return (
		(this->xy == val.xy) &&
		(this->xz == val.xz) &&
		(this->xw == val.xw) &&
		(this->yz == val.yz) &&
		(this->yw == val.yw) &&
		(this->zw == val.zw));
}

//!=オペレーター
bool axis4d::operator!=(
	axis4d& val)
{
	return (!(
		(this->xy == val.xy) &&
		(this->xz == val.xz) &&
		(this->xw == val.xw) &&
		(this->yz == val.yz) &&
		(this->yw == val.yw) &&
		(this->zw == val.zw)));
}

//取得
axis4d axis4d::set(
	int xy, int xz, int xw, int yz, int yw, int zw)
{
	axis4d r = { xy,xz,xw,yz,yw,zw };
	return r;
}