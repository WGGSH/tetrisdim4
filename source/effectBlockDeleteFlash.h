#pragma once
#include "effectBase.h"

namespace EFFECT
{
};

class effectBlockDeleteFlash :
	public effectBase
{
private:
	int zpos;
public:
	effectBlockDeleteFlash(game&, int);
	~effectBlockDeleteFlash();

	int update();
};

