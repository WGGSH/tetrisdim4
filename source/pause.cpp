#include "main.h"

//ポーズ画面の入力処理
int maingame::pauseInput()
{
	if (this->cController.getButton(BUTTON::START) == 1)
	{
		this->eState = STATE::PLAYING;
	}

	return 0;
}

//ポーズ画面の更新
int maingame::pauseUpdate()
{
	this->pauseInput();
	this->pauseDraw();

	return 0;
}

//ポーズ画面の描画
int maingame::pauseDraw()
{
	DxLib::DrawGraph(0, 0, this->backHandle, TRUE);

	DxLib::DrawFormatStringToHandle(
		this->cGame.getScreenX(0.45f),
		this->cGame.getScreenY(0.45f),
		DxLib::GetColor(128, 128, 255),
		this->cGame.getFont().getFonts()[2],
		"PAUSE");

	return 0;
}