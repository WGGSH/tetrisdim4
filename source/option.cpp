#include "main.h"


//コンストラクタ
option::option(
	game& c_Game):
	cGame(c_Game)
{
}

//デストラクタ
option::~option()
{
}

//オプションデータ保存処理
void option::save()
{
	FILE *file;
	errno_t error;

	//ファイルの書き込み開始
	if (error = fopen_s(&file, "userData\\option.dat", "wb") != 0) {
		//エラー処理
	}
	else {
		//所持金の書き込み
		fwrite(&this->sSaveData, sizeof(OPTION::saveData), 1, file);
	}

	fclose(file);
}

//オプションデータ読み込み処理
void option::load()
{
	FILE *file;
	errno_t error;

	if (error = fopen_s(&file, "userData\\option.dat", "rb") != 0)
	{
		//エラー処理
		//読み込みに失敗したとき用の初期値
		this->sSaveData.fpsMax = 60;
		this->sSaveData.fullScreen = TRUE;
		//this->sSaveData.version = 100;
		this->sSaveData.windowType = 1;
	}
	else
	{
		fread(&this->sSaveData, sizeof(OPTION::saveData), 1, file);
		fclose(file);
	}
}