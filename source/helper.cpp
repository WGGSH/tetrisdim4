#include "main.h"

//HSB��RGB�ϊ��֐�
//H:0~360
//S:0~1
//B:0~1
unsigned int helper::GetColorHSBtoRGB(float H, float S, float V) {
	int r, g, b;
	int h;
	float f;
	int p, q, t;
	int br;

	if (H>360.0f)
		H -= 360.0f;
	else if (H<0.0f)
		H += 360.0f;

	if (S>1.0f)
		S = 1.0f;
	else if (S<0.0f)
		S = 0.0f;

	br = (int)(255 * V);

	if (br>255)
		br = 255;
	else if (br<0)
		br = 0;

	if (S == 0.0) {
		r = br;
		g = br;
		b = br;
	}
	else {
		h = (int)(H / 60.0f);
		f = H / 60.0f - h;
		p = (int)(br*(1.0 - S));

		if (p<0)
			p = 0;
		if (p>255)
			p = 255;

		q = (int)(br*(1.0f - f*S));
		if (q<0)
			q = 0;
		if (q>255)
			q = 255;

		t = (int)(br*(1.0f - (1.0f - f)*S));
		if (t<0)
			t = 0;
		if (t>255)
			t = 255;

		switch (h) {
		case 0: r = br; g = t; b = p; break;
		case 1: r = q; g = br; b = p; break;
		case 2: r = p; g = br; b = t; break;
		case 3: r = p; g = q; b = br; break;
		case 4: r = t; g = p; b = br; break;
		case 5: r = br; g = p; b = q; break;
		default: r = g = b = 0; break;
		}
	}

	return DxLib::GetColor(r, g, b);
}