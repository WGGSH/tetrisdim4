#include "main.h"

using namespace CONTROLLER;

//コンストラクタ
controller::controller(
	game& c_game) :
	cGame(c_game)
{
	//padParam
	this->padParam = new short*[BUTTON_MAX];

	//コンフィグの初期化
	//ゲームパッド
	//初期値
	//コンフィグデータがあれば読み込んで書き換える

	//キーボード
	//初期値
	//変更可能にする予定なし
	this->setKey[static_cast<int>(BUTTON::A)] = KEY_INPUT_Z;
	this->setKey[static_cast<int>(BUTTON::B)] = KEY_INPUT_X;
	this->setKey[static_cast<int>(BUTTON::X)] = KEY_INPUT_C;
	this->setKey[static_cast<int>(BUTTON::Y)] = KEY_INPUT_V;
	this->setKey[static_cast<int>(BUTTON::LB)] = KEY_INPUT_LSHIFT;
	this->setKey[static_cast<int>(BUTTON::RB)] = KEY_INPUT_RSHIFT;
	this->setKey[static_cast<int>(BUTTON::LS)] = KEY_INPUT_Q;
	this->setKey[static_cast<int>(BUTTON::RS)] = KEY_INPUT_O;
	this->setKey[static_cast<int>(BUTTON::BACK)] = KEY_INPUT_ESCAPE;
	this->setKey[static_cast<int>(BUTTON::START)] = KEY_INPUT_SPACE;
	this->setKey[static_cast<int>(BUTTON::HAT_UP)] = KEY_INPUT_I;
	this->setKey[static_cast<int>(BUTTON::HAT_RIGHT)] = KEY_INPUT_L;
	this->setKey[static_cast<int>(BUTTON::HAT_DOWN)] = KEY_INPUT_K;
	this->setKey[static_cast<int>(BUTTON::HAT_LEFT)] = KEY_INPUT_J;
	this->setKey[static_cast<int>(BUTTON::LT)] = KEY_INPUT_LCONTROL;
	this->setKey[static_cast<int>(BUTTON::RT)] = KEY_INPUT_BACKSLASH;
	this->setKey[static_cast<int>(BUTTON::LSTICK_UP)] = KEY_INPUT_UP;
	this->setKey[static_cast<int>(BUTTON::LSTICK_RIGHT)] = KEY_INPUT_RIGHT;
	this->setKey[static_cast<int>(BUTTON::LSTICK_DOWN)] = KEY_INPUT_DOWN;
	this->setKey[static_cast<int>(BUTTON::LSTICK_LEFT)] = KEY_INPUT_LEFT;
	this->setKey[static_cast<int>(BUTTON::RSTICK_UP)] = KEY_INPUT_W;
	this->setKey[static_cast<int>(BUTTON::RSTICK_RIGHT)] = KEY_INPUT_D;
	this->setKey[static_cast<int>(BUTTON::RSTICK_DOWN)] = KEY_INPUT_S;
	this->setKey[static_cast<int>(BUTTON::RSTICK_LEFT)] = KEY_INPUT_A;
}

//デストラクタ
controller::~controller()
{
}

//指定したゲームボタンの入力状態を返す
//他のクラスからはこの関数を用いてボタンの入力判定を行う
int controller::getButton(
	BUTTON button)
{
	return this->button[static_cast<int>(button)];
}

//全てのボタンの入力状態を返す
int* controller::getButtonAll()
{
	return this->button;
}


//キー取得関数
//キーボードのすべてのキーの入力状態を取得する
int controller::getAllKey()
{
	if (DxLib::GetHitKeyStateAll(this->allKey)==-1)return -1;
	return 0;
}

//DirectInput用のボツ
/*
//ゲームパッド取得関数
//ゲームパッドのすべてのボタン,アナログスティック,ハットスイッチの入力状態を取得する
int controller::getPadState()
{
	if(GetJoypadDirectInputState(DX_INPUT_PAD1, &this->padInput)==-1)return -1;
	return 0;
}

//指定されたボタンの入力状態を
//キーボードとゲームパッドの入力状態から決定する
int controller::setButton(BUTTON button, int key, int pad)
{
	//キーボードとボタンのうち、どちらかが押されていたらオンにする
	this->button[static_cast<int>(button)] =
		(this->allKey[key] | (this->padInput.Buttons[pad]))
		? TRUE : FALSE;
	return 0;
}


//指定されたボタンの入力状態を
//キーボードのキーとゲームパッドのアナログスティックから決定する
//direction:PLUS/MINUS
//border 常に正の値で~1000とする
int controller::setAnalogButton(BUTTON button, int key, int padParam,BOOL direction,int border=350)
{
	this->button[static_cast<int>(button)] =
		(this->allKey[key] || (direction==PLUS ? padParam>border : padParam<-border)) 
		? TRUE : FALSE;
	return 0;
}

//指定されたボタンの入力状態を
//キーボードのキーとゲームパッドのハットスイッチから決定する
//directon: UP/RIGHT/DOWN/LEFT
int controller::setHatSwitch(BUTTON button, int key, int padParam, int direction)
{

	this->button[static_cast<int>(button)] =
		(this->allKey[key] ||
			((padParam==HAT_NO_INPUT) ? FALSE :
				(padParam== direction || padParam== direction+HAT_OFFSET || padParam==(direction-HAT_OFFSET+HAT_MAX)%HAT_MAX)))
				//(padParam+HAT_MAX >= direction - HAT_OFFSET+HAT_MAX && padParam+HAT_MAX <= direction + HAT_OFFSET+HAT_MAX)))
		? TRUE : FALSE;
	return 0;
}

//全てのゲーム内ボタンの入力状態を設定する
int controller::setInputAll()
{
	//全てのボタンを押していない状態にする
	for (int i = 0; i < BUTTON_MAX; i++)
	{
		this->button[i] = FALSE;
	}

	//各ボタンの設定
	this->setButton(BUTTON::A, KEY_INPUT_Z, 0);
	this->setButton(BUTTON::B, KEY_INPUT_X, 1);
	this->setButton(BUTTON::X, KEY_INPUT_C, 2);
	this->setButton(BUTTON::Y, KEY_INPUT_V, 3);
	this->setButton(BUTTON::LB, KEY_INPUT_LSHIFT, 4);
	this->setButton(BUTTON::RB, KEY_INPUT_RSHIFT, 5);
	this->setButton(BUTTON::BACK, KEY_INPUT_ESCAPE, 6);
	this->setButton(BUTTON::START, KEY_INPUT_SPACE, 7);
	this->setButton(BUTTON::LS, KEY_INPUT_Q, 8);
	this->setButton(BUTTON::RS, KEY_INPUT_O, 9);

	//スティック,レバー,ハットスイッチの設定
	//キーボードで指定したキーが押されているか,
	//スティックが一定以上傾いていると
	//ゲーム内ボタンの入力をオンにする

	//左スティック
	this->setAnalogButton(BUTTON::LSTICK_UP, KEY_INPUT_UP, this->padInput.Y,MINUS);
	this->setAnalogButton(BUTTON::LSTICK_RIGHT, KEY_INPUT_RIGHT, this->padInput.X,PLUS);
	this->setAnalogButton(BUTTON::LSTICK_DOWN, KEY_INPUT_DOWN, this->padInput.Y, PLUS);
	this->setAnalogButton(BUTTON::LSTICK_LEFT, KEY_INPUT_LEFT, this->padInput.X, MINUS);

	//右スティック
	this->setAnalogButton(BUTTON::RSTICK_UP, KEY_INPUT_W, this->padInput.Ry, MINUS);
	this->setAnalogButton(BUTTON::RSTICK_RIGHT, KEY_INPUT_D, this->padInput.Rx, PLUS);
	this->setAnalogButton(BUTTON::RSTICK_DOWN, KEY_INPUT_S, this->padInput.Ry, PLUS);
	this->setAnalogButton(BUTTON::RSTICK_LEFT, KEY_INPUT_A, this->padInput.Rx, MINUS);

	//トリガー
	this->setAnalogButton(BUTTON::LT, KEY_INPUT_LCONTROL, this->padInput.Z, PLUS);
	this->setAnalogButton(BUTTON::RT, KEY_INPUT_RCONTROL, this->padInput.Z, MINUS);

	//ハットスイッチ
	this->setHatSwitch(BUTTON::HAT_UP, KEY_INPUT_I, this->padInput.POV[0], HAT_UP);
	this->setHatSwitch(BUTTON::HAT_RIGHT, KEY_INPUT_L, this->padInput.POV[0], HAT_RIGHT);
	this->setHatSwitch(BUTTON::HAT_DOWN, KEY_INPUT_K, this->padInput.POV[0], HAT_DOWN);
	this->setHatSwitch(BUTTON::HAT_LEFT, KEY_INPUT_J, this->padInput.POV[0], HAT_LEFT);

	return 0;
}
*/

//XInput用
//GetJoypadXInputStateを自作のMYXINPUT_STATEに対応
int controller::myGetJoyPadXInputState(
	int input_type,
	DxLib::XINPUT_STATE *xinput,
	MYXINPUT_STATE *myxinput)
{
	int r = DxLib::GetJoypadXInputState(input_type, xinput);

	if (r != -1) {

		//値を複製する
		for (int i = 0; i < 16; i++)
		{
			myxinput->input[i] = (unsigned short)xinput->Buttons[i];
		}
		//3000は誤差の範囲とする
		myxinput->input[MYXINPUT_LT] = (unsigned short)xinput->LeftTrigger;
		myxinput->input[MYXINPUT_RT] = (unsigned short)xinput->RightTrigger;
		myxinput->input[MYXINPUT_LSTICK_RIGHT] = (unsigned short)(xinput->ThumbLX > 3000 ? xinput->ThumbLX : 0);
		myxinput->input[MYXINPUT_LSTICK_LEFT] = (unsigned short)(xinput->ThumbLX < -3000 ? -xinput->ThumbLX : 0);
		myxinput->input[MYXINPUT_LSTICK_UP] = (unsigned short)(xinput->ThumbLY > 3000 ? xinput->ThumbLY : 0);
		myxinput->input[MYXINPUT_LSTICK_DOWN] = (unsigned short)(xinput->ThumbLY < -3000 ? -xinput->ThumbLY : 0);
		myxinput->input[MYXINPUT_RSTICK_RIGHT] = (unsigned short)(xinput->ThumbRX > 3000 ? xinput->ThumbRX : 0);
		myxinput->input[MYXINPUT_RSTICK_LEFT] = (unsigned short)(xinput->ThumbRX < -3000 ? -xinput->ThumbRX : 0);
		myxinput->input[MYXINPUT_RSTICK_UP] = (unsigned short)(xinput->ThumbRY > 3000 ? xinput->ThumbRY : 0);
		myxinput->input[MYXINPUT_RSTICK_DOWN] = (unsigned short)(xinput->ThumbRY < -3000 ? -xinput->ThumbRY : 0);
	}
	else
	{
		for (int i = 0; i < 26; i++)
		{
			myxinput->input[i] = 0;
		}
	}

	return r;
}

//MYXINPUT_STATEの値を取得して,ボタンが押されているか判定する
int controller::setButtonFromMyXInput(
	BUTTON button,
	int key,
	int pad,
	int border = 35)
{
	//ボタン(0/1)
	if (pad <= 15)
	{
		if ((this->allKey[key] | (this->myXInput.input[pad])))
		{
			this->button[static_cast<int>(button)]++;
		}
		else
		{
			this->button[static_cast<int>(button)] = 0;
		}
	}
	//トリガー(0~255)
	else if (pad <= 17)
	{
		if ((this->allKey[key] | (this->myXInput.input[pad] >= TRIGGER_MAX / 100 * border)))
		{
			this->button[static_cast<int>(button)]++;
		}
		else
		{
			this->button[static_cast<int>(button)] = 0;
		}
	}
	//スティック(0~32767)
	else
	{
		if ((this->allKey[key] | (this->myXInput.input[pad] >= STICK_MAX / 100 * border)))
		{
			this->button[static_cast<int>(button)]++;
		}
		else
		{
			this->button[static_cast<int>(button)] = 0;
		}
	}
	return 0;
}

//XInput用
//指定されたボタンの入力状態を
//キーボードとゲームパッドの入力状態から決定する
int controller::setButton(
	BUTTON button,
	int key,
	int pad)
{
	//キーボードとボタンのうち、どちらかが押されていたらオンにする
	if ((this->allKey[key] | (this->myXInput.input[pad])))
	{
		this->button[static_cast<int>(button)]++;
	}
	else
	{
		this->button[static_cast<int>(button)]=0;
	}
	return 0;
}

//XInput用
//キーボードのキーとゲームパッドのアナログスティック入力状態から
//ゲーム内ボタンの入力状態を決定する
//direction: BOOL 
//boder:int 0~100で割合
int controller::setStick(
	BUTTON button,
	int key,
	short padParam,
	BOOL direction,
	int border=35)
{
	if ((this->allKey[key] |
		((direction == PLUS) ? (padParam > STICK_MAX / 100.0*border) : (padParam < -STICK_MAX / 100.0*border))))
	{
		this->button[static_cast<int>(button)]++;
	}
	else
	{
		this->button[static_cast<int>(button)] = 0;
	}
	return 0;
}

//XInput用
//キーボードのキーとゲームパッドのトリガーの入力状態から
//ゲーム内ボタンの入力状態を決定する
//border:int 0~100で割合
int controller::setTrigger(
	BUTTON button,
	int key,
	unsigned char padParam,
	int border=35)
{
	if ((this->allKey[key] |
		(padParam > TRIGGER_MAX / 100.0*border)))
	{
		this->button[static_cast<int>(button)]++;
	}
	else
	{
		this->button[static_cast<int>(button)] = 0;
	}
	return 0;
}

//XInput用
//全てのゲーム内ボタンの入力状態を設定する
int controller::setInputAll()
{
	//全てのボタンを押していない状態にする
	/*for (int i = 0; i < BUTTON_MAX; i++)
	{
		this->button[i] = FALSE;
	}*/

	//this->xInputHandle = DxLib::GetJoypadXInputState(DX_INPUT_PAD1, &this->xInput);
	this->xInputHandle = this->myGetJoyPadXInputState(DX_INPUT_PAD1, &this->xInput, &this->myXInput);
	


	for (int i = 0; i < BUTTON_MAX; i++)
	{
		//this->setButton(static_cast<BUTTON>(i), this->setKey[i], this->config[i]);
		this->setButtonFromMyXInput(static_cast<BUTTON>(i), this->setKey[i], this->config[i], 35);
	}

	return 0;
}

//アップデート関数
int controller::update()
{
	this->getAllKey();
	this->setInputAll();
	
#ifdef _DEBUG
	//入力状態の可視化
	for (int i = 0; i < BUTTON_MAX; i++)
	{
		DxLib::DrawFormatString(20 * i+500, 0, DxLib::GetColor(255, 255, 255), "%d", i);
		DxLib::DrawFormatString(20 * i+500, 16, DxLib::GetColor(255, 255, 255), "%i", (this->getButton(static_cast<BUTTON>(i) )));
	}
#endif
	return 0;
}