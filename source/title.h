#pragma once

namespace TITLE
{
	//定数
	const int SELECT_MAX = 5;//タイトル画面の選択肢の総数
	enum SELECT : int
	{
		GAMESTART,//ゲーム開始
		CONFIG,//キーコンフィグ
		//REPLAY,//リプレイ
		RANKING,//ランキング
		AUTOPLAY,//自動プレイ
		EXIT,//終了
	};
	enum class STATE : int
	{
		WIPE_S,//シーン突入時のワイプ画面
		SELECT,//メイン画面
		WIPE_F, //選択後のワイプ画面
	};

	const std::string TITLE_TEXT[SELECT_MAX] =
	{
		"GAME START",
		"CONFIG",
		"RANKING",
		"AUTO PLAY",
		"EXIT",
	};
}

class title
{
private:
	//参照
	game& cGame;
	controller& cController;

	//所持クラス
	titleBack *cTitleBack;//タイトル画面背景

	//変数
	int wipeCount;//ワイプカウンタ
	TITLE::STATE eState;//シーン状態

	int select;//現在選択中のメニュー

	//関数
	int draw();//描画総合
	void drawBack();//背景描画
	void drawUI();//UI描画
	void drawWipe();//ワイプ
	int input();//入力受付
public:
	title(game& c_Game,controller& c_Controller);
	~title();

	int initialize();//初期化
	int update();//更新
};

