#include "main.h"



effectMagicCircleTop::effectMagicCircleTop(
	game& c_Game):
	effectMagicCircle(c_Game)
{
	this->pos1[0] = VGet(-150, 200, -150);
	this->pos1[1] = VGet(150, 200, -150);
	this->pos1[2] = VGet(-150, 200, 150);
	this->pos1[3] = VGet(150, 200, 150);


	this->vertex1[0].norm = VGet(0, 1, 0);
	this->vertex1[0].spc = GetColorU8(255, 255, 255, 128);
	this->vertex1[0].dif = GetColorU8(255, 255, 255, 128);
	this->vertex1[0].u = 0.0f;
	this->vertex1[0].v = 0.0f;


	this->vertex1[1].norm = VGet(0, 1, 0);
	this->vertex1[1].spc = GetColorU8(255, 255, 255, 128);
	this->vertex1[1].dif = GetColorU8(255, 255, 255, 128);
	this->vertex1[1].u = 1.0f;
	this->vertex1[1].v = 0.0f;


	this->vertex1[2].norm = VGet(0, 1, 0);
	this->vertex1[2].spc = GetColorU8(255, 255, 255, 128);
	this->vertex1[2].dif = GetColorU8(255, 255, 255, 128);
	this->vertex1[2].u = 0.0f;
	this->vertex1[2].v = 1.0f;


	this->vertex1[3].norm = VGet(0, 1, 0);
	this->vertex1[3].spc = GetColorU8(255, 255, 255, 128);
	this->vertex1[3].dif = GetColorU8(255, 255, 255, 128);
	this->vertex1[3].u = 0.0f;
	this->vertex1[3].v = 0.0f;


	this->vertex1[4].norm = VGet(0, 1, 0);
	this->vertex1[4].spc = GetColorU8(255, 255, 255, 128);
	this->vertex1[4].dif = GetColorU8(255, 255, 255, 128);
	this->vertex1[4].u = 1.0f;
	this->vertex1[4].v = 0.0f;


	this->vertex1[5].norm = VGet(0, 1, 0);
	this->vertex1[5].spc = GetColorU8(255, 255, 255, 128);
	this->vertex1[5].dif = GetColorU8(255, 255, 255, 128);
	this->vertex1[5].u = 0.0f;
	this->vertex1[5].v = 1.0f;
}


effectMagicCircleTop::~effectMagicCircleTop()
{
}

int effectMagicCircleTop::update()
{
	if (this->cGame.getScene() == SCENE::CONFIG ||
		this->cGame.getScene() == SCENE::RANKING ||
		this->cGame.getScene() == SCENE::REPLAY ||
		this->cGame.getScene() == SCENE::TITLE)return 0;

	this->count++;

	this->rotMat = MGetRotY(PI / 180 * GetNowCount() / -450);
	this->scaleMat = MGetScale(VGet(
		1.0,//+ 0.05*cos(PI / 180 * this->count),
		1.0,//+ 0.05*cos(PI / 180 * this->count),
		1.0));//+ 0.05*cos(PI / 180 * this->count)));
	this->convertMat = MMult(this->scaleMat, this->rotMat);

	//���@�w�̕`��
	this->vertex1[0].pos = VTransform(this->pos1[0], convertMat);
	this->vertex1[1].pos = VTransform(this->pos1[1], convertMat);
	this->vertex1[2].pos = VTransform(this->pos1[2], convertMat);
	this->vertex1[3].pos = VTransform(this->pos1[3], convertMat);
	this->vertex1[4].pos = VTransform(this->pos1[1], convertMat);
	this->vertex1[5].pos = VTransform(this->pos1[2], convertMat);

	DrawPolygon3D(this->vertex1, 2, this->cGame.getImage().getHandle(IMAGE::IMAGES::MAGIC_CIRCLE2), TRUE);
	SetDrawBlendMode(DX_BLENDMODE_ADD, 255 * sin(PI / 180 * this->count));
	DrawPolygon3D(this->vertex1, 2, this->cGame.getImage().getHandle(IMAGE::IMAGES::MAGIC_CIRCLE2_FLASH), TRUE);
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

	if (this->count == -1)return -1;

	return 0;
}
