#include "main.h"



effectFieldPole::effectFieldPole(
	game& c_Game) :
	effectBase(c_Game)
{
	this->pos = VGet(-100, 100, -100);
	this->SCALE_X = 200.0f;
	this->SCALE_Y = 1.0f;
}


effectFieldPole::~effectFieldPole()
{
}

int effectFieldPole::update()
{
	if (this->cGame.getScene() == SCENE::CONFIG ||
		this->cGame.getScene() == SCENE::RANKING ||
		this->cGame.getScene() == SCENE::REPLAY ||
		this->cGame.getScene() == SCENE::TITLE)return 0;

	this->count++;

	for (int w = 0; w < FIELD_SPACE_W; w++)
	{
		this->rotMat = MGetRotY(PI / (FIELD_SPACE_W + 1)*(w));

		VECTOR dpos = VTransform(this->pos, this->rotMat);

		SetDrawBlendMode(DX_BLENDMODE_ADD, 255);

		DrawBillboard3D(dpos, 0.5f, 0.5f, this->SCALE_X, 0.0f, this->cGame.getImage().getHandle(IMAGE::IMAGES::FIELD_POLE), TRUE);

		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	}

	return 0;
}
