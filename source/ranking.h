#pragma once

namespace RANKING
{
	//定数
	const int RANK_MAX = 10;

	//ランキングデータ構造体
	struct USER_DATA
	{
		int score;//得点
	};

	//状態
	enum class STATE :int
	{
		WIPE_S,
		MAIN,
		WIPE_F,
	};
}

class ranking
{
private:
	//参照
	game& cGame;
	controller& cController;

	RANKING::USER_DATA sRanking[RANKING::RANK_MAX];//ランキング
	RANKING::STATE state;//シーン状態

	int wipeCount;//ワイプカウンタ
	int backHandle;//背景の画像ハンドル

	void sortRank();//ランキングの並べ替え

	int input();//入力
	int load();//ランキングの読み込み
	int save();//ランキングの保存
	int draw();//描画
	void drawWipe();//ワイプ描画
public:
	ranking(game&,controller&);
	~ranking();

	int initialize();//初期化
	int update();//更新
	int addUser(
		RANKING::USER_DATA&);//ランキングに登録
	int getBackHandle() { return this->backHandle; }
};

