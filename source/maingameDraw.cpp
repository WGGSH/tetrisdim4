#include "main.h"

//闇の3D描画関数たち

//地面の描画(闇)
void maingame::drawGround()
{
	VECTOR pos1, pos2, pos3;
	MATRIX rotmat;
	int line_color = helper::GetColorHSBtoRGB(this->count % 360, 1, 0.25);

	//床
	for (int w = 0; w < FIELD_SIZE_W; w++)
	{
		rotmat = DxLib::MGetRotY(PI2 / (FIELD_SPACE_W + 1)*(w));
		//床の頂点設定
		if (w >= 1 && w <= FIELD_SPACE_W)
		{
			pos1 = DxLib::VGet(-40, 0, -40); pos1 = DxLib::VTransform(pos1, rotmat);
			pos2 = DxLib::VGet(-140, 0, -40); pos2 = DxLib::VTransform(pos2, rotmat);
			pos3 = DxLib::VGet(-40, 0, -140); pos3 = DxLib::VTransform(pos3, rotmat);
			DxLib::DrawTriangle3D(pos1, pos2, pos3, GetColor(255, 255, 255), TRUE);
			pos1 = DxLib::VGet(-140, 0, -140); pos1 = DxLib::VTransform(pos1, rotmat);
			DxLib::DrawTriangle3D(pos1, pos2, pos3, GetColor(255, 255, 255), TRUE);

			//床のライン描画
			for (int x = 0; x < FIELD_SPACE_X; x++) {
				pos1 = DxLib::VGet(
					-40 - 20 * x,
					0,
					-40); pos1 = DxLib::VTransform(pos1, rotmat);
				pos2 = DxLib::VGet(
					-40 - 20 * x,
					0,
					-140); pos2 = DxLib::VTransform(pos2, rotmat);
				DxLib::DrawLine3D(pos1, pos2, line_color);
				pos1 = DxLib::VGet(
					-40,
					0,
					-40 - 20 * x); pos1 = DxLib::VTransform(pos1, rotmat);
				pos2 = DxLib::VGet(
					-140,
					0,
					-40 - 20 * x); pos2 = DxLib::VTransform(pos2, rotmat);
				DxLib::DrawLine3D(pos1, pos2, line_color);
			}

		}

		//ブロックの描画
		rotmat = DxLib::MGetRotY(PI2 / (FIELD_SPACE_W + 1)*(w));
		for (int x = 0; x < FIELD_SIZE_X; x++) {
			for (int y = 0; y < FIELD_SIZE_Y; y++) {
				for (int z = 0; z < FIELD_SIZE_Z; z++) {
					if (this->field[x][y][z][w] != static_cast<int>(BLOCK_TYPE::NON) &&
						this->field[x][y][z][w] != static_cast<int>(BLOCK_TYPE::GROUND))
					{
						int color = helper::GetColorHSBtoRGB(
							360 / TETROTESSERACT_MAX * this->field[x][y][z][w], 1, 1);
						this->drawBlock3D(position::set(x, y, z, w), color, this->field);
					}

				}
			}
		}


	}

}

//ゴーストの描画
void maingame::drawGhost()
{
	MATRIX rotmat2;

	//ゴーストの描画
	for (int z2 = 0; z2 < FIELD_SIZE_Z; z2++)
	{
		if (this->check(this->field, this->nowBlock, this->location + position::set(0, 0, z2, 0)) == FALSE)
		{
			//ゴーストブロックの描画
			for (int w = 0; w < TESSERACT_SIZE; w++) {
				rotmat2 = DxLib::MGetRotY(PI2 / (FIELD_SPACE_W + 1)*(w + this->location.w));
				for (int x = 0; x < TESSERACT_SIZE; x++) {
					for (int y = 0; y < TESSERACT_SIZE; y++) {
						for (int z = 0; z < TESSERACT_SIZE; z++) {
							if (this->nowBlock[x][y][z][w] != static_cast<int>(BLOCK_TYPE::NON))
							{
								int color = helper::GetColorHSBtoRGB(
									360 / TETROTESSERACT_MAX*this->nowBlock[x][y][z][w], 1, 0.25);

								//ここからクソコード
								this->drawBlock3D(
									position::set(this->location.x + x, this->location.y + y,
										this->location.z + z2 + z - 1, this->location.w + w),
									color, this->field);
							}

						}
					}
				}
			}

			break;
		}
	}

}

//落下中のブロックの描画
void maingame::drawDropBlock()
{
	int line_color = helper::GetColorHSBtoRGB(this->count % 360, 1, 0.25);
	MATRIX rotmat2;

	//落下中のブロックの描画
	for (int w = 0; w < TESSERACT_SIZE; w++) {
		rotmat2 = DxLib::MGetRotY(PI2 / (FIELD_SPACE_W + 1)*(w + this->location.w));
		for (int x = 0; x < TESSERACT_SIZE; x++) {
			for (int y = 0; y < TESSERACT_SIZE; y++) {
				for (int z = 0; z < TESSERACT_SIZE; z++) {
					if (this->nowBlock[x][y][z][w] != static_cast<int>(BLOCK_TYPE::NON))
					{
						int color = helper::GetColorHSBtoRGB(
							360 / TETROTESSERACT_MAX * this->nowBlock[x][y][z][w], 1, 1);
						this->drawBlock3D(position::set(this->location.x + x, this->location.y + y,
							this->location.z + z, this->location.w + w),
							color, this->field);
						this->drawBlockLine3D(position::set(this->location.x + x, this->location.y + y,
							this->location.z + z, this->location.w + w),
							line_color, this->field);

					}

				}
			}
		}
	}
}

/*
//全てのブロックのフレームを描画(闇)
void maingame::drawBlockFrameAll()
{
	MATRIX rotmat2;

	//ブロックの線を描画する闇の処理を纏める
	BOOL flag = TRUE;
	for (int w = 1; w < FIELD_SIZE_W - 1; w++)
	{
		rotmat2 = DxLib::MGetRotY(PI2 / (FIELD_SPACE_W + 1)*(w));
		//上から
		for (int x = 1; x < FIELD_SIZE_X - 1; x++) {
			for (int y = 1; y < FIELD_SIZE_Y - 1; y++) {
				flag = TRUE;
				for (int z = 1; z < FIELD_SIZE_Z - 1; z++) {
					if (this->field[x][y][z][w] % static_cast<int>(BLOCK_TYPE::GROUND) != 0)
					{
						if (flag == TRUE)
						{
							//描画
							this->drawBlockFrame_Z(&position::set(x, y, z, w), 0x000000, &rotmat2);
							this->drawBlockFrame_X(&position::set(x, y, z, w), 0x000000, &rotmat2);
							flag = FALSE;
						}

					}
					else
					{
						flag = TRUE;
					}
				}
			}
		}

		//横(X)から
		for (int y = 0; y < FIELD_SIZE_Y; y++) {
			for (int z = 0; z < FIELD_SIZE_Z; z++) {
				flag = TRUE;
				for (int x = 1; x < FIELD_SIZE_X; x++) {
					if (this->field[x][y][z][w] % static_cast<int>(BLOCK_TYPE::GROUND) != 0)
					{
						if (flag == TRUE)
						{
							//描画
							this->drawBlockFrame_Z(&position::set(x, y, z, w), 0x000000, &rotmat2);
							this->drawBlockFrame_Y(&position::set(x, y, z, w), 0x000000, &rotmat2);
							flag = FALSE;
						}

					}
					else
					{
						flag = TRUE;
					}
				}
			}
		}

		//横(Y)から
		for (int z = 0; z < FIELD_SIZE_Z; z++) {
			for (int x = 1; x < FIELD_SIZE_X; x++) {
				flag = TRUE;
				for (int y = 0; y < FIELD_SIZE_Y; y++) {
					if (this->field[x][y][z][w] % static_cast<int>(BLOCK_TYPE::GROUND) != 0)
					{
						if (flag == TRUE)
						{
							//描画
							this->drawBlockFrame_Y(&position::set(x, y, z, w), 0x000000, &rotmat2);
							this->drawBlockFrame_X(&position::set(x, y, z, w), 0x000000, &rotmat2);
							flag = FALSE;
						}

					}
					else
					{
						flag = TRUE;
					}
				}
			}
		}

		//後方から1回だけ
		//下は不要
		for (int z = 1; z < FIELD_SIZE_Z - 1; z++) {
			//X
			for (int y = 1; y < FIELD_SIZE_Y - 1; y++) {
				for (int x = FIELD_SIZE_X - 1; x >= 1; x--)
				{
					if (this->field[x][y][z][w] % static_cast<int>(BLOCK_TYPE::GROUND) != 0)
					{
						this->drawBlockFrame_Y(&position::set(x + 1, y, z, w), 0x000000, &rotmat2);
						this->drawBlockFrame_Z(&position::set(x + 1, y, z, w), 0x000000, &rotmat2);
						break;
					}
				}
			}

			for (int x = 1; x < FIELD_SIZE_X - 1; x++) {
				for (int y = FIELD_SIZE_Y - 1; y >= 1; y--)
				{
					if (this->field[x][y][z][w] % static_cast<int>(BLOCK_TYPE::GROUND) != 0)
					{
						this->drawBlockFrame_Y(&position::set(x, y + 1, z, w), 0x000000, &rotmat2);
						this->drawBlockFrame_X(&position::set(x, y + 1, z, w), 0x000000, &rotmat2);
						break;
					}
				}
			}
		}

	}
}
*/

//全てのブロックのフレームを描画(闇)
void maingame::drawBlockFrameAll()
{
	MATRIX rotmat2;

	//ブロックの線を描画する闇の処理を纏める
	BOOL flag = TRUE;
	for (int w = 1; w < FIELD_SIZE_W - 1; w++)
	{
		rotmat2 = DxLib::MGetRotY(PI2 / (FIELD_SPACE_W + 1)*(w));
		
		//上から
		for (int x = 1; x < FIELD_SIZE_X - 1; x++) {
			for (int y = 1; y < FIELD_SIZE_Y - 1; y++) {
				flag = TRUE;
				for (int z = 1; z < FIELD_SIZE_Z - 1; z++) {
					if (this->field[x][y][z][w] % static_cast<int>(BLOCK_TYPE::GROUND) != 0)
					{
						if (flag == TRUE)
						{
							//描画
							this->drawBlockFrame_Z(&position::set(x, y, z, w), 0x000000, &rotmat2);
							this->drawBlockFrame_X(&position::set(x, y, z, w), 0x000000, &rotmat2);
							flag = FALSE;
						}

					}
					else
					{
						flag = TRUE;
					}
				}
			}
		}

		//横(X)から
		for (int y = 0; y < FIELD_SIZE_Y; y++) {
			for (int z = 0; z < FIELD_SIZE_Z; z++) {
				flag = TRUE;
				for (int x = 1; x < FIELD_SIZE_X; x++) {
					if (this->field[x][y][z][w] % static_cast<int>(BLOCK_TYPE::GROUND) != 0)
					{
						if (flag == TRUE)
						{
							//描画
							this->drawBlockFrame_Z(&position::set(x, y, z, w), 0x000000, &rotmat2);
							this->drawBlockFrame_Y(&position::set(x, y + 1, z, w), 0x000000, &rotmat2);
							flag = FALSE;
						}

					}
					else
					{
						flag = TRUE;
					}
				}
			}
		}

		//横(Y)から
		for (int z = 0; z < FIELD_SIZE_Z; z++) {
			for (int x = 1; x < FIELD_SIZE_X; x++) {
				flag = TRUE;
				for (int y = 0; y < FIELD_SIZE_Y; y++) {
					if (this->field[x][y][z][w] % static_cast<int>(BLOCK_TYPE::GROUND) != 0)
					{
						if (flag == TRUE)
						{
							//描画
							this->drawBlockFrame_Y(&position::set(x, y, z, w), 0x000000, &rotmat2);
							this->drawBlockFrame_X(&position::set(x , y, z, w), 0x000000, &rotmat2);
							flag = FALSE;
						}

					}
					else
					{
						flag = TRUE;
					}
				}
			}
		}

		//後方から1回だけ
		//下は不要
		for (int z = 1; z < FIELD_SIZE_Z - 1; z++) {
			//X
			for (int y = 1; y < FIELD_SIZE_Y - 1; y++) {
				flag = TRUE;
				for (int x = FIELD_SIZE_X - 1; x >= 1; x--)
				{
					if (this->field[x][y][z][w] % static_cast<int>(BLOCK_TYPE::GROUND) != 0)
					{
						if (flag == TRUE) {
							this->drawBlockFrame_Y(&position::set(x + 1, y, z, w), 0x000000, &rotmat2);
							this->drawBlockFrame_Z(&position::set(x + 1, y, z, w), 0x000000, &rotmat2);
							flag = FALSE;
							//break;
						}
					}
					else
					{
						flag = TRUE;
					}

				}
			}

			for (int x = 1; x < FIELD_SIZE_X - 1; x++) {
				flag = TRUE;
				for (int y = FIELD_SIZE_Y - 1; y >= 1; y--)
				{
					if (this->field[x][y][z][w] % static_cast<int>(BLOCK_TYPE::GROUND) != 0)
					{
						if (flag == TRUE) {
							this->drawBlockFrame_Y(&position::set(x + 1, y + 1, z, w), 0x000000, &rotmat2);
							this->drawBlockFrame_X(&position::set(x, y + 1, z, w), 0x000000, &rotmat2);
							flag = FALSE;
						}
					}
					else
					{
						flag = TRUE;
					}
				}
			}
		}

	}
}


//ブロックの縁の線を描画する関数(X)(闇)
void maingame::drawBlockFrame_X(
	const position *pos, int color, const MATRIX *mat)
{
	DxLib::DrawLine3D(
		DxLib::VTransform(DxLib::VGet(
			-20 - 20 * pos->x,
			220 - 20 * pos->z,
			-20 - 20 * pos->y), *mat),
		DxLib::VTransform(DxLib::VGet(
			-20 - 20 * (pos->x + 1),
			220 - 20 * pos->z,
			-20 - 20 * pos->y), *mat),
		color);
}

//ブロックの縁の線を描画する関数(Y)(闇)
void maingame::drawBlockFrame_Y(
	const position *pos, int color, const MATRIX *mat)
{
	DxLib::DrawLine3D(
		DxLib::VTransform(DxLib::VGet(
			-20 - 20 * pos->x,
			220 - 20 * pos->z,
			-20 - 20 * pos->y), *mat),
		DxLib::VTransform(DxLib::VGet(
			-20 - 20 * pos->x,
			220 - 20 * (pos->z + 1),
			-20 - 20 * pos->y), *mat),
		color);
}

//ブロックの縁の線を描画する関数(Z)(闇)
void maingame::drawBlockFrame_Z(
	const position *pos, int color, const MATRIX *mat)
{
	DxLib::DrawLine3D(
		DxLib::VTransform(DxLib::VGet(
			-20 - 20 * pos->x,
			220 - 20 * pos->z,
			-20 - 20 * pos->y), *mat),
		DxLib::VTransform(DxLib::VGet(
			-20 - 20 * pos->x,
			220 - 20 * pos->z,
			-20 - 20 * (pos->y + 1)), *mat),
		color);
}

//ブロックを描画する闇の関数
//position:描画対象の座標
//int:色
//int[][][][]:フィールド
void maingame::drawBlock3D(
	position pos, int color, 
	int field[FIELD_SIZE_X][FIELD_SIZE_Y][FIELD_SIZE_Z][FIELD_SIZE_W])
{
	VECTOR pos1, pos2, pos3;
	MATRIX rotmat2;
	int x = pos.x, y = pos.y, z = pos.z, w = pos.w;
	rotmat2 = DxLib::MGetRotY(PI2 / (FIELD_SPACE_W + 1)*(w));

	int line_color = helper::GetColorHSBtoRGB(this->count % 360, 1, 0.25);
	//ここからクソコード

	//上面(Y-)
	if (field[x][y][z - 1][w] % static_cast<int>(BLOCK_TYPE::GROUND) == 0)
	{
		{
			pos1 = DxLib::VGet(
				-20 - 20 * (x),
				220 - 20 * (z),
				-20 - 20 * (y));
			pos1 = (DxLib::VTransform(pos1, rotmat2));
			pos2 = DxLib::VGet(
				-20 - 20 * (x + 1),
				220 - 20 * (z),
				-20 - 20 * (y));
			pos2 = (DxLib::VTransform(pos2, rotmat2));
			pos3 = DxLib::VGet(
				-20 - 20 * (x),
				220 - 20 * (z),
				-20 - 20 * (y + 1));
			pos3 = (DxLib::VTransform(pos3, rotmat2));
			DxLib::DrawTriangle3D(pos1, pos2, pos3, color, TRUE);
			pos1 = DxLib::VGet(
				-20 - 20 * (x + 1),
				220 - 20 * (z),
				-20 - 20 * (y + 1));
			pos1 = (DxLib::VTransform(pos1, rotmat2));
			DxLib::DrawTriangle3D(pos1, pos2, pos3, color, TRUE);
		}
	}
	//下面(Y+)
	if (field[x][y][z + 1][w] % static_cast<int>(BLOCK_TYPE::GROUND) == 0)
	{
		{
			pos1 = DxLib::VGet(
				-20 - 20 * (x),
				220 - 20 * (z + 1),
				-20 - 20 * (y));
			pos1 = (DxLib::VTransform(pos1, rotmat2));
			pos2 = DxLib::VGet(
				-20 - 20 * (x + 1),
				220 - 20 * (z + 1),
				-20 - 20 * (y));
			pos2 = (DxLib::VTransform(pos2, rotmat2));
			pos3 = DxLib::VGet(
				-20 - 20 * (x),
				220 - 20 * (z + 1),
				-20 - 20 * (y + 1));
			pos3 = (DxLib::VTransform(pos3, rotmat2));
			DxLib::DrawTriangle3D(pos1, pos2, pos3, color, TRUE);
			pos1 = DxLib::VGet(
				-20 - 20 * (x + 1),
				220 - 20 * (z + 1),
				-20 - 20 * (y + 1));
			pos1 = (DxLib::VTransform(pos1, rotmat2));
			DxLib::DrawTriangle3D(pos1, pos2, pos3, color, TRUE);
		}
	}
	//横(X-)
	if (field[x - 1][y][z][w] % static_cast<int>(BLOCK_TYPE::GROUND) == 0)
	{
		{
			pos1 = DxLib::VGet(
				-20 - 20 * (x),
				220 - 20 * (z),
				-20 - 20 * (y));
			pos1 = (DxLib::VTransform(pos1, rotmat2));
			pos2 = DxLib::VGet(
				-20 - 20 * (x),
				220 - 20 * (z + 1),
				-20 - 20 * (y));
			pos2 = (DxLib::VTransform(pos2, rotmat2));
			pos3 = DxLib::VGet(
				-20 - 20 * (x),
				220 - 20 * (z),
				-20 - 20 * (y + 1));
			pos3 = (DxLib::VTransform(pos3, rotmat2));
			DxLib::DrawTriangle3D(pos1, pos2, pos3, color, TRUE);
			pos1 = DxLib::VGet(
				-20 - 20 * (x),
				220 - 20 * (z + 1),
				-20 - 20 * (y + 1));
			pos1 = (DxLib::VTransform(pos1, rotmat2));
			DxLib::DrawTriangle3D(pos1, pos2, pos3, color, TRUE);
		}
	}
	//横(X+)
	if (field[x + 1][y][z][w] % static_cast<int>(BLOCK_TYPE::GROUND) == 0)
	{
		{
			pos1 = DxLib::VGet(
				-20 - 20 * (x + 1),
				220 - 20 * (z),
				-20 - 20 * (y));
			pos1 = (DxLib::VTransform(pos1, rotmat2));
			pos2 = DxLib::VGet(
				-20 - 20 * (x + 1),
				220 - 20 * (z + 1),
				-20 - 20 * (y));
			pos2 = (DxLib::VTransform(pos2, rotmat2));
			pos3 = DxLib::VGet(
				-20 - 20 * (x + 1),
				220 - 20 * (z),
				-20 - 20 * (y + 1));
			pos3 = (DxLib::VTransform(pos3, rotmat2));
			DxLib::DrawTriangle3D(pos1, pos2, pos3, color, TRUE);
			pos1 = DxLib::VGet(
				-20 - 20 * (x + 1),
				220 - 20 * (z + 1),
				-20 - 20 * (y + 1));
			pos1 = (DxLib::VTransform(pos1, rotmat2));
			DxLib::DrawTriangle3D(pos1, pos2, pos3, color, TRUE);
		}
	}
	//横(Z-)
	if (field[x][y - 1][z][w] % static_cast<int>(BLOCK_TYPE::GROUND) == 0)
	{
		{
			pos1 = DxLib::VGet(
				-20 - 20 * (x),
				220 - 20 * (z),
				-20 - 20 * (y));
			pos1 = (DxLib::VTransform(pos1, rotmat2));
			pos2 = DxLib::VGet(
				-20 - 20 * (x + 1),
				220 - 20 * (z),
				-20 - 20 * (y));
			pos2 = (DxLib::VTransform(pos2, rotmat2));
			pos3 = DxLib::VGet(
				-20 - 20 * (x),
				220 - 20 * (z + 1),
				-20 - 20 * (y));
			pos3 = (DxLib::VTransform(pos3, rotmat2));
			DxLib::DrawTriangle3D(pos1, pos2, pos3, color, TRUE);
			pos1 = DxLib::VGet(
				-20 - 20 * (x + 1),
				220 - 20 * (z + 1),
				-20 - 20 * (y));
			pos1 = (DxLib::VTransform(pos1, rotmat2));
			DxLib::DrawTriangle3D(pos1, pos2, pos3, color, TRUE);
		}
	}
	//横(Z+)
	if (field[x][y + 1][z][w] % static_cast<int>(BLOCK_TYPE::GROUND) == 0)
	{
		{
			pos1 = DxLib::VGet(
				-20 - 20 * (x),
				220 - 20 * (z),
				-20 - 20 * (y + 1));
			pos1 = (DxLib::VTransform(pos1, rotmat2));
			pos2 = DxLib::VGet(
				-20 - 20 * (x + 1),
				220 - 20 * (z),
				-20 - 20 * (y + 1));
			pos2 = (DxLib::VTransform(pos2, rotmat2));
			pos3 = DxLib::VGet(
				-20 - 20 * (x),
				220 - 20 * (z + 1),
				-20 - 20 * (y + 1));
			pos3 = (DxLib::VTransform(pos3, rotmat2));
			DxLib::DrawTriangle3D(pos1, pos2, pos3, color, TRUE);
			pos1 = DxLib::VGet(
				-20 - 20 * (x + 1),
				220 - 20 * (z + 1),
				-20 - 20 * (y + 1));
			pos1 = (DxLib::VTransform(pos1, rotmat2));
			DxLib::DrawTriangle3D(pos1, pos2, pos3, color, TRUE);
		}
	}

}

//ブロックから壁に向かって引くリード線を描画する闇の関数
//position:描画対象の座標
//int:色
//int[][][][]:フィールド
void maingame::drawBlockLine3D(
	position pos, int color,
	int field[FIELD_SIZE_X][FIELD_SIZE_Y][FIELD_SIZE_Z][FIELD_SIZE_W])
{

	VECTOR pos1, pos2, pos3;
	MATRIX rotmat2;
	int x = pos.x, y = pos.y, z = pos.z, w = pos.w;
	rotmat2 = DxLib::MGetRotY(PI2 / (FIELD_SPACE_W + 1)*(w));

	//ラインの描画
	//下方
	{
		//下方1
		pos1 = DxLib::VGet(
			-20 - 20 * (x),
			220 - 20 * (z),
			-20 - 20 * (y));
		pos1 = DxLib::VTransform(pos1, rotmat2);
		pos2 = DxLib::VGet(
			-20 - 20 * (x),
			0,
			-20 - 20 * (y));
		pos2 = DxLib::VTransform(pos2, rotmat2);
		DxLib::DrawLine3D(pos1, pos2, color);

		//下方2
		pos1 = DxLib::VGet(
			-20 - 20 * (x + 1),
			220 - 20 * (z),
			-20 - 20 * (y));
		pos1 = DxLib::VTransform(pos1, rotmat2);
		pos2 = DxLib::VGet(
			-20 - 20 * (x + 1),
			0,
			-20 - 20 * (y));
		pos2 = DxLib::VTransform(pos2, rotmat2);
		DxLib::DrawLine3D(pos1, pos2, color);

		//下方3
		pos1 = DxLib::VGet(
			-20 - 20 * (x),
			220 - 20 * (z),
			-20 - 20 * (y + 1));
		pos1 = DxLib::VTransform(pos1, rotmat2);
		pos2 = DxLib::VGet(
			-20 - 20 * (x),
			0,
			-20 - 20 * (y + 1));
		pos2 = DxLib::VTransform(pos2, rotmat2);
		DxLib::DrawLine3D(pos1, pos2, color);

		//下方4
		pos1 = DxLib::VGet(
			-20 - 20 * (x + 1),
			220 - 20 * (z),
			-20 - 20 * (y + 1));
		pos1 = DxLib::VTransform(pos1, rotmat2);
		pos2 = DxLib::VGet(
			-20 - 20 * (x + 1),
			0,
			-20 - 20 * (y + 1));
		pos2 = DxLib::VTransform(pos2, rotmat2);
		DxLib::DrawLine3D(pos1, pos2, color);
	}
	//横1
	{
		//X-1
		pos1 = DxLib::VGet(
			-140,
			220 - 20 * (z),
			-20 - 20 * (y));
		pos1 = DxLib::VTransform(pos1, rotmat2);
		pos2 = DxLib::VGet(
			-40,
			220 - 20 * (z),
			-20 - 20 * (y));
		pos2 = DxLib::VTransform(pos2, rotmat2);
		DxLib::DrawLine3D(pos1, pos2, color);

		//X-
		pos1 = DxLib::VGet(
			-140,
			220 - 20 * (z + 1),
			-20 - 20 * (y));
		pos1 = DxLib::VTransform(pos1, rotmat2);
		pos2 = DxLib::VGet(
			-40,
			220 - 20 * (z + 1),
			-20 - 20 * (y));
		pos2 = DxLib::VTransform(pos2, rotmat2);
		DxLib::DrawLine3D(pos1, pos2, color);

		//X-1
		pos1 = DxLib::VGet(
			-140,
			220 - 20 * (z),
			-20 - 20 * (y + 1));
		pos1 = DxLib::VTransform(pos1, rotmat2);
		pos2 = DxLib::VGet(
			-40,
			220 - 20 * (z),
			-20 - 20 * (y + 1));
		pos2 = DxLib::VTransform(pos2, rotmat2);
		DxLib::DrawLine3D(pos1, pos2, color);

		//X-
		pos1 = DxLib::VGet(
			-140,
			220 - 20 * (z + 1),
			-20 - 20 * (y + 1));
		pos1 = DxLib::VTransform(pos1, rotmat2);
		pos2 = DxLib::VGet(
			-40,
			220 - 20 * (z + 1),
			-20 - 20 * (y + 1));
		pos2 = DxLib::VTransform(pos2, rotmat2);
		DxLib::DrawLine3D(pos1, pos2, color);
	}
	//横2
	{
		//Y-1
		pos1 = DxLib::VGet(
			-20 - 20 * (x),
			220 - 20 * (z),
			-40);
		pos1 = DxLib::VTransform(pos1, rotmat2);
		pos2 = DxLib::VGet(
			-20 - 20 * (x),
			220 - 20 * (z),
			-140);
		pos2 = DxLib::VTransform(pos2, rotmat2);
		DxLib::DrawLine3D(pos1, pos2, color);

		//X-
		pos1 = DxLib::VGet(
			-20 - 20 * (x + 1),
			220 - 20 * (z),
			-40);
		pos1 = DxLib::VTransform(pos1, rotmat2);
		pos2 = DxLib::VGet(
			-20 - 20 * (x + 1),
			220 - 20 * (z),
			-140);
		pos2 = DxLib::VTransform(pos2, rotmat2);
		DxLib::DrawLine3D(pos1, pos2, color);

		//Y-1
		pos1 = DxLib::VGet(
			-20 - 20 * (x),
			220 - 20 * (z + 1),
			-40);
		pos1 = DxLib::VTransform(pos1, rotmat2);
		pos2 = DxLib::VGet(
			-20 - 20 * (x),
			220 - 20 * (z + 1),
			-140);
		pos2 = DxLib::VTransform(pos2, rotmat2);
		DxLib::DrawLine3D(pos1, pos2, color);

		//X-
		pos1 = DxLib::VGet(
			-20 - 20 * (x + 1),
			220 - 20 * (z + 1),
			-40);
		pos1 = DxLib::VTransform(pos1, rotmat2);
		pos2 = DxLib::VGet(
			-20 - 20 * (x + 1),
			220 - 20 * (z + 1),
			-140);
		pos2 = DxLib::VTransform(pos2, rotmat2);
		DxLib::DrawLine3D(pos1, pos2, color);
	}
}
