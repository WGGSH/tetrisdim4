#pragma once
#include "maingame.h"

namespace REPLAY
{
	//リプレイデータ構造体
	struct REPLAYDATA
	{
		int srand;//初期乱数
		int frames;//総フレーム数
		std::vector<int*> inputList;//入力
		int *input[BUTTON_MAX];
	};
}

class replay :
	public maingame
{
private:
	REPLAY::REPLAYDATA *sReplayData;//リプレイデータ

	int over();//ゲームオーバー処理
public:
	replay(game&,controller&);
	~replay();

	int initialize();//初期化
	int update();//更新
	void load();//読み込み
};

