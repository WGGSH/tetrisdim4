#pragma once

//#include "tetrotesseract.h"

namespace MAINGAME
{
	//定数
	const int TESSERACT_SIZE = 4;//1つのテトリミノを格納する胞の1辺の大きさ
	const int AUTO_MOVE_COUNT = 12;//キー押しっぱなしで移動し続けるまでの猶予フレーム
	//フィールドの大きさ
	const int FIELD_SPACE_X = 5;
	const int FIELD_SPACE_Y = 5;
	const int FIELD_SPACE_Z = 10;
	const int FIELD_SPACE_W = 5;
	const int FIELD_SIZE_X = FIELD_SPACE_X+2;
	const int FIELD_SIZE_Y = FIELD_SPACE_Y + 2;
	const int FIELD_SIZE_Z = FIELD_SPACE_Z + 2;
	const int FIELD_SIZE_W = FIELD_SPACE_W + 2;

	const int TETROTESSERACT_MAX = 7;//テトリミノの種類総数
	//ブロックの種類
	enum class BLOCK_TYPE
	{
		NON,//空きスペース
		TESSERACT_1,
		TESSERACT_2,
		TESSERACT_3,
		TESSERACT_4,
		TESSERACT_5,
		TESSERACT_6,
		TESSERACT_7,
		GROUND,//壁,地面
	};

	enum class GAME_STATE
	{
		DROP,//落下中
		DELETING,//ブロック削除中//使わなさそう
	};
	
	//ゲーム状態
	enum class STATE
	{
		WIPE_S,//シーン突入時
		PLAYING,//ゲームプレイ中
		PAUSE,//ポーズ画面
		GAMEOVER_ANIM,//ゲームオーバー(アニメーション中)
		GAMEOVER_SELECT,//ゲームオーバー(リザルト中)
		WIPE_F,//シーン脱出時
	};

	//カメラ構造体
	struct CAMERA
	{
		DxLib::VECTOR position;//カメラの座標
		//角度1
		float phi;
		float phi_vel;
		float phi_accel;
		//角度2
		float theta;
		float theta_vel;
		float theta_accel;
		//距離
		float length;
		float length_vel;
		float length_accel;

		//座標計算処理
		void setPosition()
		{
			this->position = DxLib::VGet(
				this->length*cos(this->phi)*cos(this->theta),
				this->length*sin(this->theta),
				this->length*sin(this->phi)*cos(this->theta));
			DxLib::SetCameraPositionAndTargetAndUpVec(
				this->position,
				DxLib::VGet(0, 0, 0),
				DxLib::VGet(0, 1, 0));
		}
	};

	//ゲームオーバー画面の文字列
	const int GAMEOVER_TEXT_MAX = 2;
	const std::string GAMEOVER_STRING[GAMEOVER_TEXT_MAX]{
		"TWEET",
		"TITLE",
	};
}


using namespace MAINGAME;

class maingame
{
private:
	//record *cRecord;//リプレイデータ
protected:
	//参照
	game& cGame;
	controller& cController;

	//エフェクト
	std::vector<std::shared_ptr<effectBase>> effectList;

	//ゲーム状態
	GAME_STATE eGameState;
	STATE eState;

	int wipeCount;//ワイプ用のカウンタ

	int backHandle;//背景画像ハンドル

	int count;//ゲームカウント
	int score;//得点

	int blockCount[MAINGAME::TETROTESSERACT_MAX];//各ブロックの出現回数

	int field[FIELD_SIZE_X][FIELD_SIZE_Y][FIELD_SIZE_Z][FIELD_SIZE_W];//フィールド
	int nowBlock[TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE];//ブロックを格納する胞
	position location;//現在座標

	MAINGAME::CAMERA camera;//カメラ

	//関数
	//ゲーム内処理全般
	virtual int create();
	virtual int over();//ゲームオーバー判定
	int move(
		position);//移動関数
	int lock();//落下完了
	int del(
		int);//削除
	int check(
		int[FIELD_SIZE_X][FIELD_SIZE_Y][FIELD_SIZE_Z][FIELD_SIZE_W],
		int[TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE], 
		position);//判定
	int rotate(
		axis4d& angle);//回転

	//入力全般
	int input();
	//移動入力
	int moveInput(
		BUTTON,
		position&, 
		position&);
	//回転入力
	int rotateInput(
		BUTTON, 
		axis4d&, 
		axis4d&);


	int draw();//2D描画(使わない)
	void drawUI();//スコア等の描画

	//3D関連
	virtual void draw3D();//3D描画関数

	void initialize3d();//3D描画用の初期化
	//-----邪悪な関数達-----//
	void drawBlock3D(position pos, int color, int field[FIELD_SIZE_X][FIELD_SIZE_Y][FIELD_SIZE_Z][FIELD_SIZE_W]);
	void drawBlockLine3D(position pos, int color, int field[FIELD_SIZE_X][FIELD_SIZE_Y][FIELD_SIZE_Z][FIELD_SIZE_W]);
	void drawBlockFrame_X(const position *pos, int color, const MATRIX *mat);
	void drawBlockFrame_Y(const position *pos, int color, const MATRIX *mat);
	void drawBlockFrame_Z(const position *pos, int color, const MATRIX *mat);
	void drawGround();
	void drawGhost();
	void drawDropBlock();
	void drawBlockFrameAll();
	//----------------------//

	void cameraInput();//カメラ入力
	virtual void cameraMove();//カメラ処理

	//ポーズ画面
	int pauseInput();
	int pauseUpdate();
	int pauseDraw();
	//ゲームオーバー画面
	int gameOverCount;
	int gameOverSelect;

	int gameOverInput();
	int gameOverAnimUpdate();
	int gameOverSelectUpdate();
	int gameOverAnimDraw();
	int gameOverSelectDraw();

	//ツイート
	void tweetScore();

	//スコア
	virtual void addScore(int);

	//ワイプ
	void drawWipe();

	//エフェクト
	void effectUpdate();
public:
	maingame(game& ,controller& );
	~maingame();
	
	virtual int update();//更新
	virtual int initialize();//初期化関数

	//カメラの取得
	MAINGAME::CAMERA& getCamera() { return this->camera; }

	//ゲーム状態の記録
	STATE getState() { return this->eState; }
};