#pragma once

namespace IMAGE
{
	const int IMAGE_MAX = 13;//画像最大数

	enum class IMAGES :int
	{
		TITLE_BACK,//タイトル画面背景
		MAGIC_CIRCLE1,//魔法陣1
		MAGIC_CIRCLE1_FLASH,//魔法陣1_発光
		MAGIC_CIRCLE2,//魔法陣2
		MAGIC_CIRCLE2_FLASH,//魔法陣2_発光
		FIELD_POLE,//フィールドの柱
		BLOCK_SAMPLE1,//ブロックサンプル
		BLOCK_SAMPLE2,//ブロックサンプル
		BLOCK_SAMPLE3,//ブロックサンプル
		BLOCK_SAMPLE4,//ブロックサンプル
		BLOCK_SAMPLE5,//ブロックサンプル
		BLOCK_SAMPLE6,//ブロックサンプル
		BLOCK_SAMPLE7,//ブロックサンプル
	};
}

class image
{
private:
	//参照
	game& cGame;
	
	//画像ハンドル
	int handle[IMAGE::IMAGE_MAX];


	int initialize();//初期化
public:
	image(game&);
	~image();

	int getHandle(IMAGE::IMAGES);
};

