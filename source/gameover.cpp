#include "main.h"

//ゲームオーバー画面での入力処理
int maingame::gameOverInput()
{
	//カーソルの移動

	//移動確認用変数
	int oldSelect = this->gameOverSelect;

	if (this->cController.getButton(BUTTON::LSTICK_UP) == 1)
	{
		this->gameOverSelect--;
	}
	if (this->cController.getButton(BUTTON::LSTICK_DOWN) == 1)
	{
		this->gameOverSelect++;
	}
	//端に行くと反対に
	if (this->gameOverSelect < 0)this->gameOverSelect = MAINGAME::GAMEOVER_TEXT_MAX-1;
	if (this->gameOverSelect > MAINGAME::GAMEOVER_TEXT_MAX-1)this->gameOverSelect = 0;

	//異動されていたらSE
	if (oldSelect != this->gameOverSelect)
	{
		this->cGame.getSound().setSound(SOUND::SOUND_NAME::SE_CURSOR_MOVE, DX_PLAYTYPE_BACK);
	}

	//Aボタンが押されたら
	if (this->cController.getButton(BUTTON::A) == 1)
	{
		//SE
		this->cGame.getSound().setSound(SOUND::SOUND_NAME::SE_CURSOR_ENTER, DX_PLAYTYPE_BACK);

		switch (this->gameOverSelect)
		{
			//スコアをツイート
		case 0:
			this->tweetScore();
			break;

			//(リプレイの保存)
		/*case 1:
			break;
			*/
			//終了
		case 1:
			this->eState = STATE::WIPE_F;
			break;
		default:
			break;
		}
	}

	return 0;
}

//ゲームオーバー突入時のアニメーション中の更新
int maingame::gameOverAnimUpdate()
{
	//カウンタの加算
	this->gameOverCount++;

	//カウンタが一定数溜まると最下段をブロックで埋める
	if (this->gameOverCount % 7 == 0)
	{
		for (int x = 1; x < FIELD_SIZE_X - 1; x++) {
			for (int y = 1; y < FIELD_SIZE_Y - 1; y++) {
				for (int w = 1; w < FIELD_SIZE_W - 1; w++) {
					this->field[x][y][FIELD_SIZE_Z - this->gameOverCount / 7 - 1][w] =
						static_cast<int>(BLOCK_TYPE::TESSERACT_7);
				}
			}
		}
	}

	//全部埋まったら
	//選択画面へ
	if (this->gameOverCount ==( FIELD_SIZE_Z-1) * 7)
	{
		this->eState = STATE::GAMEOVER_SELECT;
	}

	//描画
	this->gameOverAnimDraw();
	return 0;
}

//ゲームオーバー選択画面での更新処理
int maingame::gameOverSelectUpdate()
{
	this->gameOverInput();
	this->gameOverSelectDraw();
	return 0;
}

//ゲームオーバーアニメーション中の描画処理
int maingame::gameOverAnimDraw()
{
	this->cameraInput();
	this->cameraMove();
	this->drawGround();
	this->drawDropBlock();
	this->drawBlockFrameAll();
	this->drawUI();
	return 0;
}

//ゲームオーバー選択画面での描画処理
int maingame::gameOverSelectDraw()
{
	//ゲーム画面
	this->cameraInput();
	this->cameraMove();
	this->drawGround();
	this->drawDropBlock();
	this->drawBlockFrameAll();
	this->drawUI();

	//メニュー選択の後ろを暗く
	DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, 64);
	DxLib::DrawBox(
		this->cGame.getScreenX(0.30f),
		this->cGame.getScreenY(0.48f),
		this->cGame.getScreenX(0.70f),
		this->cGame.getScreenY(0.9f),
		DxLib::GetColor(0, 0, 0), TRUE);
	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);


	DxLib::DrawFormatStringToHandle(
		this->cGame.getScreenX(0.4f),
		this->cGame.getScreenY(0.5f),
		DxLib::GetColor(0, 128, 255),
		this->cGame.getFont().getFonts()[2],
		"GAME OVER");

	for (int i = 0; i < MAINGAME::GAMEOVER_TEXT_MAX; i++)
	{
		DxLib::DrawFormatStringToHandle(
			this->cGame.getScreenX(0.36f),
			this->cGame.getScreenY(0.6f + 0.1f*i),
			DxLib::GetColor(0, 128, 255),
			this->cGame.getFont().getFonts()[2],
			"%s", MAINGAME::GAMEOVER_STRING[i].c_str());
	}
	DxLib::DrawFormatStringToHandle(
		this->cGame.getScreenX(0.32f),
		this->cGame.getScreenY(0.6f + 0.1*this->gameOverSelect),
		DxLib::GetColor(0, 128, 255),
		this->cGame.getFont().getFonts()[2],
		"○");
	//DrawFormatString(400, 750, 0xffffff, "PRESS A BUTTON");

	return 0;
}