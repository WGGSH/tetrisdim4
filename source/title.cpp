#include "main.h"


//コンストラクタ
title::title(
	game& c_Game,controller& c_Controller):
	cGame(c_Game),cController(c_Controller)
{
	this->cTitleBack = new titleBack(this->cGame,this->cController);
	//this->initialize();
	this->select = 0;
}

//デストラクタ
title::~title()
{
	delete this->cTitleBack;
}

//初期化
int title::initialize()
{
	this->cTitleBack->initialize();
	this->wipeCount = 255;
	this->eState = TITLE::STATE::WIPE_S;
	return 0;
}

//入力受付
int title::input()
{
	//メニュー選択処理

	//移動判定用変数
	int oldSelect = this->select;
	if (this->cController.getButton(BUTTON::LSTICK_DOWN) == 1)
	{
		this->select++;
		this->cTitleBack->getCamera().phi_vel = PI / 180 * 9;
		this->cTitleBack->getCamera().phi_accel = -PI / 180 * 0.3f;
	}
	if (this->cController.getButton(BUTTON::LSTICK_UP) == 1)
	{
		this->select--;
		this->cTitleBack->getCamera().phi_vel = -PI / 180 * 9;
		this->cTitleBack->getCamera().phi_accel = PI / 180*0.3f;
	}
	if (this->select >= TITLE::SELECT_MAX)
	{
		this->select = 0;
	}
	if (this->select < 0)
	{
		this->select = TITLE::SELECT_MAX-1;
	}

	//移動していたらSE
	if (oldSelect != this->select)
	{
		this->cGame.getSound().setSound(SOUND::SOUND_NAME::SE_CURSOR_MOVE, DX_PLAYTYPE_BACK);
	}

	if (this->cTitleBack->getCamera().phi_vel<PI / 180 * 0.1f && this->cTitleBack->getCamera().phi_vel>-PI / 180 * 0.1f)
	{
		this->cTitleBack->getCamera().phi_accel = 0;
		this->cTitleBack->getCamera().phi_vel = PI / 180 * 0.1f;
	}

	//決定処理
	if (this->cController.getButton(BUTTON::A) == 1)
	{
		this->eState = TITLE::STATE::WIPE_F;
		//SE
		this->cGame.getSound().setSound(SOUND::SOUND_NAME::SE_CURSOR_ENTER,DX_PLAYTYPE_BACK);
	}

	return 0;
}

//アップデート
int title::update()
{
	this->cTitleBack->update();

	this->draw();
	switch (this->eState)
	{
	case TITLE::STATE::WIPE_S:
		this->drawWipe();
		break;
	case TITLE::STATE::SELECT:
		this->input();
		break;
	case TITLE::STATE::WIPE_F:
		this->drawWipe();
		break;
	}
	return 0;
}

//ワイプ描画
void title::drawWipe()
{
	switch (this->eState)
	{
	case TITLE::STATE::WIPE_S:
		this->wipeCount-=6;
		if (this->wipeCount <= 0)
		{
			this->eState = TITLE::STATE::SELECT;
		}
		break;
	case TITLE::STATE::WIPE_F:
		this->wipeCount+=6;
		if (this->wipeCount >= 255)
		{

			switch (this->select)
			{
				//スタート
			case TITLE::SELECT::GAMESTART:
				this->cGame.setScene(SCENE::MAINGAME);
				this->cGame.getMainGame().initialize();
				break;

				//コンフィグ
			case TITLE::SELECT::CONFIG:
				this->cGame.setScene(SCENE::CONFIG);
				this->cGame.getConfig().initialize();
				//this->cGame.getConfig().getBackHandle()=
				//this->draw();
				//ScreenFlip();
				DxLib::GetDrawScreenGraph(
					0, 0,
					OPTION::WINDOW_LIST_X[this->cGame.getOption().getSaveData().windowType],
					OPTION::WINDOW_LIST_Y[this->cGame.getOption().getSaveData().windowType],
					this->cGame.getConfig().getBackHandle());
				DxLib::GraphFilter(this->cGame.getConfig().getBackHandle(),
					DX_GRAPH_FILTER_GAUSS, 32, 600);
				break;
				/*
			case TITLE::REPLAY:
				this->cGame.setScene(SCENE::REPLAY);
				this->cGame.getReplay().initialize();
				break;*/

				//オートプレイ
			case TITLE::AUTOPLAY:
				this->cGame.setScene(SCENE::AUTOPLAY);
				this->cGame.getAutoPlay().initialize();
				break;

				//ランキング
			case TITLE::RANKING:
				this->cGame.setScene(SCENE::RANKING);
				this->cGame.getRanking().initialize();
				DxLib::GetDrawScreenGraph(
					0, 0,
					OPTION::WINDOW_LIST_X[this->cGame.getOption().getSaveData().windowType],
					OPTION::WINDOW_LIST_Y[this->cGame.getOption().getSaveData().windowType],
					this->cGame.getRanking().getBackHandle());
				DxLib::GraphFilter(this->cGame.getConfig().getBackHandle(),
					DX_GRAPH_FILTER_GAUSS, 32, 600);
				break;

				//終了
			case TITLE::SELECT::EXIT:
				exit(0);
				break;
			default:
				this->eState = TITLE::STATE::SELECT;
				break;
			}
		}
		break;
	default:
		break;
	}
	this->wipeCount++;
	DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, this->wipeCount);

	DxLib::DrawBox(
		0, 0,
		this->cGame.getScreenX(1.0f),
		this->cGame.getScreenY(1.0f),
		0x000000, TRUE);
		
	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
}

//背景の描画
void title::drawBack()
{

	SetDrawBlendMode(DX_BLENDMODE_ADD, 128);
	DrawRotaGraph(
		this->cGame.getScreenX(0.5f),this->cGame.getScreenY(0.5f),
		OPTION::SCREEN_SCALE_RATE[this->cGame.getOption().getSaveData().windowType],
		0.0f,
		this->cGame.getImage().getHandle(IMAGE::IMAGES::TITLE_BACK),TRUE);
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
}

//UI描画
void title::drawUI()
{

}

//描画
int title::draw()
{
	SetDrawZ(1.0f);
	this->drawBack();

	SetDrawZ(0.2f);

	DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, 128);
	DxLib::DrawFillBox(0, 0, this->cGame.getScreenX(1.0f), this->cGame.getScreenY(1.0f), 0x000000);

	DxLib::DrawBox(this->cGame.getScreenX(0.35f),this->cGame.getScreenY(0.48f),
		this->cGame.getScreenX(0.6f), this->cGame.getScreenY(0.9f), DxLib::GetColor(50,0,0), TRUE);

	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

	//DrawFormatString(100, 100, GetColor(255, 255, 255), "PRESS A BUTTON");

	//各メニューの描画

	for (int i = 0; i < TITLE::SELECT_MAX; i++) {
		DxLib::DrawFormatStringToHandle(
			this->cGame.getScreenX(0.4f),
			this->cGame.getScreenY(0.5f + 0.07f*i),
			0x777777,
			this->cGame.getFont().getFonts()[2],
			TITLE::TITLE_TEXT[i].c_str());
	}
	DxLib::DrawFormatStringToHandle(
		this->cGame.getScreenX(0.35f),
		this->cGame.getScreenY(0.5f+0.07f*this->select),
		0xffffff,
		this->cGame.getFont().getFonts()[2],"○");

	SetDrawBlendMode(DX_BLENDMODE_ADD,255);
	DrawFormatStringToHandle(
		this->cGame.getScreenX(0.4f),
		this->cGame.getScreenY(0.5f + 0.07f*this->select),
		GetColor(0, 255, 255),
		this->cGame.getFont().getFonts()[2],
		TITLE::TITLE_TEXT[this->select].c_str());
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);


	return 0;
}