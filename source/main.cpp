#include "main.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	game *cGame = new game();
	if (cGame->update() == -1)return -1;
	delete cGame;
	return 0;
}