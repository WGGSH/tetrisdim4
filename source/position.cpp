#include "main.h"

//+オペレータ
position position::operator+(position& p)
{
	position r =
	{
		this->x + p.x,
		this->y + p.y,
		this->z + p.z,
		this->w + p.w
	};
	return r;
}

//-オペレータ
position position::operator-(position& p)
{
	position r =
	{
		this->x - p.x,
		this->y - p.y,
		this->z - p.z,
		this->w - p.w
	};
	return r;
}

//+=オペレータ
position position::operator+=(position& p)
{
	this->x += p.x;
	this->y += p.y;
	this->z += p.z;
	this->w += p.w;
	return *this;
}

//-=オペレータ
position position::operator-=(position& p)
{
	this->x -= p.x;
	this->y -= p.y;
	this->z -= p.z;
	this->w -= p.w;
	return *this;
}

//==オペレータ
bool position::operator==(position& p)
{
	return (
		(this->x == p.x)&&
		(this->y == p.y)&&
		(this->z == p.z)&&
		(this->w == p.w));
}

//!=オペレータ
bool position::operator!=(position& p)
{
	return !(
		(this->x == p.x)&&
		(this->y == p.y)&&
		(this->z == p.z)&&
		(this->w == p.w));
}

//取得
position position::set(int x, int y, int z,int w)
{
	position r;
	r.x = x;
	r.y = y;
	r.z = z;
	r.w = w;
	return r;
}