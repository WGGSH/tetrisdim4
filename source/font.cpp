#include "main.h"



//コンストラクタ
font::font(
	game& c_Game):
	cGame(c_Game)
{
	this->initialize();
}

//デストラクタ
font::~font()
{
}

//初期化処理
//ゲーム内で使用するフォントの作成
int font::initialize()
{
	this->fonts[static_cast<int>(FONT::FONT_TYPE::SMALL)] = DxLib::CreateFontToHandle(
		NULL, 22 * OPTION::SCREEN_SCALE_RATE[this->cGame.getOption().getSaveData().windowType], 1,DX_FONTTYPE_EDGE);
	this->fonts[static_cast<int>(FONT::FONT_TYPE::NORMAL)] = DxLib::CreateFontToHandle(
		NULL, 40 * OPTION::SCREEN_SCALE_RATE[this->cGame.getOption().getSaveData().windowType], 1,DX_FONTTYPE_EDGE);
	this->fonts[static_cast<int>(FONT::FONT_TYPE::LARGE)] = DxLib::CreateFontToHandle(
		NULL, 64 * OPTION::SCREEN_SCALE_RATE[this->cGame.getOption().getSaveData().windowType],9,DX_FONTTYPE_EDGE);
	return 0;
}