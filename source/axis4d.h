#pragma once

//4次元回転軸を表す構造体
struct axis4d
{
	//各パラメータ
	int xy;
	int xz;
	int xw;
	int yz;
	int yw;
	int zw;

	//オペレータ
	axis4d operator+(axis4d&);
	axis4d operator-(axis4d&);
	axis4d operator+=(axis4d&);
	axis4d operator-=(axis4d&);
	axis4d operator*(int);
	bool operator==(axis4d&);
	bool operator!=(axis4d&);

	//インスタンス取得関数
	static axis4d set(int, int, int, int, int, int);
};