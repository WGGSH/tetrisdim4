#pragma once
class record
{
private:
	//参照
	game& cGame;
	controller& cController;

	//変数
	REPLAY::REPLAYDATA *sReplayData;//リプレイデータ
public:
	record(game&,controller&);
	~record();

	int initialize();//初期化
	int update();//更新
	void save();//リプレイの書き込み	
};

