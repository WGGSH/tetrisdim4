#include "main.h"


//コンストラクタ
config::config(
	game& c_Game,controller& c_Controller):
	cGame(c_Game),cController(c_Controller)
{
	this->wipeCount = 255;
	this->load();
}

//デストラクタ
config::~config()
{
	this->save();
}

//初期化処理
//各種カウンタの初期化
int config::initialize()
{
	this->state = CONFIG::STATE::WIPE_S;
	this->select = 0;
	this->upCount = 0;
	this->downCount = 0;
	this->backHandle = DxLib::MakeGraph(
		OPTION::WINDOW_LIST_X[this->cGame.getOption().getSaveData().windowType],
		OPTION::WINDOW_LIST_Y[this->cGame.getOption().getSaveData().windowType]);
	return 0;
}

//コンフィグするボタンを選択する画面での入力処理
void config::inputMain()
{

	//カーソル移動
	
	//移動確認用変数
	int oldSelect = this->select;

	if (this->cController.getKeys()[KEY_INPUT_DOWN]==1)
	{
		this->downCount++;
	}
	else
	{
		this->downCount = 0;
	}
	if (this->cController.getKeys()[KEY_INPUT_UP]==1)
	{
		this->upCount++;
	}
	else
	{
		this->upCount = 0;
	}

	//端に行くとループする
	if (this->downCount == 1)this->select++;
	if (this->upCount == 1)this->select--;
	if (this->select >= BUTTON_MAX+1)this->select = 0;
	if (this->select < 0)this->select = BUTTON_MAX;

	//移動していたらSE
	if (oldSelect != this->select)
	{
		this->cGame.getSound().setSound(SOUND::SOUND_NAME::SE_CURSOR_MOVE, DX_PLAYTYPE_BACK);
	}

	//スペースキーで選択
	if (this->cController.getKeys()[KEY_INPUT_SPACE]==1)
	{
		if (this->select == BUTTON_MAX)
		{
			this->state = CONFIG::STATE::WIPE_F;
		}
		else
		{
			this->state = CONFIG::STATE::WAIT_INPUT;
		}
		//SE
		this->cGame.getSound().setSound(SOUND::SOUND_NAME::SE_CURSOR_ENTER, DX_PLAYTYPE_BACK);
	}

	if (this->cController.getKeys()[KEY_INPUT_ESCAPE])
	{
		this->select = BUTTON_MAX;
	}
}

//コンフィグ後のボタンを受け付ける画面での入力処理
void config::inputWait()
{
	//全てのボタンの入力状態を確認する
	MYXINPUT_STATE& myxinput = this->cController.getMyXInputState();

	for (int i = 0; i < 26; i++)
	{
		if (myxinput.input[i] >= 1)
		{
			//ボタンが押された
			//this->cController.getConfig()[this->select] = i;
			this->padConfig[this->select] = i;
			
			this->state = CONFIG::STATE::MAIN;
			//SE
			this->cGame.getSound().setSound(SOUND::SOUND_NAME::SE_CURSOR_ENTER, DX_PLAYTYPE_BACK);
		}
	}

}

//保存処理
//コンフィグデータをファイルに保存する
int config::save()
{
	FILE *file;
	errno_t error;

	//ファイルの書き込み開始
	if (error = fopen_s(&file, "userData\\config.dat", "wb") != 0) {
		//エラー処理
	}
	else {
		//コンフィグデータの書き込み
		fwrite(&this->padConfig, sizeof(int), BUTTON_MAX, file);
	}

	fclose(file);

	//データをコントローラに渡す
	this->cController.setConfig(this->padConfig);

	return 0;
}

//読み込み処理
//コンフィグデータを読み込んで設定する
int config::load()
{
	FILE *file;
	errno_t error;

	if (error = fopen_s(&file, "userData\\config.dat", "rb") != 0)
	{
		//エラー処理
		//読み込みに失敗したとき用の初期値
		this->padConfig[static_cast<int>(BUTTON::A)] = MYXINPUT_A;
		this->padConfig[static_cast<int>(BUTTON::B)] = MYXINPUT_B;
		this->padConfig[static_cast<int>(BUTTON::X)] = MYXINPUT_X;
		this->padConfig[static_cast<int>(BUTTON::Y)] = MYXINPUT_Y;
		this->padConfig[static_cast<int>(BUTTON::LB)] = MYXINPUT_LB;
		this->padConfig[static_cast<int>(BUTTON::RB)] = MYXINPUT_RB;
		this->padConfig[static_cast<int>(BUTTON::LS)] = MYXINPUT_LS;
		this->padConfig[static_cast<int>(BUTTON::RS)] = MYXINPUT_RS;
		this->padConfig[static_cast<int>(BUTTON::BACK)] = MYXINPUT_BACK;
		this->padConfig[static_cast<int>(BUTTON::START)] = MYXINPUT_START;
		this->padConfig[static_cast<int>(BUTTON::HAT_UP)] = MYXINPUT_DPAD_UP;
		this->padConfig[static_cast<int>(BUTTON::HAT_RIGHT)] = MYXINPUT_DPAD_RIGHT;
		this->padConfig[static_cast<int>(BUTTON::HAT_DOWN)] = MYXINPUT_DPAD_DOWN;
		this->padConfig[static_cast<int>(BUTTON::HAT_LEFT)] = MYXINPUT_DPAD_LEFT;
		this->padConfig[static_cast<int>(BUTTON::LT)] = MYXINPUT_LT;
		this->padConfig[static_cast<int>(BUTTON::RT)] = MYXINPUT_RT;
		this->padConfig[static_cast<int>(BUTTON::LSTICK_DOWN)] = MYXINPUT_LSTICK_DOWN;
		this->padConfig[static_cast<int>(BUTTON::LSTICK_UP)] = MYXINPUT_LSTICK_UP;
		this->padConfig[static_cast<int>(BUTTON::LSTICK_RIGHT)] = MYXINPUT_LSTICK_RIGHT;
		this->padConfig[static_cast<int>(BUTTON::LSTICK_LEFT)] = MYXINPUT_LSTICK_LEFT;
		this->padConfig[static_cast<int>(BUTTON::RSTICK_DOWN)] = MYXINPUT_RSTICK_DOWN;
		this->padConfig[static_cast<int>(BUTTON::RSTICK_UP)] = MYXINPUT_RSTICK_UP;
		this->padConfig[static_cast<int>(BUTTON::RSTICK_RIGHT)] = MYXINPUT_RSTICK_RIGHT;
		this->padConfig[static_cast<int>(BUTTON::RSTICK_LEFT)] = MYXINPUT_RSTICK_LEFT;
	}
	else
	{
		fread(&this->padConfig, sizeof(int), BUTTON_MAX, file);
		fclose(file);
	}

	this->cController.setConfig(this->padConfig);

	return 0;
}

//更新処理
int config::update()
{
	this->draw();
	switch (this->state)
	{
	case CONFIG::WIPE_S:
		this->drawWipe();
		break;
	case CONFIG::WIPE_F:
		this->drawWipe();
		break;
	case CONFIG::STATE::MAIN:
		this->inputMain();
		break;
	case CONFIG::STATE::WAIT_INPUT:
		this->inputWait();
		break;
	default:
		break;
	}
	return 0;
}


//ワイプ描画
void config::drawWipe()
{
	switch (this->state)
	{
	case CONFIG::STATE::WIPE_S:
		this->wipeCount -= 6;
		if (this->wipeCount <= 0)
		{
			this->state = CONFIG::STATE::MAIN;
		}
		break;
	case CONFIG::STATE::WIPE_F:
		this->wipeCount += 6;
		if (this->wipeCount >= 255)
		{
			this->save();
			this->cGame.setScene(SCENE::TITLE);
			this->cGame.getTitle().initialize();
		}

	}
	this->wipeCount++;
	DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, this->wipeCount);

	DxLib::DrawBox(
		0, 0,
		this->cGame.getScreenX(1.0f),
		this->cGame.getScreenY(1.0f),
		0x000000, TRUE);

	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
}

//描画
void config::draw()
{
	//背景の描画
	DxLib::DrawGraph(0, 0, this->backHandle, TRUE);
	DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, 192);
	DxLib::DrawBox(
		0, 0,
		this->cGame.getScreenX(1.0),
		this->cGame.getScreenY(1.0), 0x000000,TRUE);
	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

	//各ボタンの現在の割り当てとボタン名を表示する
	for (int i = 0; i < BUTTON_MAX; i++)
	{
		//DrawFormatString(200, 200+i*20, 0xffffff, "%d", this->padConfig[i]);
		DxLib::DrawFormatStringToHandle(
			this->cGame.getScreenX(0.3f),
			this->cGame.getScreenY(0.15f + i*0.03f),
			DxLib::GetColor(255, 255, 255),
			this->cGame.getFont().getFonts()[1],
			"%d", this->padConfig[i]);
		DxLib::DrawFormatStringToHandle(
			this->cGame.getScreenX(0.4f),
			this->cGame.getScreenY(0.15f + i*0.03f),
			DxLib::GetColor(255, 255, 255),
			this->cGame.getFont().getFonts()[1],
			"%s", CONTROLLER::BUTTON_STRING[i].c_str());
		//DrawFormatString(230, 200 + i * 20, 0xffffff, "%s", CONTROLLER::BUTTON_STRING[i].c_str());
	}
	DxLib::DrawFormatStringToHandle(
		this->cGame.getScreenX(0.3f),
		this->cGame.getScreenY(0.15f + BUTTON_MAX*0.03f),
		DxLib::GetColor(255, 255, 255),
		this->cGame.getFont().getFonts()[1],
		"戻る");

	DxLib::DrawFormatStringToHandle(
		this->cGame.getScreenX(0.25f),
		this->cGame.getScreenY(0.15f + 0.03f*this->select),
		DxLib::GetColor(255, 255, 255),
		this->cGame.getFont().getFonts()[1],
		"○");
	//DrawFormatString(180, 200 + this->select * 20, 0xffffff, "○");

	//DrawFormatString(200, 200 + 20 * BUTTON_MAX, 0xffffff, "%s", "戻る");
	//for (int i = 0; i < 26; i++) {
		//DrawFormatString(600, 200 + i * 20, 0xffffff, "%d:%d",i, this->cController.getMyXInputState().input[i]);
	//}


	switch (this->state)
	{
	case CONFIG::STATE::MAIN:
		DxLib::DrawFormatStringToHandle(this->cGame.getScreenX(0.4f),
			this->cGame.getScreenY(0.08f),
			DxLib::GetColor(255, 255, 255),
			this->cGame.getFont().getFonts()[2],
			"SPACEキーで変更");
		//DrawFormatString(100, 50, 0xffffff, "SPACEキーで変更");
		break;
	case CONFIG::STATE::WAIT_INPUT:
		DxLib::DrawFormatStringToHandle(this->cGame.getScreenX(0.4f),
			this->cGame.getScreenY(0.08f),
			DxLib::GetColor(255, 255, 255),
			this->cGame.getFont().getFonts()[2],
			"変更後のボタンを入力");
		//DrawFormatString(100, 50, 0xffffff, "PRESS ANY BUTTON");
		break;
	default:
		break;
	}
}
