#include "main.h"



effectBlockDeleteText::effectBlockDeleteText(
	game& c_Game,int number):
	effectBase(c_Game),num(number)
{
}


effectBlockDeleteText::~effectBlockDeleteText()
{
}

int effectBlockDeleteText::update()
{
	count++;

	DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255 / 180.0*(180-this->count));
	DxLib::DrawFormatStringToHandle(
		this->cGame.getScreenX(0.75f),
		this->cGame.getScreenY(0.3f-0.002f*this->count),
		DxLib::GetColor(128,128,255),
		this->cGame.getFont().getFonts()[2],
		"%s", EFFECT::BLOCK_DELETE_TEXT[this->num].c_str());
	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

	if (this->count == 180)return -1;
	return 0;
}
