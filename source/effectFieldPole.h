#pragma once
#include "effectBase.h"
class effectFieldPole :
	public effectBase
{
private:
	float SCALE_X;
	float SCALE_Y;
	MATRIX rotMat;
	VECTOR pos;
public:
	effectFieldPole(game&);
	~effectFieldPole();

	int update();
};

