#include "main.h"


//コンストラクタ
sound::sound(
	game& c_Game): cGame(c_Game)
{
	//ロード
	this->initialize();
}

//デストラクタ
sound::~sound()
{
}

//初期化
int sound::initialize()
{
	//サウンドデータの初期化
	for (int i = 0; i < SOUND::SOUND_MAX; i++) {
		this->sSoundData[i].handle = -1;
		this->sSoundData[i].count = 0;
		this->sSoundData[i].playtype = 0;
		this->sSoundData[i].sSoundState = SOUND::SOUND_STATE::NO_PLAYING;
	}

	this->sSoundData[static_cast<int>(SOUND::SOUND_NAME::SE_CURSOR_MOVE)].handle = DxLib::LoadSoundMem("dat\\sound\\SE\\cursorMove.mp3");
	this->sSoundData[static_cast<int>(SOUND::SOUND_NAME::SE_CURSOR_ENTER)].handle = DxLib::LoadSoundMem("dat\\sound\\SE\\cursorDicision.mp3");
	this->sSoundData[static_cast<int>(SOUND::SOUND_NAME::SE_BLOCK_MOVE)].handle = DxLib::LoadSoundMem("dat\\sound\\SE\\blockMove.mp3");
	DxLib::ChangeVolumeSoundMem(255*60/100,this->sSoundData[static_cast<int>(SOUND::SOUND_NAME::SE_BLOCK_MOVE)].handle);
	DxLib::SetCreateSoundPitchRate(-3000.0f);
	this->sSoundData[static_cast<int>(SOUND::SOUND_NAME::SE_BLOCK_ROTATE)].handle = DxLib::LoadSoundMem("dat\\sound\\SE\\blockRotate.mp3");
	DxLib::SetCreateSoundPitchRate(0);
	this->sSoundData[static_cast<int>(SOUND::SOUND_NAME::SE_BLOCK_DELETE)].handle = DxLib::LoadSoundMem("dat\\sound\\SE\\blockDelete.mp3");
	this->sSoundData[static_cast<int>(SOUND::SOUND_NAME::SE_BLOCK_LOCK)].handle = DxLib::LoadSoundMem("dat\\sound\\SE\\blockLock.mp3");
	this->sSoundData[static_cast<int>(SOUND::SOUND_NAME::BGM_MAINGAME)].handle = DxLib::LoadSoundMem("dat\\sound\\BGM\\gameBGM.mp3");

	//読み込みに失敗した音がないか確認する
	for (int i = 0; i < SOUND::SOUND_MAX; i++) {
		if (this->sSoundData[i].handle == -1)
		{
			//読み込み失敗
			MessageBox(NULL, "サウンドファイル読み込み失敗", "TETRIS4D", 0);
			exit(-1);
		}
	}
	return 0;
}

//サウンド再生
void sound::setSound(SOUND::SOUND_NAME name, int playtype)
{
	this->sSoundData[static_cast<int>(name)].count = 0;
	this->sSoundData[static_cast<int>(name)].sSoundState = SOUND::SOUND_STATE::PLAY_READY;
	this->sSoundData[static_cast<int>(name)].playtype = playtype;
}

//サウンド停止
void sound::stopSound(SOUND::SOUND_NAME name)
{
	this->sSoundData[static_cast<int>(name)].sSoundState = SOUND::SOUND_STATE::STOP_READY;
}

//更新
int sound::update()
{
	for (int i = 0; i < SOUND::SOUND_MAX; i++) {

		//再生状態によって分岐
		switch (this->sSoundData[i].sSoundState)
		{
			//再生準備完了時
		case SOUND::SOUND_STATE::PLAY_READY:
			//再生
			DxLib::PlaySoundMem(this->sSoundData[i].handle, this->sSoundData[i].playtype, 1);

			//再生パターンの登録
			if (this->sSoundData[i].playtype == DX_PLAYTYPE_BACK)
			{
				this->sSoundData[i].sSoundState = SOUND::SOUND_STATE::PLAYING_NOLOOP;
			}
			if (this->sSoundData[i].playtype == DX_PLAYTYPE_LOOP)
			{
				this->sSoundData[i].sSoundState = SOUND::SOUND_STATE::PLAYING_LOOP;
			}
			break;

			//停止準備完了時
		case SOUND::SOUND_STATE::STOP_READY:
			DxLib::StopSoundMem(this->sSoundData[i].handle);
			this->sSoundData[i].sSoundState = SOUND::SOUND_STATE::NO_PLAYING;
			this->sSoundData[i].playtype = 0;
			break;

			//ループ再生中
		case SOUND::SOUND_STATE::PLAYING_LOOP:
			//カウンタ加算
			this->sSoundData[i].count++;
			break;

			//非ループ再生中
		case SOUND::SOUND_STATE::PLAYING_NOLOOP:
			//カウンタ加算
			this->sSoundData[i].count++;
			//終端に達したか判定
			if (CheckSoundMem(this->sSoundData[i].handle) == FALSE)
			{
				//再生終了していれば
				this->sSoundData[i].count = 0;
				this->sSoundData[i].playtype = 0;
				this->sSoundData[i].sSoundState = SOUND::SOUND_STATE::NO_PLAYING;
			}
			break;

			//再生していない場合
			//case SOUND::SOUND_STATE::NO_PLAYING
			//break;

		default:
			break;
		}
	}

	return 0;
}