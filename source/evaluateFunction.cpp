#include "main.h"


//評価関数0
//方針:積極的にラインを消させる
//帰り値:最もブロックで埋まっている段のブロックの個数を求める
float evaluateFunction::evaluate00(int field[FIELD_SIZE_X][FIELD_SIZE_Y][FIELD_SIZE_Z][FIELD_SIZE_W], AUTOPLAY::ACTIONDATA& action)
{
	int maxnum = 0;
	for (int z = 1; z < FIELD_SIZE_Z - 1; z++) {
		int num = 0;
		for (int x = 1; x < FIELD_SIZE_X - 1; x++) {
			for (int y = 1; y < FIELD_SIZE_Y - 1; y++) {
				for (int w = 1; w < FIELD_SIZE_W - 1; w++) {
					if (field[x][y][z][w] != 0)num++;
				}
			}
		}
		if (num > maxnum)maxnum = num;
	}
	return maxnum;
}


//評価関数1
//ブロックの消去シミュレート
//maingameのdel関数と中身が酷似しているので纏めたいがわからない
//方針:積極的にブロックを消す
//返値:消去した段の数
//この関数によって引数のfieldは変化するので他の評価関数はこの前後どちらに配置するか考える必要がある
float evaluateFunction::evaluate01(
	int field[FIELD_SIZE_X][FIELD_SIZE_Y][FIELD_SIZE_Z][FIELD_SIZE_W], AUTOPLAY::ACTIONDATA& action)
{
	
	int delnum;
	BOOL delFlag=TRUE;
	for (delnum = 0; delnum < 4; delnum++) {
		if (delFlag == FALSE)break;
		delFlag = FALSE;
		for (int z = 1; z < FIELD_SIZE_Z - 1; z++) {
			BOOL flag = TRUE;
			for (int x = 1; x < FIELD_SIZE_X - 1; x++) {
				for (int y = 1; y < FIELD_SIZE_Y - 1; y++) {
					for (int w = 1; w < FIELD_SIZE_W - 1; w++) {
						if (field[x][y][z][w] == static_cast<int>(BLOCK_TYPE::NON))
						{
							flag = FALSE;
						}
					}
				}
			}
			//flagがTRUEなら消える
			if (flag == TRUE)
			{
				//delnum++;
				delFlag = TRUE;
				//ブロックを1段ずつ落下する
				for (int z2 = z; z2 >= 1; z2--) {
					for (int x = 1; x < FIELD_SIZE_X - 1; x++) {
						for (int y = 1; y < FIELD_SIZE_Y - 1; y++) {
							for (int w = 1; w < FIELD_SIZE_W - 1; w++) {
								field[x][y][z2][w] =
									(field[x][y][z2 - 1][w] != static_cast<int>(BLOCK_TYPE::GROUND)) ?
									(field[x][y][z2 - 1][w]) :
									static_cast<int>(BLOCK_TYPE::NON);
							}
						}
					}

				}
				//再起処理
				break;
			}

		}
	}
	return delnum-1;
}


//評価関数2
//危険ラインに突入するか判定
//方針:危険な置き方を避ける
//返値:突入している:1
//突入していない:0
float evaluateFunction::evaluate02(
	int field[FIELD_SIZE_X][FIELD_SIZE_Y][FIELD_SIZE_Z][FIELD_SIZE_W], AUTOPLAY::ACTIONDATA& action)
{
	//return action.targetPos.z;
	int num = 0;
	for (int z = 1; z < 5; z++) {
		for (int x = 1; x < FIELD_SIZE_X - 1; x++) {
			for (int y = 1; y < FIELD_SIZE_Y - 1; y++) {
				for (int w = 1; w < FIELD_SIZE_W-1; w++) {
					if (field[x][y][z][w] != 0)
					{
						//return 1;
						num++;
					}
				}

			}
		}
	}
	return num;
}

//評価関数3
//ホールの数を判定
//方針:消しづらくなるブロックの置き方を避ける
//返値:ホールの数
float evaluateFunction::evaluate03(
	int field[FIELD_SIZE_X][FIELD_SIZE_Y][FIELD_SIZE_Z][FIELD_SIZE_W], AUTOPLAY::ACTIONDATA& action)
{
	BOOL flag = FALSE;
	int holeNum = 0;
	for (int x = 1; x < FIELD_SIZE_X; x++) {
		for (int y = 1; y < FIELD_SIZE_Y; y++) {
			for (int w = 1; w < FIELD_SIZE_W; w++) {
				flag = FALSE;
				for (int z = 1; z < FIELD_SIZE_Z; z++) {
					if (field[x][y][z][w] != 0)
					{
						if (flag == FALSE)
						{
							flag = TRUE;
						}
					}
					else if (flag == TRUE)
					{
						holeNum++;
					}
				}
			}
		}
	}

	return holeNum;
}

//評価関数3
//Z座標の判定
//方針:危険な置き方を避ける
//返値:Z座標
float evaluateFunction::evaluate04(
	int field[FIELD_SIZE_X][FIELD_SIZE_Y][FIELD_SIZE_Z][FIELD_SIZE_W], AUTOPLAY::ACTIONDATA& action)
{
	return action.targetPos.z;
}


//評価関数4
//フィールドの高さを判定
//方針:危険な置き方を避ける
//返値:フィールドの最も高い場所のZ座標
float evaluateFunction::evaluate05(
	int field[FIELD_SIZE_X][FIELD_SIZE_Y][FIELD_SIZE_Z][FIELD_SIZE_W], AUTOPLAY::ACTIONDATA& action)
{
	for (int z = 1; z < FIELD_SIZE_Z - 1; z++) {
		for (int x = 1; x < FIELD_SIZE_X - 1; x++) {
			for (int y = 1; y < FIELD_SIZE_Y - 1; y++) {
				for (int w = 1; w < FIELD_SIZE_W - 1; w++) {
					if (field[x][y][z][w] != 0)return z;
				}
			}
		}
	}
	return FIELD_SIZE_Z;
}
