#pragma once
class effectBase
{
protected:
	game& cGame;
	int count;
public:
	effectBase(game&);
	~effectBase();

	virtual int update()=0;
};

#include "effectBlockDeleteText.h"
#include "effectBlockDeleteFlash.h"
#include "effectMagicCircle.h"
#include "effectFieldPole.h"