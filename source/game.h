#pragma once

//シーン
enum class SCENE
{
	TITLE,
	MAINGAME,
	CONFIG,
	REPLAY,
	AUTOPLAY,
	RANKING,
};

class game
{
private:
	//所持クラス
	option *cOption;//オプション
	sound *cSound;//サウンド
	image *cImage;//画像
	font *cFont;//フォント
	fps *cFps;//FPS管理
	controller *cController;//コントローラ
	config *cConfig;//コンフィグ
	title *cTitle;//タイトル画面
	maingame *cMaingame;//メインゲーム
	SCENE eScene;//現在のゲーム状態
	replay *cReplay;
	autoPlay *cAutoPlay;//オート
	ranking *cRanking;//ランキング
	std::vector<std::shared_ptr<effectBase>> effectList;//エフェクトリスト

	//OPTION::saveData *sOption;

	//関数
	int initialize();//初期化

	void effectUpdate();//エフェクトの更新
public:
	game();
	~game();

	int update();//更新

	void setScene(SCENE scene);//ゲーム状態の設定

	//各クラスの取得
	option& getOption() { return *this->cOption; }
	sound& getSound() { return *this->cSound; }
	image& getImage() { return *this->cImage; }
	fps& getFps() { return *this->cFps; }
	maingame& getMainGame() { return *this->cMaingame; }
	title& getTitle() { return *this->cTitle; }
	config& getConfig() { return *this->cConfig; }
	font& getFont() { return *this->cFont; }
	replay& getReplay() { return *this->cReplay; }
	autoPlay& getAutoPlay() { return *this->cAutoPlay; }
	ranking& getRanking() { return *this->cRanking; }
	std::vector<std::shared_ptr<effectBase>>& getEffectList() { return this->effectList; }

	//スクリーン上の座標を割合で取得する
	int getScreenX(float);
	int getScreenY(float);
	SCENE getScene() { return this->eScene; }
};

