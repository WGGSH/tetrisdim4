#pragma once
#include "maingame.h"

namespace AUTOPLAY
{
	//定数
	//このカウントを獲得するたびに行動する
	const int AI_MOVE_TIME = 10000;
	//評価関数の総数
	const int EVALUATE_FUNCTION_MAX = 6;

	//AIの行動データ構造体
	struct ACTIONDATA
	{
		position targetPos;//目標座標
		axis4d targetAxis;//目標回転フォーム
		float score;//スコア
		float scores[EVALUATE_FUNCTION_MAX];//各スコア
		int color;//ブロックの色
		BOOL flag;//行動を決定したかどうか
	};
}

class autoPlay :
	public maingame
{
private:
	//定数
	float SCORE_OFFSET[AUTOPLAY::EVALUATE_FUNCTION_MAX];//評価関数のオフセットリスト

	//変数
	AUTOPLAY::ACTIONDATA sActionData;//現在のAIの行動

	std::vector<axis4d> formList[TETROTESSERACT_MAX];//回転フォームリスト
	//評価関数リスト
	std::vector <std::function<float(
		int[FIELD_SIZE_X][FIELD_SIZE_Y][FIELD_SIZE_Z][FIELD_SIZE_W],
		AUTOPLAY::ACTIONDATA&)>> evaluateFunctionList;

	int moveTime;//行動するために積む時間
	int counter;//1フレームで加算する時間


	//関数
	//スコアを取得
	void getScore(
		int field[FIELD_SIZE_X][FIELD_SIZE_Y][FIELD_SIZE_Z][FIELD_SIZE_W],
		AUTOPLAY::ACTIONDATA&);
	//次の行動を決定
	void setAction();
	//AIの行動入力
	void AIInput();
	//ブロック生成
	int create();
	//回転フォームリストを生成する
	void generateFormList();
	//リプレイ速度の変更
	void replaySpeedInput();
	//2つのブロックが同一か判定
	int checkForm(
		int[TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE],
		int[TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE]);
	//回転シミュレート
	void vRotate(
		int block[TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE],
		axis4d&);
	int over();//ゲームオーバー処理
	//仮想フィールドの削除シミュレート
	int vDel(
		int[FIELD_SIZE_X][FIELD_SIZE_Y][FIELD_SIZE_Z][FIELD_SIZE_W],
		int);

	//ツイート
	void tweetScore();

public:
	autoPlay(game&,controller&);
	~autoPlay();

	int update();//更新
	int initialize();//初期化
};

