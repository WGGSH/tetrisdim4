#include "main.h"

#include "tetrotesseract.h"

//コンストラクタ
maingame::maingame(
	game& c_game,controller& c_controller):
	cGame(c_game),cController(c_controller)
{
	this->wipeCount = 255;

	this->effectList.clear();

	//this->cRecord = new record(this->cGame,this->cController);

}


//デストラクタ
maingame::~maingame()
{
	//delete this->cReplay;
}

//ゲームの初期化
int maingame::initialize()
{
	//カウンタの初期化
	this->count = 0;
	this->gameOverCount = 0;
	//ゲーム状態の初期化
	this->eState = STATE::WIPE_S;
	this->eGameState = GAME_STATE::DROP;
	this->gameOverSelect = 0;
	//得点の初期化
	this->score = 0;
	//ブロック出現数の初期化
	for (int i = 0; i < TETROTESSERACT_MAX; i++)this->blockCount[i] = 0;
	//エフェクトの初期化
	this->effectList.clear();
	//this->effectList.push_back(std::shared_ptr<effectBase>(new effectMagicCircle(
		//this->cGame)));

	//背景画像ハンドルの初期化
	this->backHandle = DxLib::MakeGraph(
		OPTION::WINDOW_LIST_X[this->cGame.getOption().getSaveData().windowType],
		OPTION::WINDOW_LIST_Y[this->cGame.getOption().getSaveData().windowType]);

	//BGMの再生
	this->cGame.getSound().setSound(SOUND::SOUND_NAME::BGM_MAINGAME, DX_PLAYTYPE_LOOP);
	

	//フィールドの初期化
	for (int z = 0; z < FIELD_SIZE_Z; z++) {
		for (int x = 0; x < FIELD_SIZE_X; x++) {
			for (int y = 0; y < FIELD_SIZE_Y; y++) {
				for (int w = 0; w < FIELD_SIZE_W; w++) {

					if (
						((x == 0 || x == FIELD_SIZE_X - 1) +
						(y == 0 || y == FIELD_SIZE_Y - 1) +
						(z == 0 || z == FIELD_SIZE_Z - 1) +
						(w == 0 || w == FIELD_SIZE_W - 1))==0)
					{
						this->field[x][y][z][w] = static_cast<int>(BLOCK_TYPE::NON);
					}
					else
					{
						this->field[x][y][z][w] = static_cast<int>(BLOCK_TYPE::GROUND);
					}

				}
			}
		}
	}

	this->create();

	//3Dの初期化処理
	this->initialize3d();

	//リプレイデータの初期化
	//this->cRecord->initialize();

	return 0;
}

//3D用の初期化関数
void maingame::initialize3d()
{
	//カメラ速度の初期化
	this->camera.phi = -2.41;
	this->camera.theta = 0.72;
	this->camera.length = 465;
	this->camera.phi_vel = 0;
	this->camera.theta_vel = 0;
	this->camera.length_vel = 0;
	this->camera.phi_accel = 0;
	this->camera.theta_accel = 0;
	this->camera.length_accel = 0;
	this->camera.setPosition();

	//カメラの中心点の設定
	DxLib::SetCameraScreenCenter(
		this->cGame.getScreenX(0.5f),
		this->cGame.getScreenY(0.65f));

	//ライトいらなくね?
	//ChangeLightTypeDir(VGet(2, 1, 1));
}

//移動処理
int maingame::move(
	position vector)
{
	this->location += vector;
	return 0;
}

//ブロックがフィールドと接触しているかの判定
//TRUE:接触していない
//FALSE:接触している
BOOL maingame::check(
	int  field[FIELD_SIZE_X][FIELD_SIZE_Y][FIELD_SIZE_Z][FIELD_SIZE_W],
	int block[TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE],
	position pos)
{
	for (int x = 0; x < TESSERACT_SIZE; x++) {
		for (int y = 0; y < TESSERACT_SIZE; y++) {
			for (int z = 0; z < TESSERACT_SIZE; z++) {
				for (int w = 0; w < TESSERACT_SIZE; w++) {
					if (block[x][y][z][w] != static_cast<int>(BLOCK_TYPE::NON) &&
						field[pos.x + x][pos.y + y][pos.z + z][pos.w + w] != static_cast<int>(BLOCK_TYPE::NON))
					{
						return FALSE;
					}
				}
			}
		}
	}

	return TRUE;
}

//落下完了したブロックを固定する
int maingame::lock()
{
	for (int x = 0; x < TESSERACT_SIZE; x++) {
		for (int y = 0; y < TESSERACT_SIZE; y++) {
			for (int z = 0; z < TESSERACT_SIZE; z++) {
				for (int w = 0; w < TESSERACT_SIZE; w++) {
					if (this->nowBlock[x][y][z][w] != static_cast<int>(BLOCK_TYPE::NON))
					{
						this->field
							[x + this->location.x]
						[y + this->location.y]
						[z + this->location.z]
						[w + this->location.w] = 
							this->nowBlock[x][y][z][w];
					}
				}
			}
		}
	}

	//点数加算
	this->addScore(10);

	//SEを鳴らす

	return 0;
}

//回転関数
//複数の改定意を同時入力した際に混乱するのを防ぐため
//1フレームで行う回転は1種類のみとする
//コード汚すぎ問題
//返値:-1→回転失敗/しない
//0:回転成功
int maingame::rotate(
	axis4d& angle)
{
	int x, y, z, w;
	int rBlock[TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE] = { 0 };//回転後の形状
	
	//回転が無ければ終了
	if (angle.xy == 0 && angle.xz == 0 && angle.xw == 0 && angle.yz == 0 && angle.yw == 0 && angle.zw == 0)return -1;
	
	if (angle.xy != 0)angle = axis4d::set(angle.xy, 0, 0, 0, 0, 0);
	if (angle.xz != 0)angle = axis4d::set(0, angle.xz, 0, 0, 0, 0);
	if (angle.xw != 0)angle = axis4d::set(0, 0, angle.xw, 0, 0, 0);
	if (angle.yz != 0)angle = axis4d::set(0, 0, 0, angle.yz, 0, 0);
	if (angle.yw != 0)angle = axis4d::set(0, 0, 0, 0, angle.yw, 0);
	if (angle.zw != 0)angle = axis4d::set(0, 0, 0, 0, 0, angle.zw);

	//回転用数値の設定
	//回転軸によって0~11の値が設定される
	int point =
		angle.xy != 0 ? (angle.xy + 1) / 2 + 0 : 0 +
		angle.xz != 0 ? (angle.xz + 1) / 2 + 2 : 0 +
		angle.xw != 0 ? (angle.xw + 1) / 2 + 4 : 0 +
		angle.yz != 0 ? (angle.yz + 1) / 2 + 6 : 0 +
		angle.yw != 0 ? (angle.yw + 1) / 2 + 8 : 0 + 
		angle.zw != 0 ? (angle.zw + 1) / 2 + 10 : 0;


	int target;//回転後の目的地
	for ( x = 0; x < TESSERACT_SIZE; x++) {
		for ( y = 0; y < TESSERACT_SIZE; y++) {
			for ( z = 0; z < TESSERACT_SIZE; z++) {
				for ( w = 0; w < TESSERACT_SIZE; w++) {
					target = this->nowBlock[x][y][z][w];
					//回転の値によって処理を変更する
					//クソコード
					switch (point)
					{
					case 0://XY_1
						rBlock[x][y][TESSERACT_SIZE - 1 - w][z] = target;
						break;
					case 1://XY_2
						rBlock[x][y][w][TESSERACT_SIZE - 1 - z] = target;
						break;
					case 2://XZ_1
						rBlock[x][TESSERACT_SIZE - 1 - w][z][y] = target;
						break;
					case 3://XZ_2
						rBlock[x][w][z][TESSERACT_SIZE - 1 - y] = target;
						break;
					case 4://XW_1
						rBlock[x][TESSERACT_SIZE - 1 - z][y][w] = target;
						break;
					case 5://XW_2
						rBlock[x][z][TESSERACT_SIZE - 1 - y][w] = target;
						break;
					case 6://YZ_1
						rBlock[TESSERACT_SIZE - 1 - w][y][z][x] = target;
						break;
					case 7://YZ_2
						rBlock[w][y][z][TESSERACT_SIZE - 1 - x] = target;
						break;
					case 8://YW_1
						rBlock[z][y][TESSERACT_SIZE - 1 - x][w] = target;
						break;
					case 9://YW_2
						rBlock[TESSERACT_SIZE - 1 - z][y][x][w] = target;
						break;
					case 10://ZW_1
						rBlock[TESSERACT_SIZE - 1 - y][x][z][w] = target;
						break;
					case 11://ZW_2
						rBlock[y][TESSERACT_SIZE - 1 - x][z][w] = target;
						break;
					default:
						rBlock[x][y][z][w] = this->nowBlock[x][y][z][w];
						break;
					}
				}
			}
		}
	}

	//回転可能かを確認
	//不可能の場合回転しない
	if (this->check(this->field, rBlock, this->location) == FALSE)return -1;

	//複写処理
	for ( x = 0; x < TESSERACT_SIZE; x++) {
		for ( y = 0; y < TESSERACT_SIZE; y++) {
			for ( z = 0; z < TESSERACT_SIZE; z++) {
				for ( w = 0; w < TESSERACT_SIZE; w++) {
					this->nowBlock[x][y][z][w] = rBlock[x][y][z][w];
				}
			}
		}
	}
	
	
	return 0;
}

//次のブロックの生成
int maingame::create()
{
	//所持ブロックの初期化
	//乱数生成アルゴリズムを考案してもいい?
	int color = GetRand(TETROTESSERACT_MAX-1);
	this->blockCount[color]++;
	for (int z = 0; z < TESSERACT_SIZE; z++) {
		for (int x = 0; x < TESSERACT_SIZE; x++) {
			for (int y = 0; y < TESSERACT_SIZE; y++) {
				for (int w = 0; w < TESSERACT_SIZE; w++) {
					this->nowBlock[x][y][z][w] =
						MAINGAME::TETROTESSERACT_SET[color][x][y][z][w];
				}
			}
		}
	}

	//座標初期化
	this->location = position::set(2,2, 0, 2);

	//ゲームオーバーの判定
	//新しく作ったブロックがフィールドと重なっていたらゲームオーバー
	this->over();

	return 0;
}

//移動入力
int maingame::moveInput(
	BUTTON button, position& frm,position& dir)
{
	//左スティック↓(Z落下のみ硬直処理は行わない)
	if (button == BUTTON::LSTICK_DOWN && this->cController.getButton(BUTTON::LSTICK_DOWN) >= 1)
	{
		frm += dir;
		return 0;
	}

	//Z落下以外の移動は一定時間の硬直を設けることで
	//移動が速すぎて目的の場所に動かせなくなる操作性の低下を抑えられる(ハズ)
	if (this->cController.getButton(button) == 1 
		|| this->cController.getButton(button) >= AUTO_MOVE_COUNT)
	{
		frm += dir;
	}
	return 0;
}

//回転入力
int maingame::rotateInput(
	BUTTON button, axis4d& frm, axis4d& dir)
{
	if (this->cController.getButton(button) == 1)
	{
		//RBボタンを押していれば回転方向を逆にする
		frm += dir*((this->cController.getButton(BUTTON::RB) >= 1) * 2 - 1)*(-1);
	}
	return 0;
}

//キー入力関数
int maingame::input()
{

	//ポーズ画面を開く
	//ポーズ画面に入る直前の画面を保存して
	//ポーズ画面の背景に使用する
	if (this->cController.getButton(BUTTON::START) == 1)
	{
		this->eState = STATE::PAUSE;
		this->draw3D();
		this->drawUI();
		DxLib::GetDrawScreenGraph(
			0, 0,
			OPTION::WINDOW_LIST_X[this->cGame.getOption().getSaveData().windowType],
			OPTION::WINDOW_LIST_Y[this->cGame.getOption().getSaveData().windowType],
			this->backHandle);
		//ガウシアンフィルタを書ける(ぼかし)
		DxLib::GraphFilter(this->backHandle,
			DX_GRAPH_FILTER_GAUSS, 32, 4000);
		return 0;
	}

	//ここからゲーム用の入力
	//--------------------------//

	position vector = { 0, 0, 0, 0 };//移動ベクトル
	axis4d rotate = { 0,0,0,0,0,0 };//回転軸

	//LBを押しているか
	if (this->cController.getButton(BUTTON::LB) >= 1)
	{
		//回転入力
		this->rotateInput(BUTTON::LSTICK_UP, rotate, axis4d::set(0, 0, -1, 0, 0, 0));
		this->rotateInput(BUTTON::LSTICK_DOWN, rotate, axis4d::set(0, 0, 1, 0, 0, 0));
		this->rotateInput(BUTTON::LSTICK_LEFT, rotate, axis4d::set(0, 0, 0, 0, 0, -1));
		this->rotateInput(BUTTON::LSTICK_RIGHT, rotate, axis4d::set(0, 0, 0, 0, 0, 1));
		this->rotateInput(BUTTON::RSTICK_UP, rotate, axis4d::set(0, 0, 0, 1, 0, 0));
		this->rotateInput(BUTTON::RSTICK_DOWN, rotate, axis4d::set(0, 0, 0, -1, 0, 0));
		this->rotateInput(BUTTON::A, rotate, axis4d::set(0, 1, 0, 0, 0, 0));
		this->rotateInput(BUTTON::X, rotate, axis4d::set(1, 0, 0, 0, 0, 0));
		this->rotateInput(BUTTON::RSTICK_RIGHT, rotate, axis4d::set(0, 0, 0, 0, 1, 0));
		this->rotateInput(BUTTON::RSTICK_LEFT, rotate, axis4d::set(0, 0, 0, 0, -1, 0));
	}
	else
	{
		//移動入力
		this->moveInput(BUTTON::LSTICK_RIGHT, vector, position::set(1, 0, 0, 0));
		this->moveInput(BUTTON::LSTICK_LEFT, vector, position::set(-1, 0, 0, 0));
		this->moveInput(BUTTON::LSTICK_DOWN, vector, position::set(0, 0, 1, 0));
		this->moveInput(BUTTON::LSTICK_UP, vector, position::set(0, 0, -1, 0));
		this->moveInput(BUTTON::RSTICK_RIGHT, vector, position::set(0, 0, 0, 1));
		this->moveInput(BUTTON::RSTICK_LEFT, vector, position::set(0, 0, 0, -1));
		this->moveInput(BUTTON::RSTICK_DOWN, vector, position::set(0, -1, 0, 0));
		this->moveInput(BUTTON::RSTICK_UP, vector, position::set(0, 1, 0, 0));
	}

	//回転可能か判定
	if (this->rotate(rotate) == 0)
	{
		this->cGame.getSound().setSound(SOUND::SOUND_NAME::SE_BLOCK_ROTATE,DX_PLAYTYPE_BACK);
	}

	//ハードドロップ判定
	//ブロックをZ一番下まで落とす(固定はしない)
	if (this->cController.getButton(BUTTON::Y) == 1)
	{
		int z;
		//落下距離の計算
		for ( z = 0; z < FIELD_SIZE_Z; z++) 
		{
			if (this->check(this->field, this->nowBlock, this->location + position::set(0, 0, z, 0)) == FALSE)
			{
				break;
			}
		}
		vector.z = z-1;
	}

	//移動できるかを判定
	if (this->check(this->field, this->nowBlock, this->location + vector) == TRUE)
	{
		this->move(vector);
		//移動量が0以外ならSE
		if (vector != position::set(0, 0, 0, 0))
		{
			this->cGame.getSound().setSound(SOUND::SOUND_NAME::SE_BLOCK_MOVE, DX_PLAYTYPE_BACK);
		}
	}


	//ブロックの固定処理
	if (this->cController.getButton(BUTTON::B) == 1 &&
		this->check(this->field, this->nowBlock, this->location + position::set(0, 0, 1, 0)) == FALSE)
	{
		this->lock();
		//SE
		this->cGame.getSound().setSound(SOUND::SOUND_NAME::SE_BLOCK_LOCK, DX_PLAYTYPE_BACK);

		this->create();
	}

	//ブロックの消去判定
	//再起関数により消した列の数が返ってくるので得点計算に利用する
	int num=this->del(0);
	if (num >= 1)
	{
		this->addScore(num *num*num*num * 10000);
		//SE
		this->cGame.getSound().setSound(SOUND::SOUND_NAME::SE_BLOCK_DELETE,DX_PLAYTYPE_BACK);


		//エフェクトの実行
		this->effectList.push_back(std::shared_ptr<effectBase>(new effectBlockDeleteText(
			this->cGame, num - 1)));
	}


	return 0;
}

//ブロックの消去処理
int maingame::del(
	int num)
{
	for (int z = 1; z < FIELD_SIZE_Z-1; z++) {
		BOOL flag = TRUE;
		for (int x = 1; x < FIELD_SIZE_X - 1; x++) {
			for (int y = 1; y < FIELD_SIZE_Y - 1; y++) {
				for (int w = 1; w < FIELD_SIZE_W - 1; w++) {
					if (this->field[x][y][z][w] == static_cast<int>(BLOCK_TYPE::NON))
					{
						flag = FALSE;
					}
				}
			}
		}
		//flagがTRUEなら消える
		if (flag == TRUE)
		{
			num++;
			//消滅エフェクトの発生
			this->effectList.push_back(std::shared_ptr<effectBase>(new effectBlockDeleteFlash(
				this->cGame, z)));

			//ブロックを1段ずつ落下する
			for (int z2 = z; z2 >= 1; z2--) {
				for (int x = 1; x < FIELD_SIZE_X - 1; x++) {
					for (int y = 1; y < FIELD_SIZE_Y - 1; y++) {
						for (int w = 1; w < FIELD_SIZE_W - 1; w++) {
							this->field[x][y][z2][w] = 
								(this->field[x][y][z2 - 1][w]!=static_cast<int>(BLOCK_TYPE::GROUND)) ? 
								(this->field[x][y][z2-1][w]) :
							static_cast<int>(BLOCK_TYPE::NON);
						}
					}
				}

			}
			//再起処理
			return this->del(num);
		}

	}
	return num;
}


//ゲームオーバー判定
int maingame::over()
{
	if (this->check(this->field, this->nowBlock, this->location) == FALSE)
	{
		
		//ゲームオーバー
		//this->initialize();
		this->eState = MAINGAME::STATE::GAMEOVER_ANIM;
		//ランキングの更新
		RANKING::USER_DATA user;
		user.score = this->score;
		this->cGame.getRanking().addUser(user);
		//this->cRecord->save();

		//BGMの停止
		this->cGame.getSound().stopSound(SOUND::SOUND_NAME::BGM_MAINGAME);
	}

	return 0;
}

//今は使われなくなった2D描画処理
int maingame::draw()
{
	//背景の描画
	DxLib::DrawBox(0, 60, 1600, 900, DxLib::GetColor(128, 128, 128), TRUE);
	//フィールドの描画
	for (int z = 0; z < FIELD_SIZE_Z; z++) {
		for (int x = 0; x < FIELD_SIZE_X; x++) {
			for (int y = 0; y < FIELD_SIZE_Y; y++) {
				for (int w = 0; w < FIELD_SIZE_W; w++) {
					int color;
					if (this->field[x][y][z][w] == static_cast<int>(BLOCK_TYPE::NON))color = DxLib::GetColor(0,0,0);
					else if (this->field[x][y][z][w] == static_cast<int>(BLOCK_TYPE::GROUND))color = DxLib::GetColor(255, 255, 255);
					else color = helper::GetColorHSBtoRGB(360 / TETROTESSERACT_MAX * this->field[x][y][z][w], 1, 1);
					DxLib::DrawBox(
						20 + x * 10 + w * 80,
						20 + z * 10 + y * 140,
						30 + x * 10 + w * 80,
						30 + z * 10 + y * 140,
						color, TRUE);
				}
			}
		}
	}

	//落下中のセルの描画
	for (int z = 0; z < TESSERACT_SIZE; z++) {
		for (int x = 0; x < TESSERACT_SIZE; x++) {
			for (int y = 0; y < TESSERACT_SIZE; y++) {
				for (int w = 0; w < TESSERACT_SIZE; w++) {
					if (this->nowBlock[x][y][z][w] != static_cast<int>(BLOCK_TYPE::NON))
					{
						DxLib::DrawBox(
							20 + (x + this->location.x) * 10 + (w + this->location.w) * 80,
							20 + (z + this->location.z) * 10 + (y + this->location.y) * 140,
							30 + (x + this->location.x) * 10 + (w + this->location.w) * 80,
							30 + (z + this->location.z) * 10 + (y + this->location.y) * 140,
							helper::GetColorHSBtoRGB(360/TETROTESSERACT_MAX*this->nowBlock[x][y][z][w],1,1) , TRUE);
					}
				}
			}
		}
	}

	//現在座標の描画
	DxLib::DrawBox(
		20 + this->location.x * 10 + this->location.w * 80,
		20 + this->location.z * 10 + this->location.y * 140,
		30 + this->location.x * 10 + this->location.w * 80,
		30 + this->location.z * 10 + this->location.y * 140, 
		DxLib::GetColor(0, 255, 255), TRUE);

	return 0;
}


//ワイプ描画
void maingame::drawWipe()
{
	switch (this->eState)
	{
	case MAINGAME::STATE::WIPE_S:
		this->wipeCount -= 6;
		if (this->wipeCount <= 0)
		{
			this->eState = MAINGAME::STATE::PLAYING;
		}
		break;
	case MAINGAME::STATE::WIPE_F:
		this->wipeCount += 6;
		if (this->wipeCount >= 255)
		{
			this->cGame.setScene(SCENE::TITLE);
			this->cGame.getTitle().initialize();
		}
			
	}
	this->wipeCount++;
	DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, this->wipeCount);

	DxLib::DrawBox(
		0, 0,
		this->cGame.getScreenX(1.0f),
		this->cGame.getScreenY(1.0f),
		0x000000, TRUE);

	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
}

//UI表示
void maingame::drawUI()
{
	SetDrawZ(0.1f);
	SetDrawBlendMode(DX_BLENDMODE_ADD, 128);
	DrawRotaGraph(
		this->cGame.getScreenX(0.5f), this->cGame.getScreenY(0.5f),
		OPTION::SCREEN_SCALE_RATE[this->cGame.getOption().getSaveData().windowType],
		0.0f,
		this->cGame.getImage().getHandle(IMAGE::IMAGES::TITLE_BACK), TRUE);
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

	//スコアの描画
	DxLib::DrawFormatStringToHandle(
		this->cGame.getScreenX(0.05f),
		this->cGame.getScreenY(0.1f),
		DxLib::GetColor(255, 128, 128),
		this->cGame.getFont().getFonts()[1],
		"SCORE");
	DxLib::DrawFormatStringToHandle(
		this->cGame.getScreenX(0.05f),
		this->cGame.getScreenY(0.15f),
		DxLib::GetColor(255, 128, 128),
		this->cGame.getFont().getFonts()[1],
		"%d", this->score);

	//各ブロックの出現回数を表示
	for (int i = 0; i < TETROTESSERACT_MAX; i++) {
		//画像
		DxLib::DrawRotaGraph(
			this->cGame.getScreenX(0.10f),
			this->cGame.getScreenY(0.32f + 0.07f*i),
			OPTION::SCREEN_SCALE_RATE[this->cGame.getOption().getSaveData().windowType],
			0.0f,
			this->cGame.getImage().getHandle(static_cast<IMAGE::IMAGES>(static_cast<int>(IMAGE::IMAGES::BLOCK_SAMPLE1)+i)),
			TRUE);

		//文字
		DxLib::DrawFormatStringToHandle(
			this->cGame.getScreenX(0.15f),
			this->cGame.getScreenY(0.3f + 0.07f*i),
			DxLib::GetColor(255, 255, 255),
			this->cGame.getFont().getFonts()[1],
			":%d", this->blockCount[i]);
	}

	//デバッグ情報
#ifdef _DEBUG
	//ブロックの所持座標
	DxLib::DrawFormatString(
		0, 300, 0xff0000, "Position[%d,%d,%d,%d]",
		this->location.x, this->location.y, this->location.z, this->location.w);
#endif
}

//3D用の描画関数
//闇の関数を纏める関数
void maingame::draw3D()
{
	//SetDrawBlendMode(DX_BLENDMODE_ALPHA, 128);

	//地面,フィールド(闇)
	//ゴーストブロック
	//落下中のブロック
	//ブロックのフレーム(闇)
	//の順に描画

	this->drawGround();
	this->drawGhost();
	this->drawDropBlock();
	this->drawBlockFrameAll();

	/*
	//軸
	DrawLine3D(VGet(0, 0, 0), VGet(10000, 0, 0), GetColor(255, 0, 0));
	DrawLine3D(VGet(0, 0, 0), VGet(0, 10000, 0), GetColor(0, 255, 0));
	DrawLine3D(VGet(0, 0, 0), VGet(0, 0, 10000), GetColor(0, 0, 255));
	*/

}

//カメラ入力
void maingame::cameraInput()
{
	//上下回転
	if (this->cController.getButton(BUTTON::HAT_UP))this->camera.theta_vel = PI / 180 * 3;
	else if (this->cController.getButton(BUTTON::HAT_DOWN))this->camera.theta_vel = -PI / 180 * 3;
	else this->camera.theta_vel = 0;

	//左右回転
	if (this->cController.getButton(BUTTON::HAT_RIGHT))this->camera.phi_vel = PI / 180 * 3;
	else if (this->cController.getButton(BUTTON::HAT_LEFT))this->camera.phi_vel = -PI / 180 * 3;
	else this->camera.phi_vel = 0;

	//ズームイン/アウト
	if (this->cController.getButton(BUTTON::LT))this->camera.length_vel = -3.0f;
	else if (this->cController.getButton(BUTTON::RT))this->camera.length_vel = 3.0f;
	else this->camera.length_vel = 0;

}

//カメラ制御関数
void maingame::cameraMove()
{
	//カメラベクトルを加算
	this->camera.phi_vel += this->camera.phi_accel;
	this->camera.theta_vel += this->camera.length_accel;
	this->camera.length_vel += this->camera.length_accel;
	this->camera.phi += this->camera.phi_vel;
	this->camera.theta += this->camera.theta_vel;
	this->camera.length += this->camera.length_vel;
	//移動制限の判定
	if (this->camera.theta >= PI / 180 * 89)this->camera.theta = PI / 180 * 89;
	if (this->camera.theta < PI / 180 * 1)this->camera.theta = PI / 180 * 1;
	if (this->camera.length < 50)this->camera.length = 50;
	
	//カメラの位置情報を基にセット
	this->camera.setPosition();
}

//ツイート処理
//まだ内容吟味中
void maingame::tweetScore()
{
	//ツイート
	std::string base = "https://twitter.com/intent/tweet?";
	std::string hash = "hashtags=TetrisDim4&";
	std::string referer = "original_referer=https://twitter.com.WGG_SH&";
	std::string text = "text=4次元テトリスでScore:";
	text += std::to_string(this->score);
	text += "点を獲得しました!";
	text += "&";
	std::string tw_p = "tw_p = tweetbutton&";
	//std::string via = "via=WGG_SH &";
	//std::string url = "url=https://twitter.com/WGG_SH";
	std::string link = base + hash + referer + text + tw_p;// +via;

	//ブラウザを開く
	ShellExecute(NULL, TEXT("open"), 
		TEXT(link.c_str()), NULL, TEXT(""), SW_SHOW);
}

//スコア加算処理
void maingame::addScore(
	int val)
{
	this->score += val;
}

//エフェクトの更新
void maingame::effectUpdate()
{
	for (std::vector<std::shared_ptr<effectBase>>::iterator itEffect = this->effectList.begin();
	itEffect != this->effectList.end();)
	{
		if (itEffect->get()->update() == -1)
		{
			itEffect = this->effectList.erase(itEffect);
			continue;
		}
		++itEffect;
	}
}

//アップデート処理
int maingame::update()
{
	//ゲームカウンタの加算
	this->count++;
	//リプレイデータの記憶
	//this->cRecord->update();


	//ゲーム状態によって分岐
	switch (this->eState)
	{
	case STATE::WIPE_S:
		this->draw3D();
		this->drawUI();
		this->drawWipe();
		break;
	case STATE::WIPE_F:
		this->draw3D();
		this->drawUI();
		this->drawWipe();
		break;
	case STATE::PLAYING:
		switch (this->eGameState)
		{
		case GAME_STATE::DROP:
			this->effectUpdate();

			this->input();
			this->cameraInput();
			this->cameraMove();
			this->draw3D();
			this->drawUI();


			break;
		case GAME_STATE::DELETING://使う必要なくね?
			break;
		}
		break;
	case STATE::PAUSE:
		this->pauseUpdate();
		break;
	case STATE::GAMEOVER_ANIM:
		this->gameOverAnimUpdate();
		break;
	case STATE::GAMEOVER_SELECT:
		this->gameOverSelectUpdate();
		break;
	default:
		break;
	}

	return 0;
}

