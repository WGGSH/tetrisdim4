#include "main.h"


//コンストラクタ
ranking::ranking(
	game& c_Game,controller& c_Controller):
	cGame(c_Game),cController(c_Controller)
{
	this->wipeCount = 255;
	this->initialize();
}


//デストラクタ
ranking::~ranking()
{
}

//ランキングの並び替え
void ranking::sortRank()
{
	for (int i = 0; i < RANKING::RANK_MAX; i++) {
		RANKING::USER_DATA top = { -1 };
		int topPos=-1;
		for (int j = i; j < RANKING::RANK_MAX; j++) {
			if (this->sRanking[j].score>top.score)
			{
				top = this->sRanking[j];
				topPos = j;
			}
		}
		this->sRanking[i] = top;
	}
}

//ランキングデータの読み込み
int ranking::load()
{
	FILE *file;
	errno_t error;

	if (error = fopen_s(&file, "userData\\score.dat", "rb") != 0)
	{
		//エラー処理
		//読み込みに失敗したとき用の初期値
		for (int i = RANKING::RANK_MAX - 1; i >= 0; i--)
		{
			this->sRanking[RANKING::RANK_MAX-i-1].score = 10000 * i;
		}
	}
	else
	{
		fread(&this->sRanking, sizeof(RANKING::USER_DATA), RANKING::RANK_MAX, file);
		fclose(file);
	}


	return 0;
}

//ランキングデータの書き込み
int ranking::save()
{
	FILE *file;
	errno_t error;

	//ファイルの書き込み開始
	if (error = fopen_s(&file, "userData\\score.dat", "wb") != 0) {
		//エラー処理
	}
	else {
		//データの書き込み
		fwrite(&this->sRanking, sizeof(RANKING::USER_DATA), RANKING::RANK_MAX, file);
	}

	fclose(file);

	return 0;
}

//ランキングにユーザを追加
int ranking::addUser(RANKING::USER_DATA &user)
{
	//最下位よりもスコアが高ければ
	if (user.score > this->sRanking[RANKING::RANK_MAX - 1].score) {
		//最下位と入れ替え
		this->sRanking[RANKING::RANK_MAX - 1] = user;

		//ソート
		this->sortRank();

		//書き込み
		this->save();
	}
	else
	{
		return -1;
	}

	return 0;
}

//初期化
int ranking::initialize()
{
	//シーン設定
	this->state = RANKING::STATE::WIPE_S;
	this->backHandle = DxLib::MakeGraph(
		OPTION::WINDOW_LIST_X[this->cGame.getOption().getSaveData().windowType],
		OPTION::WINDOW_LIST_Y[this->cGame.getOption().getSaveData().windowType]);

	//読み込み
	this->load();

	return 0;
}

//入力受付
int ranking::input()
{
	//決定ボタン,キャンセルボタンでタイトル画面に戻る
	if (this->cController.getButton(BUTTON::A) == 1 || this->cController.getButton(BUTTON::B) == 1)
	{
		this->state = RANKING::STATE::WIPE_F;
		//SE
		this->cGame.getSound().setSound(SOUND::SOUND_NAME::SE_CURSOR_ENTER, DX_PLAYTYPE_BACK);
	}

	return 0;
}

//ワイプ描画
void ranking::drawWipe()
{
	switch (this->state)
	{
	case RANKING::STATE::WIPE_S:
		this->wipeCount -= 6;
		if (this->wipeCount <= 0)
		{
			this->state = RANKING::STATE::MAIN;
		}
		break;
	case RANKING::STATE::WIPE_F:
		this->wipeCount += 6;
		if (this->wipeCount >= 255)
		{
			this->save();
			this->cGame.setScene(SCENE::TITLE);
			this->cGame.getTitle().initialize();
		}

	}
	this->wipeCount++;
	DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, this->wipeCount);

	DxLib::DrawBox(
		0, 0,
		this->cGame.getScreenX(1.0f),
		this->cGame.getScreenY(1.0f),
		0x000000, TRUE);

	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
}

//更新
int ranking::update()
{
	this->draw();
	switch (this->state)
	{
	case RANKING::STATE::WIPE_S:
	case RANKING::STATE::WIPE_F:
		this->drawWipe();
		break;
	case RANKING::STATE::MAIN:
		this->input();
		break;
	}
	return 0;
}

//描画
int ranking::draw()
{
	//背景の描画
	DxLib::DrawGraph(0, 0, this->backHandle, TRUE);
	DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, 192);
	DxLib::DrawBox(
		0, 0,
		this->cGame.getScreenX(1.0),
		this->cGame.getScreenY(1.0), 0x000000, TRUE);
	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

	//スコアの描画
	for (int i = 0; i < RANKING::RANK_MAX; i++) {
		DxLib::DrawFormatStringToHandle(
			this->cGame.getScreenX(0.4f),
			this->cGame.getScreenY(0.1f + 0.05*i),
			DxLib::GetColor(255, 255, 255),
			this->cGame.getFont().getFonts()[1],
			"%d:", i+1);
		DxLib::DrawFormatStringToHandle(
			this->cGame.getScreenX(0.45f),
			this->cGame.getScreenY(0.1f + 0.05*i),
			DxLib::GetColor(255, 255, 255),
			this->cGame.getFont().getFonts()[1],
			"%d", this->sRanking[i].score);
	}
	return 0;
}