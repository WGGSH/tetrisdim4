#pragma once

namespace FPS
{
	//多分使わない
	//const int FPS = 60;//理想フレームレート
}

using namespace FPS;

class fps
{
private:
	//変数
	int fpsMax;//FPS最大値
	game& cGame;
	int fps_count;
	int count0t;
	int *f;
	float average;

	float framerate;//現在のFPS

	//関数
	void wait();//FPS調整を行う待機処理
	
public:
	fps(game& c_Game);
	~fps();

	void update();//更新
	void draw();//描画
};

