#include "main.h"



effectBlockDeleteFlash::effectBlockDeleteFlash(
	game& c_Game, int position) :
	effectBase(c_Game), zpos(position)
{
}


effectBlockDeleteFlash::~effectBlockDeleteFlash()
{
}

int effectBlockDeleteFlash::update()
{
	this->count++;

	//描画
	SetDrawBlendMode(DX_BLENDMODE_ADD, 255);

	VECTOR pos1, pos2, pos3;
	MATRIX rotMat;
	MATRIX rotMat2;
	MATRIX scaleMat;
	MATRIX convertMat;
	MATRIX convertMat2;
	
	for (int i = 0; i < 5; i++) {
		DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 255 / 180.0*(180 - this->count-i*20));
		for (int w = 0; w < 5; w++) {
			//行列の設定
			rotMat = MGetRotY(PI2 / (FIELD_SPACE_W + 1)*w-PI/15);
			rotMat2 = MGetRotY(PI2 / (FIELD_SPACE_W + 1)*(w + 1) - PI / 15);
			scaleMat = MGetScale(VGet(
				this->count*this->count / 900.0 + 7.0f + 0.5f*i,
				1.0f - i*0.23f, 
				this->count *this->count/ 900.0 + 7.0f + 0.5f*i));
			//scaleMat = MGetScale(VGet(1, 1, 1));
			convertMat = MMult(scaleMat, rotMat);
			convertMat2 = MMult(scaleMat, rotMat2);

			//座標指定
			pos1 = VGet(
				-20,
				220 - 20 * this->zpos,
				0); pos1 = VTransform(pos1, convertMat);
			pos2 = VGet(
				-20,
				220 - 20 * (this->zpos + 1),
				0); pos2 = VTransform(pos2, convertMat);
			pos3 = VGet(
				-20,
				220 - 20 * this->zpos,
				0); pos3 = VTransform(pos3, convertMat2);
			DrawTriangle3D(
				pos1, pos2, pos3,
				GetColor(0, 192, 255), TRUE);
			pos1 = VGet(
				-20,
				220 - 20 * this->zpos,
				0); pos1 = VTransform(pos1, convertMat2);
			pos3 = VGet(
				-20,
				220 - 20 * (this->zpos + 1),
				0); pos3 = VTransform(pos3, convertMat2);
			DrawTriangle3D(
				pos1, pos2, pos3,
				GetColor(0, 192, 255), TRUE);

		}
	}
	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);


	if (this->count == -1)return -1;

	return 0;
}
