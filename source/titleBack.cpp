#include "main.h"

#include "tetrotesseract.h"

//コンストラクタ
titleBack::titleBack(
	game& c_Game, controller& c_Controller) :
	maingame(c_Game, c_Controller)
{

}

titleBack::~titleBack()
{

}


//初期化
int titleBack::initialize()
{
	//カウンタの初期化
	this->count = 0;
	//ゲーム状態の初期化
	this->eState = STATE::PLAYING;
	this->eGameState = GAME_STATE::DROP;
	//得点の初期化
	this->score = 0;

	//フィールドの初期化
	for (int z = 0; z < FIELD_SIZE_Z; z++) {
		for (int x = 0; x < FIELD_SIZE_X; x++) {
			for (int y = 0; y < FIELD_SIZE_Y; y++) {
				for (int w = 0; w < FIELD_SIZE_W; w++) {

					if (
						((x == 0 || x == FIELD_SIZE_X - 1) +
							(y == 0 || y == FIELD_SIZE_Y - 1) +
							(z == 0 || z == FIELD_SIZE_Z - 1) +
							(w == 0 || w == FIELD_SIZE_W - 1)) == 0)
					{
						this->field[x][y][z][w] = static_cast<int>(BLOCK_TYPE::NON);
					}
					else
					{
						this->field[x][y][z][w] = static_cast<int>(BLOCK_TYPE::GROUND);
					}

				}
			}
		}
	}

	//ブロックを一定回数積み込む
	int num = GetRand(80) + 10;
	for (int i = 0; i < num; i++)
	{
		this->create();
		position vector = {
			GetRand(4) - 2,
			GetRand(4) - 2,
			0,
			GetRand(4) - 2
		};

		if (this->check(this->field, this->nowBlock, this->location + vector) == TRUE)
		{
			//移動
			this->move(vector);

			//ハードドロップ
			int z;
			for (z = 0; z < FIELD_SIZE_Z; z++)
			{
				if (this->check(this->field, this->nowBlock, this->location + position::set(0, 0, z, 0)) == FALSE)
				{
					break;
				}
			}
			//落下
			this->move(position::set(0, 0, z - 1, 0));

			//固定
			this->lock();
		}
		else
		{
			continue;
		}
	}

	//3Dの初期化処理
	this->initialize3d();

	this->camera.phi_vel = PI / 180 * 0.1f;

	return 0;
}

//次のブロックの生成
int titleBack::create()
{
	//所持ブロックの初期化
	//乱数生成アルゴリズムを考案してもいい?
	int color = GetRand(TETROTESSERACT_MAX - 1);
	for (int z = 0; z < TESSERACT_SIZE; z++) {
		for (int x = 0; x < TESSERACT_SIZE; x++) {
			for (int y = 0; y < TESSERACT_SIZE; y++) {
				for (int w = 0; w < TESSERACT_SIZE; w++) {
					this->nowBlock[x][y][z][w] =
						MAINGAME::TETROTESSERACT_SET[color][x][y][z][w];
				}
			}
		}
	}

	//座標初期化
	this->location = position::set(2, 2, 0, 2);

	//ゲームオーバーの判定
	//新しく作ったブロックがフィールドと重なっていたらゲームオーバー

	return 0;
}


//3D描画
void titleBack::draw3D()
{
	this->drawGround();
	this->drawBlockFrameAll();
}

//アップデート関数
int titleBack::update()
{
	this->cameraMove();
	this->draw3D();

	return 0;
}
