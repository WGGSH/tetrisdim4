#include "main.h"

//コンストラクタ
fps::fps(
	game& C_Game) :
	cGame(C_Game) {
	//イニシャライズ
	//Gameクラスへの参照を渡す
	this->count0t = 0;
	this->fps_count = 0;
	this->average = 0;
	this->framerate = 0;

	this->fpsMax = this->cGame.getOption().getSaveData().fpsMax;//ここを変えるとフレームレート上限が上がる
	this->f = new int[this->fpsMax];
}

//デストラクタ
fps::~fps() {

}

//待機関数
void fps::wait() {
	int term, i, gnt;
	static int t = 0;
	if (this->fps_count == 0) {//60フレームの1回目なら
		if (t == 0)//完全に最初ならまたない
			term = 0;
		else//前回記録した時間を元に計算
			term = this->count0t + 1000 - DxLib::GetNowCount();
	}
	else    //待つべき時間=現在あるべき時刻-現在の時刻
		term = (int)(this->count0t + this->fps_count*(1000.0 / this->fpsMax)) - DxLib::GetNowCount();

	if (term>0)//待つべき時間だけ待つ
		Sleep(term);

	gnt = DxLib::GetNowCount();

	if (this->fps_count == 0)//フレームに1度基準を作る
		this->count0t = gnt;
	this->f[this->fps_count] = gnt - t;//１周した時間を記録
	t = gnt;
	//平均計算
	if (this->fps_count == this->fpsMax - 1) {
		this->average = 0;
		for (i = 0; i<this->fpsMax; i++)
			this->average += this->f[i];
		this->average /= this->fpsMax;
	}
	this->fps_count = (++(this->fps_count)) % this->fpsMax;

	if (fabs(this->average)>0.000001)
	{
		this->framerate = 1000.0 / this->average;
	}
	else
	{
		this->framerate = 30;
	}
}

//描画関数
void fps::draw() {

	DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);

	DxLib::DrawFormatStringToHandle(
		this->cGame.getScreenX(0),
		this->cGame.getScreenY(0.96f),
		DxLib::GetColor(128, 128, 128),
		this->cGame.getFont().getFonts()[1],
		"[%.2f]", this->framerate);
	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

	return;
}


//アップデート関数
void fps::update() {
	this->wait();
	this->draw();
}