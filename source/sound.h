#pragma once

namespace SOUND
{
	//定数
	const int SOUND_MAX = 7;//使用する音の最大数

	//サウンドの種類
	enum class SOUND_NAME : int
	{
		SE_CURSOR_MOVE,//カーソル移動
		SE_CURSOR_ENTER,//カーソル決定
		SE_BLOCK_MOVE,//ブロック移動
		SE_BLOCK_ROTATE,//ブロック回転
		SE_BLOCK_DELETE,//ブロック消去
		SE_BLOCK_LOCK,//ブロックのロック
		BGM_MAINGAME,//メインゲーム中のBGM
	};

	//サウンドの再生状態
	enum class SOUND_STATE :int
	{
		NO_PLAYING,//再生していない
		PLAY_READY,//再生開始直前
		PLAYING_LOOP,//ループ再生中
		PLAYING_NOLOOP,//非ループ再生中
		STOP_READY,//停止直前
	};

	//サウンドハンドル構造体
	struct SOUNDDATA
	{
		int handle;//ハンドル
		int count;//カウンタ
		SOUND_STATE sSoundState;//再生状態
		int playtype;//再生形式
	};
}

class sound
{
private:
	//参照
	game& cGame;

	SOUND::SOUNDDATA sSoundData[SOUND::SOUND_MAX];//サウンドデータ

public:
	sound(game&);
	~sound();

	int initialize();//初期化
	int update();//更新

	void setSound(SOUND::SOUND_NAME, int);//サウンド再生関数
	void stopSound(SOUND::SOUND_NAME);//サウンド停止関数
};

