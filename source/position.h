#pragma once

//4次元座標構造体
struct position
{
	//パラメータ
	int x;
	int y;
	int z;
	int w;

	//オペレータ
	position operator+(position&);
	position operator-(position&);
	position operator+=(position&);
	position operator-=(position&);
	bool operator==(position&);
	bool operator!=(position&);

	//取得関数
	static position set(int, int, int, int);
};
