#pragma once

namespace CONTROLLER
{
	//定数
	const int BUTTON_MAX = 24;//ボタン最大数
	const int KEY_MAX = 256;//キー最大数
	//アナログスティック判別用
	const BOOL PLUS = TRUE;//いらない?
	const BOOL MINUS = FALSE;//いらない?

	//ハットスイッチ判定用
	//DirectInputでのみ使用
	//XInputはハットスイッチがボタン扱いなのでいらない
	const int HAT_NO_INPUT = -1;//入力なし
	const int HAT_OFFSET = 4500;//オフセット
	const int HAT_MAX = 36000;//ハットの最大値
	const int HAT_UP = 0;//↑
	const int HAT_RIGHT = 9000;//→
	const int HAT_DOWN = 18000;//↓
	const int HAT_LEFT = 27000;//←

	//XInput用
	const int STICK_MAX = 32768;//スティックの上限値
	const int TRIGGER_MAX = 255;//トリガーの上限値

	//XINPUTをコピーして全てshort型に変える独自構造体
	struct MYXINPUT_STATE
	{
		unsigned short input[26];
	};
	//マクロ定義
#define MYXINPUT_DPAD_UP 0
#define MYXINPUT_DPAD_DOWN 1
#define MYXINPUT_DPAD_LEFT 2
#define MYXINPUT_DPAD_RIGHT 3
#define MYXINPUT_START 4
#define MYXINPUT_BACK 5
#define MYXINPUT_LS 6
#define MYXINPUT_RS 7
#define MYXINPUT_LB 8
#define MYXINPUT_RB 9
#define MYXINPUT_A 12
#define MYXINPUT_B 13
#define MYXINPUT_X 14
#define MYXINPUT_Y 15
#define MYXINPUT_LT 16
#define MYXINPUT_RT 17
#define MYXINPUT_LSTICK_RIGHT 18
#define MYXINPUT_LSTICK_LEFT 19
#define MYXINPUT_LSTICK_UP 20
#define MYXINPUT_LSTICK_DOWN 21
#define MYXINPUT_RSTICK_RIGHT 22
#define MYXINPUT_RSTICK_LEFT 23
#define MYXINPUT_RSTICK_UP 24
#define MYXINPUT_RSTICK_DOWN 25


	//コンフィグ画面で表示する文字列
	const std::string BUTTON_STRING[BUTTON_MAX]
	{
		"決定/(XZ回転+)","ブロック固定","(XY回転+)","ハードドロップ",
		"(回転)","(逆回転)",
		"LS","RS",
		"BACK","START",
		"カメラ↑","カメラ→","カメラ↓","カメラ←",
		"カメラズームイン","カメラズームアウト",
		"カーソル↑/Z移動+/(XW回転-)","カーソル↓/Z移動-/(XW回転+)","X移動-/(ZW回転-)","X移動+/(ZW回転+)",
		"Y移動-/(YZ回転+)","Y移動+/(YZ回転-)","W移動-/(YW回転+)","W移動+/(YW回転-)",
	};
}
//ゲームコントローラの各ボタン定義
enum  class BUTTON : int
{
	//ここからボタン
	A, B, X, Y,
	LB, RB,
	LS, RS,
	BACK, START,
	HAT_UP, HAT_RIGHT, HAT_DOWN, HAT_LEFT,
	//ここからトリガー
	LT, RT,
	//ここからスティック
	LSTICK_UP,  LSTICK_DOWN, LSTICK_RIGHT, LSTICK_LEFT,
	RSTICK_UP,  RSTICK_DOWN, RSTICK_RIGHT, RSTICK_LEFT,
};

using namespace CONTROLLER;

class controller
{
	//参照
	game& cGame;//gameクラスへの参照

	//変数
	DxLib::XINPUT_STATE xInput;//DXライブラリから得たXInputのデータ
	MYXINPUT_STATE myXInput;//↑を自作のmyXInputに書き換えたデータを格納する
	int xInputHandle;//XInput用ゲームパッドの入力状態を格納する
	char allKey[KEY_MAX];

	int config[BUTTON_MAX];//キーコンフィグ用の番号
	int setKey[BUTTON_MAX];//各ゲームボタンに対応したキー

	short **padParam;//ゲーム内ボタンと対応しているパラメータのポインタの配列

	//ゲーム内ボタン
	//他のクラスからはここの値を見てボタンが押されているか判定する
	int button[BUTTON_MAX];

	//ここからコントローラのパラメータ
	/*Direct Inputt用
	DINPUT_JOYSTATE padInput;//ゲームパッドの入力状態を格納する構造体
	int getPadState();//ゲームパッドの入力状態を取得
	int setAnalogButton(BUTTON button, int key, int padParam,BOOL direction,int border);
	int setButton(BUTTON button, int key_input, int padInput);
	int setHatSwitch(BUTTON button, int key, int padParam, int direction);
	*/

	//関数
	int getAllKey();//キーボードの入力状態を取得
	int setInputAll();//ゲーム内ボタンの入力を決定

	//指定のゲーム内ボタンの入力状態を,キーボードとパッドの入力状態から決定する
	int setButtonFromMyXInput(BUTTON, int, int, int);

	//使わない可能性大
	int setButton(BUTTON button, int key_input, int padInput);
	int setStick(BUTTON button, int key_input, short padParam, BOOL direction, int border);
	int setTrigger(BUTTON button, int key_input, unsigned char padParam, int border);
	//

	//DXライブラリが取得したXInputを自作構造体に変換する
	int myGetJoyPadXInputState(int,DxLib::XINPUT_STATE*,MYXINPUT_STATE*);

public:
	controller(game& game);//コンストラクタ
	~controller();//デストラクタ

	int update();//アップデート関数

	//アップデート後に,外部からこの関数を呼び出すとそのボタンの
	//入力状態を取得できる
	int getButton(BUTTON button);

	//全てのボタンの入力状態の取得
	//特別な場合を除いてgetButton()の使用を推奨
	//乱用禁止
	int* getButtonAll();

	//XInputのハンドルを取得
	//-1の時はXInput対応ゲームパッドが接続されていない
	int getXInputHandle() { return this->xInputHandle; }

	//XInputの取得
	DxLib::XINPUT_STATE& getXInputState() { return this->xInput; }
	//MyXInputの取得
	MYXINPUT_STATE& getMyXInputState() { return this->myXInput; }
	//コンフィグデータの取得
	int* getConfig() { return this->config; }
	//コンフィグデータの書き換え
	void setConfig(int* data) { for (int i = 0; i < BUTTON_MAX; i++)this->config[i] = data[i]; }
	//全てのキーの入力状態を取得
	//ゲームパッドの入力を判定しない場合,
	//ゲーム内ボタンに定義されていないキーを取得する
	//値は0/1の2通りなので,連打判定は使う側で行う
	//乱用禁止
	char* getKeys() { return this->allKey; }
};

