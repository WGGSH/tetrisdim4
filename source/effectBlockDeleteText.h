#pragma once
#include "effectBase.h"

namespace EFFECT
{
	const std::string BLOCK_DELETE_TEXT[4] =
	{
		"SINGLE",
		"DOUBLE",
		"TRIPLE",
		"TETRIS",
	};
};

class effectBlockDeleteText :
	public effectBase
{
private:
	int num;
public:
	effectBlockDeleteText(game&,int);
	~effectBlockDeleteText();

	int update();
};

