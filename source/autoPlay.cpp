#include "main.h"

//ブロックリストのインポート
#include "tetrotesseract.h"

//コンストラクタ
autoPlay::autoPlay(
	game& c_Game,controller& c_Controller):
	maingame(c_Game,c_Controller)
{
	this->counter = 1250;
	this->moveTime = 0;// this->counter * 43;

	//SRand(2);
	//回転フォームリストの生成
	this->generateFormList();

	//評価関数リストの作成
	this->evaluateFunctionList.clear();
	this->evaluateFunctionList.push_back(evaluateFunction::evaluate00);
	this->evaluateFunctionList.push_back(evaluateFunction::evaluate01);
	this->evaluateFunctionList.push_back(evaluateFunction::evaluate02);
	this->evaluateFunctionList.push_back(evaluateFunction::evaluate03);
	this->evaluateFunctionList.push_back(evaluateFunction::evaluate04);
	this->evaluateFunctionList.push_back(evaluateFunction::evaluate05);

	//評価関数のオフセット定義
	/*
	this->SCORE_OFFSET[0] = 1.0f;//消した段の数
	this->SCORE_OFFSET[1] = -30.0f;//危険ゾーンに突入するか
	this->SCORE_OFFSET[2] = -1.7f;//ホールの数
	this->SCORE_OFFSET[3] = 0.5f;//落下位置の低さ
	this->SCORE_OFFSET[4] = 0.2f;//最大高度
	this->SCORE_OFFSET[5] = 0.05f;//最大高度
	*/

	/*
	テトリス棒待ちをするようになって少し危険
	this->SCORE_OFFSET[0] = 0.05f;//最もブロックで埋まった段のブロック数
	this->SCORE_OFFSET[1] = 1.0f;//消した段の数
	this->SCORE_OFFSET[2] = -30.0f;//危険ゾーンに突入するか
	this->SCORE_OFFSET[3] = -1.7f;//ホールの数
	this->SCORE_OFFSET[4] = 0.5f;//落下位置の低さ
	this->SCORE_OFFSET[5] = 0.2f;//最大高度
	*/


	//低めに狙うことでテトリス棒に頼らず消していける
	this->SCORE_OFFSET[0] = 0.05f;//最もブロックで埋まった段のブロック数
	this->SCORE_OFFSET[1] = 1.0f;//消した段の数
	this->SCORE_OFFSET[2] = -30.0f;//危険ゾーンに突入するか
	this->SCORE_OFFSET[3] = -1.7f;//ホールの数
	this->SCORE_OFFSET[4] = 1.5f;//落下位置の低さ
	this->SCORE_OFFSET[5] = 0.2f;//最大高度
	
}

//デストラクタ
autoPlay::~autoPlay()
{
}


//ゲームの初期化
int autoPlay::initialize()
{
	//カウンタの初期化
	this->count = 0;
	this->gameOverCount = 0;
	//ゲーム状態の初期化
	this->eState = STATE::WIPE_S;
	this->eGameState = GAME_STATE::DROP;
	this->gameOverSelect = 0;
	//得点の初期化
	this->score = 0;
	//ブロック出現数の初期化
	for (int i = 0; i < TETROTESSERACT_MAX; i++)this->blockCount[i] = 0;
	this->effectList.clear();
	this->effectList.push_back(std::shared_ptr<effectBase>(new effectMagicCircle(
		this->cGame)));

	//背景画像ハンドルの初期化
	this->backHandle = DxLib::MakeGraph(
		OPTION::WINDOW_LIST_X[this->cGame.getOption().getSaveData().windowType],
		OPTION::WINDOW_LIST_Y[this->cGame.getOption().getSaveData().windowType]);
	//BGMの再生
	this->cGame.getSound().setSound(SOUND::SOUND_NAME::BGM_MAINGAME, DX_PLAYTYPE_LOOP);


	//フィールドの初期化
	for (int z = 0; z < FIELD_SIZE_Z; z++) {
		for (int x = 0; x < FIELD_SIZE_X; x++) {
			for (int y = 0; y < FIELD_SIZE_Y; y++) {
				for (int w = 0; w < FIELD_SIZE_W; w++) {

					if (
						((x == 0 || x == FIELD_SIZE_X - 1) +
							(y == 0 || y == FIELD_SIZE_Y - 1) +
							(z == 0 || z == FIELD_SIZE_Z - 1) +
							(w == 0 || w == FIELD_SIZE_W - 1)) == 0)
					{
						this->field[x][y][z][w] = static_cast<int>(BLOCK_TYPE::NON);
					}
					else
					{
						this->field[x][y][z][w] = static_cast<int>(BLOCK_TYPE::GROUND);
					}

				}
			}
		}
	}

	this->create();

	//3Dの初期化処理
	this->initialize3d();

	return 0;
}

//起動時に実行して回転フォームリストを作成する関数
//精度を上げるほど計算量が爆発的に増えるので注意
void autoPlay::generateFormList()
{
	//回転パターンの生成
	//std::vector<axis4d> formList[TETROTESSERACT_MAX];
	for (int i = 0; i < TETROTESSERACT_MAX; i++) {
		this->formList[i].clear();//初期化

		//比較用仮想ブロックの生成
		int virtualBlockSample[TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE];
		for (int x = 0; x < TESSERACT_SIZE; x++) {
			for (int y = 0; y < TESSERACT_SIZE; y++) {
				for (int z = 0; z < TESSERACT_SIZE; z++) {
					for (int w = 0; w < TESSERACT_SIZE; w++) {
						virtualBlockSample[x][y][z][w] = TETROTESSERACT_SET[i][x][y][z][w];
					}
				}
			}
		}

		//大体すべての回転パターンを抽出

		//全て計算すると処理時間が莫大になるので一部のみ調べる
		//全2949120通りの内
		//64通りのみ調べる

		//パターン1
		//3次元空間内での回転のみを使用してパズルする
		//XW,YW,ZW
		//最有力候補?
		/*for (int xw = 0; xw < 4; xw++) {
			for (int yw = 0; yw < 4; yw++) {
				for (int zw = 0; zw < 4; zw++) {
					axis4d bufaxis = { 0,0,xw,0,yw,zw };*/

		//パターン2
		//4次元的回転を少し混ぜることで,
		//4次元ゲームだという事を認識しやすくする
		//XZ,YW,ZW
		//十分消せる,というかほぼ封鎖もゲームオーバーもしないのでこれも良さげ?
		for (int xz = 0; xz < 4; xz++) {
			for (int yw = 0; yw < 4; yw++) {
				for (int zw = 0; zw < 4; zw++) {
					axis4d bufaxis = { xz,0,0,0,yw,zw };

					//計算時間が足りない...
					//for (int xy = 0; xy < 4; xy++) {
						//for (int yz = 0; yz < 4; yz++) {
							//for (int xz = 0; xz < 4; xz++) {

					//乱数による姿勢制御は意外とパズルした
					//bufaxis = { GetRand(3),GetRand(3),GetRand(3),GetRand(3),GetRand(3),GetRand(3) };
					
					//axis4d bufaxis = { xy,xz,xw,yz,yw,zw };
					//パターン3
		            //4次元的な回転を多用する
					//危険
					/*for (int xy = 0; xy < 4;xy++){
						for (int xw = 0; xw < 4;xw++){
							for (int yw = 0; yw < 4;yw++){
								axis4d bufaxis = { xy,0,xw,0,yw,0 };*/




					//回転シミュレートを行う仮想ブロックの生成
					int virtualBlockTest[TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE];
					for (int x = 0; x < TESSERACT_SIZE; x++) {
						for (int y = 0; y < TESSERACT_SIZE; y++) {
							for (int z = 0; z < TESSERACT_SIZE; z++) {
								for (int w = 0; w < TESSERACT_SIZE; w++) {
									virtualBlockTest[x][y][z][w] = TETROTESSERACT_SET[i][x][y][z][w];
								}
							}
						}
					}

					//回転シミュレート
					this->vRotate(virtualBlockTest, bufaxis);

					BOOL flag = TRUE;
					for (auto form : formList[i]) {
						if (this->checkForm(virtualBlockSample, virtualBlockTest) == TRUE)
						{
							//既にシミュレートした形と同じものが存在したらスキップ
							flag = FALSE;
						}
					}
					if (flag == TRUE) {
						//初めて登場した形ならリストに追加
						this->formList[i].push_back(bufaxis);
					}

				}
			}
		}
		
		//}
		//}
		//}

	}
}

//仮想回転関数
//コード汚すぎ問題
void autoPlay::vRotate(
	int block[TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE],
	axis4d& angle)
{
	axis4d bufAxis;
	bufAxis.xy = angle.xy;
	bufAxis.xz = angle.xz;
	bufAxis.xw = angle.xw;
	bufAxis.yz = angle.yz;
	bufAxis.yw = angle.yw;
	bufAxis.zw = angle.zw;

	int x, y, z, w;
	int rBlock[TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE] = { 0 };//回転後の形状

	//回転が完了するまで回し続ける
	while (bufAxis.xy != 0 || bufAxis.xz != 0 || bufAxis.xw != 0 ||
		bufAxis.yz != 0 || bufAxis.yw != 0 || bufAxis.zw != 0) {

		for (x = 0; x < TESSERACT_SIZE; x++) {
			for (y = 0; y < TESSERACT_SIZE; y++) {
				for (z = 0; z < TESSERACT_SIZE; z++) {
					for (w = 0; w < TESSERACT_SIZE; w++) {
						int target = block[x][y][z][w];
						if (bufAxis.xy > 0)
						{
							//xy回転
							rBlock[x][y][w][TESSERACT_SIZE - 1 - z] = target;
						}
						else if (bufAxis.xz > 0)
						{
							//xz回転
							rBlock[x][w][z][TESSERACT_SIZE - 1 - y] = target;

						}
						else if (bufAxis.xw > 0)
						{
							//xw回転
							rBlock[x][z][TESSERACT_SIZE - 1 - y][w] = target;
						}
						else if (bufAxis.yz > 0)
						{
							//yz回転
							rBlock[w][y][z][TESSERACT_SIZE - 1 - x] = target;
						}
						else if (bufAxis.yw > 0)
						{
							//yw回転
							rBlock[TESSERACT_SIZE - 1 - z][y][x][w] = target;
						}
						else if (bufAxis.zw > 0)
						{
							//zw回転
							rBlock[y][TESSERACT_SIZE - 1 - x][z][w] = target;
						}
					}
				}
			}
		}
		if (bufAxis.xy > 0)bufAxis.xy--;
		else if (bufAxis.xz > 0)bufAxis.xz--;
		else if (bufAxis.xw > 0)bufAxis.xw--;
		else if (bufAxis.yz > 0)bufAxis.yz--;
		else if (bufAxis.yw > 0)bufAxis.yw--;
		else if (bufAxis.zw > 0)bufAxis.zw--;



		//複写処理
		for (x = 0; x < TESSERACT_SIZE; x++) {
			for (y = 0; y < TESSERACT_SIZE; y++) {
				for (z = 0; z < TESSERACT_SIZE; z++) {
					for (w = 0; w < TESSERACT_SIZE; w++) {
						block[x][y][z][w] = rBlock[x][y][z][w];
					}
				}
			}
		}
	}
}

//ブロックの同一性を判断
//TRUE:同一
//FALSE:非同一
int autoPlay::checkForm(
	int blocka[TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE],
	int blockb[TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE])
{
	for (int x = 0; x < TESSERACT_SIZE; x++) {
		for (int y = 0; y < TESSERACT_SIZE; y++) {
			for (int z = 0; z < TESSERACT_SIZE; z++) {
				for (int w = 0; w < TESSERACT_SIZE; w++) {
					if (blocka[x][y][z][w] != blockb[x][y][z][w])
					{
						return FALSE;
					}
				}
			}
		}
	}
	return TRUE;
}

//仮想フィールドのブロック消去処理
int autoPlay::vDel(
	int vfield[FIELD_SIZE_X][FIELD_SIZE_Y][FIELD_SIZE_Z][FIELD_SIZE_W],
	int num)
{
	for (int z = 1; z < FIELD_SIZE_Z - 1; z++) {
		BOOL flag = TRUE;
		for (int x = 1; x < FIELD_SIZE_X - 1; x++) {
			for (int y = 1; y < FIELD_SIZE_Y - 1; y++) {
				for (int w = 1; w < FIELD_SIZE_W - 1; w++) {
					if (vfield[x][y][z][w] == static_cast<int>(BLOCK_TYPE::NON))
					{
						flag = FALSE;
					}
				}
			}
		}
		//flagがTRUEなら消える
		if (flag == TRUE)
		{
			//ブロックを1段ずつ落下する
			for (int z2 = z; z2 >= 1; z2--) {
				for (int x = 1; x < FIELD_SIZE_X - 1; x++) {
					for (int y = 1; y < FIELD_SIZE_Y - 1; y++) {
						for (int w = 1; w < FIELD_SIZE_W - 1; w++) {
							vfield[x][y][z2][w] =
								(vfield[x][y][z2 - 1][w] != static_cast<int>(BLOCK_TYPE::GROUND)) ?
								(vfield[x][y][z2 - 1][w]) :
								static_cast<int>(BLOCK_TYPE::NON);
						}
					}
				}

			}
			//再起処理
			return this->vDel(vfield,num+1);
		}

	}
	return num;
}



//評価関数総合
void autoPlay::getScore(
	int field[FIELD_SIZE_X][FIELD_SIZE_Y][FIELD_SIZE_Z][FIELD_SIZE_W],
	AUTOPLAY::ACTIONDATA& action)
{
	for (int i = 0; i < AUTOPLAY::EVALUATE_FUNCTION_MAX; i++) {
		action.scores[i] = this->evaluateFunctionList.at(i)(field,action);
		//if (i == 0 && action.scores[i] == 125)action.scores[i] =0;
		action.score += action.scores[i] * this->SCORE_OFFSET[i];
	}
}

//行動決定関数
void autoPlay::setAction()
{
	std::vector<AUTOPLAY::ACTIONDATA> action_list;
	action_list.clear();

	AUTOPLAY::ACTIONDATA nondata;
	nondata.score = 0;
	for (int i = 0; i < AUTOPLAY::EVALUATE_FUNCTION_MAX; i++)nondata.scores[i] = 0;
	nondata.targetPos = position::set(-1, -1, -1, -1);
	nondata.targetAxis = axis4d::set(0, 0, 0, 0, 0, 0);
	nondata.color = this->sActionData.color;
	nondata.flag = TRUE;

	//全ての回転フォームに対してシミュレートする
	int size = this->formList[this->sActionData.color].size();
	for (int i = 0; i < size; i++) {

		//XYW座標が全ての場合に対して移動可能かシミュレートする
		for (int x = 0; x < FIELD_SPACE_X; x++) {
			for (int y = 0; y < FIELD_SPACE_Y; y++) {
				for (int w = 0; w < FIELD_SPACE_W; w++) {
					action_list.push_back(nondata);
					action_list.back().targetPos = position::set(x, y, 0, w);
					action_list.back().targetAxis= this->formList[this->sActionData.color].at(i);

					//仮想座標
					position virtualPos = this->location;
					//仮想フィールド
					int virtualField[FIELD_SIZE_X][FIELD_SIZE_Y][FIELD_SIZE_Z][FIELD_SIZE_W];
					//仮想ブロック
					int virtualBlockTest[TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE][TESSERACT_SIZE];
					//仮想回転軸
					axis4d virtualAxis;

					//仮想フィールドの初期化
					for (int x2 = 0; x2 < FIELD_SIZE_X; x2++) {
						for (int y2 = 0; y2 < FIELD_SIZE_Y; y2++) {
							for (int z2 = 0; z2 < FIELD_SIZE_Z; z2++) {
								for (int w2 = 0; w2 < FIELD_SIZE_W; w2++) {
									virtualField[x2][y2][z2][w2] = this->field[x2][y2][z2][w2];
								}
							}
						}
					}

					//回転シミュレートを行う仮想ブロックの生成
					for (int x = 0; x < TESSERACT_SIZE; x++) {
						for (int y = 0; y < TESSERACT_SIZE; y++) {
							for (int z = 0; z < TESSERACT_SIZE; z++) {
								for (int w = 0; w < TESSERACT_SIZE; w++) {
									virtualBlockTest[x][y][z][w] = TETROTESSERACT_SET[this->sActionData.color][x][y][z][w];
								}
							}
						}
					}

					//仮想回転
					virtualAxis = this->formList[this->sActionData.color].at(i);
					this->vRotate(virtualBlockTest, virtualAxis);


					//目的の座標になるまでループ
					while (!
						(virtualPos.x == action_list.back().targetPos.x &&
							virtualPos.y == action_list.back().targetPos.y &&
							virtualPos.w == action_list.back().targetPos.w))
					{
						if (virtualPos.x < action_list.back().targetPos.x)
						{
							if (this->check(virtualField, virtualBlockTest, virtualPos + position::set(1, 0, 0, 0)) == FALSE)goto FAILED;
							else virtualPos.x++;
						}
						else if (virtualPos.x>action_list.back().targetPos.x)
						{
							if (this->check(virtualField, virtualBlockTest, virtualPos + position::set(-1, 0, 0, 0)) == FALSE)goto FAILED;
							else virtualPos.x--;
						}
						else if (virtualPos.y < action_list.back().targetPos.y)
						{
							if (this->check(virtualField, virtualBlockTest, virtualPos + position::set(0, 1, 0, 0)) == FALSE)goto FAILED;
							else virtualPos.y++;
						}
						else if (virtualPos.y > action_list.back().targetPos.y)
						{
							if (this->check(virtualField, virtualBlockTest, virtualPos + position::set(0, -1, 0, 0)) == FALSE)goto FAILED;
							else virtualPos.y--;
						}
						else if (virtualPos.w < action_list.back().targetPos.w)
						{
							if (this->check(virtualField, virtualBlockTest, virtualPos + position::set(0, 0, 0, 1)) == FALSE)goto FAILED;
							else virtualPos.w++;
						}
						else if (virtualPos.w > action_list.back().targetPos.w)
						{
							if (this->check(virtualField, virtualBlockTest, virtualPos + position::set(0, 0, 0, -1)) == FALSE)goto FAILED;
							else virtualPos.w--;
						}
					}

					/*確定じゃない*////ここまで来たらその場所に移動可能と確定する

					//Z座標の決定
					int z;
					for (z = 0; z < FIELD_SIZE_Z; z++) {
						if (this->check(virtualField, virtualBlockTest, virtualPos + position::set(0, 0, z, 0)) == FALSE)break;
					}
					virtualPos.z = this->location.z + z - 1;
					action_list.back().targetPos.z = virtualPos.z;

					//仮想フィールドにブロックを置く
					for (int x = 0; x < TESSERACT_SIZE; x++) {
						for (int y = 0; y < TESSERACT_SIZE; y++) {
							for (int z = 0; z < TESSERACT_SIZE; z++) {
								for (int w = 0; w < TESSERACT_SIZE; w++) {
									if (virtualBlockTest[x][y][z][w] != 0)
									{
										if (virtualPos.x + x >= FIELD_SIZE_X ||
											virtualPos.y + y >= FIELD_SIZE_Y ||
											virtualPos.z + z >= FIELD_SIZE_Z ||
											virtualPos.w + w >= FIELD_SIZE_W)
										{
											//continue;
											goto FAILED;
										}
										virtualField
											[virtualPos.x + x][virtualPos.y + y]
											[virtualPos.z + z][virtualPos.w + w] = virtualBlockTest[x][y][z][w];
									}

								}
							}
						}
					}

					//ブロック消去シミュレート

					//スコアの計算
					this->getScore(virtualField, action_list.back());

					continue;

					//失敗した場合,作ったものを削除する
				FAILED:
					//this->setAction();
					action_list.pop_back();
				}
			}
		}

	}
	//目的地の決定
	int max = -10000;
	int maxpos = -1;
	for (int i = 0; i < action_list.size(); i++) {
		if (action_list.at(i).score>max)
		{
			max = action_list.at(i).score;
			maxpos = i;
		}
	}
	//action.targetPos.z = virtualPos.z;
	
	this->sActionData = action_list.at(maxpos);


	return;
}

//新規ブロックの作成
int autoPlay::create()
{
	//所持ブロックの初期化
	int color = GetRand(TETROTESSERACT_MAX - 1);
	this->blockCount[color]++;
	//color = 6;
	this->sActionData.color = color;
	for (int z = 0; z < TESSERACT_SIZE; z++) {
		for (int x = 0; x < TESSERACT_SIZE; x++) {
			for (int y = 0; y < TESSERACT_SIZE; y++) {
				for (int w = 0; w < TESSERACT_SIZE; w++) {
					this->nowBlock[x][y][z][w] = MAINGAME::TETROTESSERACT_SET[color][x][y][z][w];
				}
			}
		}
	}

	//座標初期化
	this->location = position::set(2, 2, 1, 2);

	//ゲームオーバーの判定
	if(this->over()==1)return 1;

	//行動決定
	this->sActionData.flag = FALSE;
	//this->setAction();

	return 0;
}

//AIの行動入力関数
//優先度を決めて1つずつif文で判定しているので
//かなり長く汚いコードになっている
void autoPlay::AIInput()
{
	//カウンターがたまっていれば
	if (this->moveTime >= AUTOPLAY::AI_MOVE_TIME) {

		//行動パターンが決定していなければここで決定する
		if (this->sActionData.flag == FALSE)
		{
			this->setAction();
		}

		//入力前の状態を記録
		axis4d oldAxis = this->sActionData.targetAxis;
		position oldPosition = this->location;

		//回転制御
		//xy
		if (this->sActionData.targetAxis.xy > 0) 
		{
			this->rotate(axis4d::set(1, 0, 0, 0, 0, 0));
			this->sActionData.targetAxis.xy--;
		}
		//xz
		else if (this->sActionData.targetAxis.xz > 0)
		{
			this->rotate(axis4d::set(0, 1, 0, 0, 0, 0));
			this->sActionData.targetAxis.xz--;
		}
		//xw
		else if (this->sActionData.targetAxis.xw > 0)
		{
			this->rotate(axis4d::set(0, 0, 1, 0, 0, 0));
			this->sActionData.targetAxis.xw--;
		}
		//yz
		else if (this->sActionData.targetAxis.yz > 0)
		{
			this->rotate(axis4d::set(0, 0, 0, 1, 0, 0));
			this->sActionData.targetAxis.yz--;
		}
		//yw
		else if (this->sActionData.targetAxis.yw > 0)
		{
			this->rotate(axis4d::set(0, 0, 0, 0, 1, 0));
			this->sActionData.targetAxis.yw--;
		}
		//zw
		else if (this->sActionData.targetAxis.zw > 0)
		{
			this->rotate(axis4d::set(0, 0, 0, 0, 0, 1));
			this->sActionData.targetAxis.zw--;
		}
		//X移動制御
		//X+
		else if (this->location.x < this->sActionData.targetPos.x)
		{
			if (this->check(this->field, this->nowBlock, this->location + position::set(1, 0, 0, 0)) == TRUE) {
				this->move(position::set(1, 0, 0, 0));
			}
			else
			{
				this->sActionData.targetPos.x--;
			}
		}
		//X-
		else if (this->location.x > this->sActionData.targetPos.x)
		{
			if (this->check(this->field, this->nowBlock, this->location + position::set(-1, 0, 0, 0)) == TRUE) {
				this->move(position::set(-1, 0, 0, 0));
			}
			else {
				this->sActionData.targetPos.x++;
			}
		}
		//Y+
		else if (this->location.y < this->sActionData.targetPos.y)
		{
			if (this->check(this->field, this->nowBlock, this->location + position::set(0, 1, 0, 0)) == TRUE) {
				this->move(position::set(0, 1, 0, 0));
			}
			else
			{
				this->sActionData.targetPos.y--;
			}
		}
		//Y-
		else if (this->location.y > this->sActionData.targetPos.y)
		{
			if (this->check(this->field, this->nowBlock, this->location + position::set(0, -1, 0, 0)) == TRUE) {
				this->move(position::set(0, -1, 0, 0));
			}
			else
			{
				this->sActionData.targetPos.y++;
			}
		}
		//W+
		else if (this->location.w < this->sActionData.targetPos.w)
		{
			if (this->check(this->field, this->nowBlock, this->location + position::set(0, 0, 0, 1)) == TRUE) {
				this->move(position::set(0, 0, 0, 1));
			}
			else
			{
				this->sActionData.targetPos.w--;
			}
		}
		//W-
		else if (this->location.w > this->sActionData.targetPos.w)
		{
			if (this->check(this->field, this->nowBlock, this->location + position::set(0, 0, 0, -1)) == TRUE) {
				this->move(position::set(0, 0, 0, -1));
			}
			else
			{
				this->sActionData.targetPos.w++;
			}
		}
		//Z+
		else if (this->location.z < this->sActionData.targetPos.z)
		{
			if (this->check(this->field, this->nowBlock, this->location + position::set(0, 0, 1, 0)) == TRUE) {
				this->move(position::set(0, 0, 1, 0));
			}
			else
			{
				this->sActionData.targetPos.z--;
			}
		}
		//Z-(無いはず)
		else if (this->location.z > this->sActionData.targetPos.z)
		{
			this->move(position::set(0, 0, -1, 0));
		}
		//目的地到達
		//ロックとブロックの消去,新規ブロックの作成,ゲームオーバー判定,
		else
		{
			this->lock();
			//ロックSE
			this->cGame.getSound().setSound(SOUND::SOUND_NAME::SE_BLOCK_LOCK, DX_PLAYTYPE_BACK);

			//ブロックの消去判定
			int num = this->del(0);
			if (num >= 1)
			{
				this->addScore(num *num*num*num * 10000);

				//SEの再生
				this->cGame.getSound().setSound(SOUND::SOUND_NAME::SE_BLOCK_DELETE, DX_PLAYTYPE_BACK);

				//エフェクトの実行
				this->effectList.push_back(std::shared_ptr<effectBase>(new effectBlockDeleteText(
					this->cGame, num - 1)));

			}
			if (this->create() == 1)return;

			//固定時にSEを鳴らさない
			oldPosition = this->location;
		}

		//カウンターを戻す
		this->moveTime -= AUTOPLAY::AI_MOVE_TIME;
		//再起することで,カウンターが溜まっていれば連続で行動できる
		this->AIInput();

		//移動が行われていたらSEを再生
		if (oldPosition != this->location)
		{
			this->cGame.getSound().setSound(SOUND::SOUND_NAME::SE_BLOCK_MOVE, DX_PLAYTYPE_BACK);
		}
		//回転が行われていた場合SEを再生
		if (oldAxis != this->sActionData.targetAxis)
		{
			this->cGame.getSound().setSound(SOUND::SOUND_NAME::SE_BLOCK_ROTATE, DX_PLAYTYPE_BACK);
		}

	}
}

//ゲームオーバー処理
int autoPlay::over()
{
	if (this->check(this->field, this->nowBlock, this->location) == FALSE)
	{

		//ゲームオーバー
		//this->initialize();
		this->eState = MAINGAME::STATE::GAMEOVER_ANIM;
		return 1;
		//this->cRecord->save();
	}

	return 0;
}

//速度変更
void autoPlay::replaySpeedInput()
{
	if (this->cController.getButton(BUTTON::Y) == 1)
	{
		this->counter *= 2;
	}
	if (this->cController.getButton(BUTTON::A) == 1)
	{
		this->counter /= 2;
	}
}


//ツイート処理
//まだ内容吟味中
void autoPlay::tweetScore()
{
	//ツイート
	std::string base = "https://twitter.com/intent/tweet?";
	std::string hash = "hashtags=TetrisDim4&";
	std::string referer = "original_referer=https://twitter.com.WGG_SH&";
	std::string text = "text=4次元テトリスでAIがScore:";
	text += std::to_string(this->score);
	text += "点を獲得しました!";
	text += "&";
	std::string tw_p = "tw_p = tweetbutton&";
	//std::string url = "url=https://twitter.com/WGG_SH";
	std::string link = base + hash + referer + text + tw_p;// +url;

	//ブラウザを開く
	ShellExecute(NULL, TEXT("open"),
		TEXT(link.c_str()), NULL, TEXT(""), SW_SHOW);
}

//更新
int autoPlay::update()
{
	
	//static int backhandle[3] = { -1 };


	this->count++;
	this->replaySpeedInput();

	switch (this->eState)
	{
	case STATE::WIPE_S:
		this->draw3D();
		this->drawUI();
		this->drawWipe();
		break;
	case STATE::WIPE_F:
		this->draw3D();
		this->drawUI();
		this->drawWipe();
		break;
	case STATE::PLAYING:
		this->effectUpdate();

		this->moveTime += this->counter;
		switch (this->eGameState)
		{
		case GAME_STATE::DROP:
			//this->input();
			//this->draw();
			this->AIInput();
			this->cameraInput();
			this->cameraMove();
			this->draw3D();
			this->drawUI();


			//ブラー処理
			/*if (backhandle[2] != -1) {
				DeleteGraph(backhandle[2]);
			}
			for (int i = 2; i >= 1; i--) {
				backhandle[i] = backhandle[i - 1];
			}
			backhandle[0] = MakeGraph(
				OPTION::WINDOW_LIST_X[this->cGame.getOption().getSaveData().windowType],
				OPTION::WINDOW_LIST_Y[this->cGame.getOption().getSaveData().windowType]);
			DxLib::GetDrawScreenGraph(
				0, 0,
				OPTION::WINDOW_LIST_X[this->cGame.getOption().getSaveData().windowType],
				OPTION::WINDOW_LIST_Y[this->cGame.getOption().getSaveData().windowType],
				backhandle[0]);
			DxLib::GraphFilter(backhandle[0],
				DX_GRAPH_FILTER_GAUSS, 32, 6000);
			for (int i = 0; i < 3; i++) {
				SetDrawBlendMode(DX_BLENDMODE_ADD, 255 / 3 * (3-i));
				DrawGraph(0,0,backhandle[i], TRUE);
			}
			SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);*/

			break;
		case GAME_STATE::DELETING:
			break;
		}
		break;
	case STATE::PAUSE:
		this->pauseUpdate();
		break;
	case STATE::GAMEOVER_ANIM:
		this->gameOverAnimUpdate();
		break;
	case STATE::GAMEOVER_SELECT:
		this->gameOverSelectUpdate();
		break;
	default:
		break;
	}


	/*DxLib::DrawFormatString(100, 420, 0xff0000, "ACTTARPOS:[%d,%d,%d,%d]",
		this->sActionData.targetPos.x, this->sActionData.targetPos.y,
		this->sActionData.targetPos.z, this->sActionData.targetPos.w);
	DxLib::DrawFormatString(100, 440, 0xffffff, "ACTTARAXIS:[%d,%d,%d,%d,%d,%d]",
		this->sActionData.targetAxis.xy, this->sActionData.targetAxis.xz, this->sActionData.targetAxis.xw,
		this->sActionData.targetAxis.yz, this->sActionData.targetAxis.yw, this->sActionData.targetAxis.zw);

	DxLib::DrawFormatString(20, 200, 0xff0000, "AXIS=[%d,%d,%d,%d,%d,%d]",
		this->sActionData.targetAxis.xy, this->sActionData.targetAxis.xz, this->sActionData.targetAxis.xw,
		this->sActionData.targetAxis.yz, this->sActionData.targetAxis.yw, this->sActionData.targetAxis.zw);

	DxLib::DrawFormatString(100, 460, 0xff0000, "ACTSCORE:%.1f", this->sActionData.score);
	for (int i = 0; i < AUTOPLAY::EVALUATE_FUNCTION_MAX; i++) {
		DrawFormatString(100, 480 + 20 * i, 0xffffff,
			"SCORE[%d]:%.2f×%.2f=%.2f",i,
			this->sActionData.scores[i], this->SCORE_OFFSET[i],
			this->sActionData.scores[i] * this->SCORE_OFFSET[i]);
	}*/
	return 0;
}